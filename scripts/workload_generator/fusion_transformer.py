from distutils.command.config import config
# from errno import ENEEDAUTH
import math
import os
import shutil
import json
import subprocess
import pandas as pd
import configparser as cp

from gen_astrasim_workload_input import Layer, TransformerLayer, TopologyItem
from utils import divisorGenerator, get_the_minimum_greater_than_element_of_list, get_line_number
from fusion_scalesim import get_scalesim_output_internal, get_analytical_output_internal


##############################
### Input parameters and files ###
# input/psum bytes for BF16 GEMM operation
input_data_type_size = 2
psum_data_type_size = 4
# NPU and MP/DP config
num_npus = 3840
mp_degree = 256
dp_degree = int(num_npus / mp_degree)
#
dnn_name = "gpt3_310B_megatron"
dnn_type = "Transformer"
# NPU
num_cores = 2
num_mxu_cores = 2
num_subarrays = 16
mem_bw = 1
mem_latency = 0
# Freq in Mhz
frequency = 1000
# Network
# BW in GB/sec
nw_bw = 80
# latency in cycles
nw_latency = 200
num_nw_links = 2
# Fusion pipeline delay overhead
fusion_pipeline_overhead_coeff = 0.2
# Compute Simulation mode
simulation_mode = "analytical"
# simulation_mode = "scalesim"
###
###################################
run_name = dnn_name + "_" + "{}".format(dp_degree) + "DP_" + "{}".format(mp_degree) + "MP_tpuv3_" + "{}".format(num_npus) + "_npu_fusion_" + simulation_mode

path_to_dnn_csv_file = "mnk_inputs/" + dnn_name + ".csv"

if simulation_mode == "analytical":
    path_to_dnn_fwd_cycles_csv = "outputs/MEGATRON_HYBRID_DATA_MODEL/" + dnn_name + "_" + "{}".format(dp_degree) + "DP_" + "{}".format(mp_degree) + "MP_tpuv3_" + "{}".format(num_npus) + "_npu_analytical/" + \
                                                                                dnn_name + "_fwd_pass_cycles.csv"
    path_to_dnn_inp_grad_cycles_csv = "outputs/MEGATRON_HYBRID_DATA_MODEL/" + dnn_name + "_" + "{}".format(dp_degree) + "DP_" + "{}".format(mp_degree) + "MP_tpuv3_" + "{}".format(num_npus) + "_npu_analytical/" + \
                                                                                dnn_name + "_inp_grad_cycles.csv"
    path_to_dnn_weight_grad_cycles_csv = "outputs/MEGATRON_HYBRID_DATA_MODEL/" + dnn_name + "_" + "{}".format(dp_degree) + "DP_" + "{}".format(mp_degree) + "MP_tpuv3_" + "{}".format(num_npus) + "_npu_analytical/" + \
                                                                                dnn_name + "_weight_grad_cycles.csv"
else:
    path_to_dnn_fwd_cycles_csv = "outputs/MEGATRON_HYBRID_DATA_MODEL/" + dnn_name + "_" + "{}".format(dp_degree) + "DP_" + "{}".format(mp_degree) + "MP_tpuv3_" + "{}".format(num_npus) + "_npu/" + \
                                                                                dnn_name + "_fwd_pass_cycles.csv"
    path_to_dnn_inp_grad_cycles_csv = "outputs/MEGATRON_HYBRID_DATA_MODEL/" + dnn_name + "_" + "{}".format(dp_degree) + "DP_" + "{}".format(mp_degree) + "MP_tpuv3_" + "{}".format(num_npus) + "_npu/" + \
                                                                                dnn_name + "_inp_grad_cycles.csv"
    path_to_dnn_weight_grad_cycles_csv = "outputs/MEGATRON_HYBRID_DATA_MODEL/" + dnn_name + "_" + "{}".format(dp_degree) + "DP_" + "{}".format(mp_degree) + "MP_tpuv3_" + "{}".format(num_npus) + "_npu/" + \
                                                                                dnn_name + "_weight_grad_cycles.csv"    

path_to_npu_config_file = "../../extern/compute/SCALE-Sim-V1/SCALE-Sim/configs/google_smaller.cfg"
path_to_scalesim = "../../extern/compute/SCALE-Sim-V1/SCALE-Sim/"

path_to_output_csv = "fusion_outputs/" + dnn_name + "_" + "{}".format(dp_degree) + "DP_" + "{}".format(mp_degree) + "MP_tpuv3_" + \
                                                            "{}".format(num_npus) + "_npu_" + simulation_mode + ".csv"

# paths for fusion communication measurements
path_to_3d_torus_npu_system_config = "../../inputs/network/analytical/sample_Torus3D_fusion_comm.json"
###################################

def main():

    ### get list of layers from mnk file
    nn_layers = get_dnn_layers_from_mnk_csv(path_to_dnn_csv_file)

    ### create a ChipFusion object
    chip_fusion_obj = TransformerFusedChips(mp_degree, dp_degree, num_cores, num_mxu_cores, num_subarrays, path_to_npu_config_file, frequency, 
                                nw_bw, nw_latency, num_nw_links, fusion_pipeline_overhead_coeff)
    chip_fusion_obj.set_npu_dims_from_config_file()

    ### get ATTN and MLP layers, as they require different methods for parallelism
    attn_layers, mlp_layers = get_attn_mlp_layers(nn_layers)

    ### get layer topologies/comm sizes for fusion
    fusion_outputs = chip_fusion_obj.get_total_transformer_layer_toplogies_fusion_requirements(attn_layers, mlp_layers)

    fwd_pass_layers = fusion_outputs['fwd_pass_layers']
    inp_grad_layers = fusion_outputs['inp_grad_layers']
    weight_grad_layers = fusion_outputs['weight_grad_layers']
    fusion_config_dict = fusion_outputs['fusion_config_dict']
    layers_num_microbatch_dict = fusion_outputs['layers_num_microbatch_dict']
    layers_num_micobatch_wgt_grad_dict = fusion_outputs['layers_num_microbatch_wgt_grad_dict']
    fwd_layers_array_util_dict = fusion_outputs['fwd_layers_array_util_dict']
    inp_grad_layers_array_util_dict = fusion_outputs['inp_grad_layers_array_util_dict']
    wgt_grad_layers_array_util_dict  = fusion_outputs['wgt_grad_layers_array_util_dict']
    attn_layers_communication_size_per_microbatch_dict_fwd_inp_grad = fusion_outputs['attn_layers_communication_size_per_microbatch_dict_fwd_inp_grad']
    attn_layers_communication_size_per_microbatch_dict_wgt_grad = fusion_outputs['attn_layers_communication_size_per_microbatch_dict_wgt_grad']
    mlp_layers_communication_size_per_microbatch_dict_fwd_inp_grad = fusion_outputs['mlp_layers_communication_size_per_microbatch_dict_fwd_inp_grad']
    mlp_layers_communication_size_per_microbatch_dict_wgt_grad = fusion_outputs['mlp_layers_communication_size_per_microbatch_dict_wgt_grad']

    nn_layers_communication_size_per_microbatch_dict_fwd_inp_grad = {**attn_layers_communication_size_per_microbatch_dict_fwd_inp_grad, **mlp_layers_communication_size_per_microbatch_dict_fwd_inp_grad}
    nn_layers_communication_size_per_microbatch_dict_wgt_grad = {**attn_layers_communication_size_per_microbatch_dict_wgt_grad, **mlp_layers_communication_size_per_microbatch_dict_wgt_grad}


    ### get Fusion_Comm time 
    fusion_comm_obj = FusionCommMeasurement(run_name, attn_layers, mlp_layers, attn_layers_communication_size_per_microbatch_dict_fwd_inp_grad,
                                                    attn_layers_communication_size_per_microbatch_dict_wgt_grad, mlp_layers_communication_size_per_microbatch_dict_fwd_inp_grad,
                                                    mlp_layers_communication_size_per_microbatch_dict_wgt_grad,
                                                    nw_bw, nw_latency, num_nw_links,
                                                    path_to_3d_torus_npu_system_config, fusion_config_dict)

    fwd_layers_communication_time_dict, inp_grad_layers_communication_time_dict, wgt_grad_layers_communication_time_dict = \
                                                    fusion_comm_obj.get_fusion_comm_from_astrasim()

    ### get the number of heads per NPU based on the fusion config
    fusion_num_heads_per_npu = get_fusion_num_head_per_npu(nn_layers, fusion_config_dict, mp_degree)
    ### get scalesim cycles with fusion
    fwd_cycles_w_fusion, inp_grad_cycles_w_fusion, wgt_grad_cycles_w_fusion = get_cycles_w_fusion(fwd_pass_layers, inp_grad_layers, 
                                                                    weight_grad_layers, layers_num_microbatch_dict, layers_num_micobatch_wgt_grad_dict, simulation_mode,
                                                                    path_to_npu_config_file, frequency, mem_bw, mem_latency, fusion_num_heads_per_npu)
    
    ### get cycles wo fusion
    baseline_csv_layer_names, fwd_cycles_baseline, inp_grad_cycles_baseline, wgt_grad_cycles_baseline = get_baseline_cycles_wo_fusion(path_to_dnn_fwd_cycles_csv,
                                                                                path_to_dnn_inp_grad_cycles_csv, path_to_dnn_weight_grad_cycles_csv)
    

    ### get Fusion_comm and Computation overlap for each microbatch + Final Fwd/Inp_grad/Wgt_grad time
    fwd_fusion_comp_overlap, inp_grad_fusion_comp_overlap, wgt_grad_fusion_comp_overlap, \
    fwd_scatter_gather_time_overhead, inp_grad_scatter_gather_time_overhead, \
    fwd_final_time, inp_grad_final_time, wgt_grad_final_time = chip_fusion_obj.get_fusion_comp_overlap_and_final_layer_time(nn_layers, fwd_cycles_w_fusion,
                                                                                                        inp_grad_cycles_w_fusion, wgt_grad_cycles_w_fusion,  
                                                                                                        fwd_layers_communication_time_dict, 
                                                                                                        inp_grad_layers_communication_time_dict,
                                                                                                        wgt_grad_layers_communication_time_dict,
                                                                                                        layers_num_microbatch_dict, layers_num_micobatch_wgt_grad_dict, 
                                                                                                        fusion_config_dict)                                                                                                   

    ### write to csv
    output_csv_col = ['Layer Name', 'm', 'k', 'n', 'DP Degree', 'MP Degree', 'Fusion Rows', 'Fusion Cols', 
                                                        'Fwd Array Util w Fusion', 'Inp Grad Array Util w Fusion', 'Wgt Grad Array Util w Fusion',
                                                        'Num Microbatch', 'Wgt Grad Num Microbatch', 'Fwd cycles baseline', 'Fwd cycles w fusion', 'Inp Grad 1 cycles baseline',
                                                        'Inp Grad 1 cycles w fusion', 'Inp Grad 2 cycles baseline',
                                                        'Inp Grad 2 cycles w fusion', 'Wgt Grad cycles baseline', 'Wgt Grad cycles w fusion', 'Fwd input communication Size per microbatch',
                                                        'Fwd input Communication Time per microbatch', 'Fwd psum communication Size per microbatch', 'Fwd psum communication Time per microbatch', 
                                                        'Fwd Scatter/Gather Communication Size', 'Fwd Scatter/Gather Communication Time', 
                                                        'Inp Grad 1 input communication Size per microbatch', 'Inp Grad 1 input communication Time per microbatch',
                                                        'Inp Grad 1 psum communication Size per microbatch', 'Inp Grad 1 psum communication Time per microbatch',
                                                        'Inp Grad 2 input communication Size per microbatch', 'Inp Grad 2 input communication Time per microbatch',
                                                        'Inp Grad 2 psum communication Size per microbatch', 'Inp Grad 2 psum communication Time per microbatch',
                                                        'Inp Grad Scatter/Gather Communication Size', 'Inp Grad Scatter/Gather Communication Time', 
                                                        'Wgt Grad input communication Size per microbatch', 'Wgt Grad input communication Time per microbatch',
                                                        'Wgt Grad loss communication Size per microbatch', 'Wgt Grad loss communication Time per microbatch',
                                                        'Fwd Fusion-Comp Overlap', 'Inp Grad Fusion-Comp Overlap', 'Wgt Grad Fusion-Comp Overlap',
                                                        'Fwd Final Time w Fusion', 'Inp Grad Final Time w Fusion', 'Wgt Grad Final Time w Fusion']
    
    output_csv_df = pd.DataFrame(columns=output_csv_col)


    for idx, layer in enumerate(nn_layers):
        if layer.name == "GEMM_QK" or layer.name == "GEMM_QKV":
            inp_grad_1_communication_size_per_microbatch_input = nn_layers_communication_size_per_microbatch_dict_fwd_inp_grad[layer.name]['inp_grad_input_1']
            inp_grad_1_communication_time_per_microbatch_input = inp_grad_layers_communication_time_dict[layer.name]['inp_grad_input_1']
            inp_grad_1_communication_size_per_microbatch_psum = nn_layers_communication_size_per_microbatch_dict_fwd_inp_grad[layer.name]['inp_grad_psum_1']
            inp_grad_1_communication_time_per_microbatch_psum = inp_grad_layers_communication_time_dict[layer.name]['inp_grad_psum_1']
            inp_grad_2_communication_size_per_microbatch_input = nn_layers_communication_size_per_microbatch_dict_fwd_inp_grad[layer.name]['inp_grad_input_2']
            inp_grad_2_communication_time_per_microbatch_input = inp_grad_layers_communication_time_dict[layer.name]['inp_grad_input_2']
            inp_grad_2_communication_size_per_microbatch_psum = nn_layers_communication_size_per_microbatch_dict_fwd_inp_grad[layer.name]['inp_grad_psum_2']
            inp_grad_2_communication_time_per_microbatch_psum = inp_grad_layers_communication_time_dict[layer.name]['inp_grad_psum_2']
            wgt_grad_communication_size_per_microbatch_input = 0
            wgt_grad_communication_time_per_microbatch_input = 0          
            wgt_grad_communication_size_per_microbatch_psum = 0
            wgt_grad_communication_time_per_microbatch_psum = 0  

            fwd_scatter_gather_size = nn_layers_communication_size_per_microbatch_dict_fwd_inp_grad[layer.name]['fwd_scatter']
            if layer.name == "GEMM_QK":
                inp_grad_scatter_gather_size = nn_layers_communication_size_per_microbatch_dict_fwd_inp_grad[layer.name]['inp_grad_scatter']
            else:
                inp_grad_scatter_gather_size = nn_layers_communication_size_per_microbatch_dict_fwd_inp_grad[layer.name]['inp_grad_gather']
            
            # inp grad cycles
            _inp_grad_2_cycles_baseline = inp_grad_cycles_baseline[baseline_csv_layer_names.index(layer.name)]
            _inp_grad_2_cycles_w_fusion = inp_grad_cycles_w_fusion[layer.name + "_2"]
        
        else:
            inp_grad_1_communication_size_per_microbatch_input = nn_layers_communication_size_per_microbatch_dict_fwd_inp_grad[layer.name]['inp_grad_input']
            inp_grad_1_communication_time_per_microbatch_input = inp_grad_layers_communication_time_dict[layer.name]['inp_grad_input']
            inp_grad_1_communication_size_per_microbatch_psum = nn_layers_communication_size_per_microbatch_dict_fwd_inp_grad[layer.name]['inp_grad_psum']
            inp_grad_1_communication_time_per_microbatch_psum = inp_grad_layers_communication_time_dict[layer.name]['inp_grad_psum']
            inp_grad_2_communication_size_per_microbatch_input = 0
            inp_grad_2_communication_time_per_microbatch_input = 0
            inp_grad_2_communication_size_per_microbatch_psum = 0
            inp_grad_2_communication_time_per_microbatch_psum = 0

            wgt_grad_communication_size_per_microbatch_input = nn_layers_communication_size_per_microbatch_dict_wgt_grad[layer.name]['wgt_grad_input']
            wgt_grad_communication_time_per_microbatch_input = wgt_grad_layers_communication_time_dict[layer.name]['wgt_grad_input']          
            wgt_grad_communication_size_per_microbatch_psum = nn_layers_communication_size_per_microbatch_dict_wgt_grad[layer.name]['wgt_grad_loss']
            wgt_grad_communication_time_per_microbatch_psum = wgt_grad_layers_communication_time_dict[layer.name]['wgt_grad_loss']  
            fwd_scatter_gather_size = 0
            inp_grad_scatter_gather_size = 0 
        
            # inp grad cycles
            _inp_grad_2_cycles_baseline = 0
            _inp_grad_2_cycles_w_fusion = 0
            
        # wgt grad cycles
        if layer.name in wgt_grad_cycles_w_fusion.keys():
            _wgt_grad_cycles_w_fusion = wgt_grad_cycles_w_fusion[layer.name]
            _wgt_grad_cycles_baseline = wgt_grad_cycles_baseline[baseline_csv_layer_names.index(layer.name)]
        else:
            _wgt_grad_cycles_w_fusion = 0
            _wgt_grad_cycles_baseline = 0


        layer_info = [layer.name, layer.m, layer.k, layer.n, dp_degree, mp_degree, fusion_config_dict[layer.name][0], fusion_config_dict[layer.name][1],
                                                        fwd_layers_array_util_dict[layer.name], inp_grad_layers_array_util_dict[layer.name], wgt_grad_layers_array_util_dict[layer.name],
                                                        layers_num_microbatch_dict[layer.name], layers_num_micobatch_wgt_grad_dict[layer.name],
                                                        fwd_cycles_baseline[baseline_csv_layer_names.index(layer.name)], fwd_cycles_w_fusion[layer.name], inp_grad_cycles_baseline[baseline_csv_layer_names.index(layer.name)],
                                                        inp_grad_cycles_w_fusion[layer.name], _inp_grad_2_cycles_baseline, _inp_grad_2_cycles_w_fusion,
                                                        _wgt_grad_cycles_baseline, _wgt_grad_cycles_w_fusion,
                                                        nn_layers_communication_size_per_microbatch_dict_fwd_inp_grad[layer.name]['fwd_input'], fwd_layers_communication_time_dict[layer.name]['fwd_input'],
                                                        nn_layers_communication_size_per_microbatch_dict_fwd_inp_grad[layer.name]['fwd_psum'], fwd_layers_communication_time_dict[layer.name]['fwd_psum'],
                                                        fwd_scatter_gather_size, fwd_scatter_gather_time_overhead[layer.name],
                                                        inp_grad_1_communication_size_per_microbatch_input, inp_grad_1_communication_time_per_microbatch_input,
                                                        inp_grad_1_communication_size_per_microbatch_psum, inp_grad_1_communication_time_per_microbatch_psum,
                                                        inp_grad_2_communication_size_per_microbatch_input, inp_grad_2_communication_time_per_microbatch_input,
                                                        inp_grad_2_communication_size_per_microbatch_psum, inp_grad_2_communication_time_per_microbatch_psum,
                                                        inp_grad_scatter_gather_size, inp_grad_scatter_gather_time_overhead[layer.name],                      
                                                        wgt_grad_communication_size_per_microbatch_input, wgt_grad_communication_time_per_microbatch_input,
                                                        wgt_grad_communication_size_per_microbatch_psum, wgt_grad_communication_time_per_microbatch_psum,                                                         
                                                        fwd_fusion_comp_overlap[layer.name], inp_grad_fusion_comp_overlap[layer.name], wgt_grad_fusion_comp_overlap[layer.name],
                                                        fwd_final_time[layer.name], inp_grad_final_time[layer.name], wgt_grad_final_time[layer.name]]
    
        layer_info_series = pd.Series(layer_info, index=output_csv_col)
        output_csv_df = output_csv_df.append(layer_info_series, ignore_index=True)
    
    output_csv_df.to_csv(path_to_output_csv)

    return

class TransformerFusedChips(object):

    """
    This class models a virtually fused groups of chips
    """

    def __init__(self, num_fused_chips, dp_degree, num_cores_per_chip, num_mxu_cores_per_core, num_subarrays, path_to_npu_config_file, frequency, 
                                        nw_bw, nw_latency, num_nw_links, fusion_pipeline_overhead_coeff):      
        self.num_fused_chips = num_fused_chips
        self.dp_degree = dp_degree
        self.num_cores_per_chip = num_cores_per_chip
        self.num_mxu_cores_per_core = num_mxu_cores_per_core
        self.num_subarrays = num_subarrays
        self.path_to_npu_config_file = path_to_npu_config_file
        self.frequency = frequency
        self.nw_bw = nw_bw
        self.nw_latency = nw_latency
        self.num_nw_links = num_nw_links
        self.fusion_pipeline_overhead_coeff = fusion_pipeline_overhead_coeff

    def set_npu_dims_from_config_file(self):
        cfg_parser = cp.ConfigParser()
        cfg_parser.read(self.path_to_npu_config_file)

        self.sys_array_rows = int(cfg_parser.get("architecture_presets", "ArrayHeight"))
        self.sys_array_cols = int(cfg_parser.get("architecture_presets", "ArrayWidth"))

    def get_possible_fusion_configs(self):
        """
        This function finds the possible fusion configurations give the degree of MP
        """
        fusion_configs = []
        n = 1
        while n <= self.num_fused_chips:
            f_c = (n, int(self.num_fused_chips / n))
            fusion_configs.append(f_c)
            n *= 2        
        return fusion_configs

    def get_mlp_comm_size(self, layers):
        """
        This function returns the MLP comm size for fwd/inp_grad/wgt_grad pass
        """
        l_mlp_1 = layers[0]
        l_mlp_2 = layers[1]
        
        mlp_comm_size = {}
        mlp_comm_size['fwd_mlp_1_input_comm_size'] = math.ceil(l_mlp_1.m / self.dp_degree) * l_mlp_1.seq_length * l_mlp_1.k * input_data_type_size
        mlp_comm_size['fwd_mlp_1_psum_comm_size'] = math.ceil(l_mlp_1.m / self.dp_degree) * l_mlp_1.seq_length * l_mlp_1.n * psum_data_type_size
        mlp_comm_size['fwd_mlp_2_input_comm_size'] = math.ceil(l_mlp_2.m / self.dp_degree) * l_mlp_2.seq_length * l_mlp_2.k * input_data_type_size
        mlp_comm_size['fwd_mlp_2_psum_comm_size'] = math.ceil(l_mlp_2.m / self.dp_degree) * l_mlp_2.seq_length * l_mlp_2.n * psum_data_type_size

        mlp_comm_size['inp_grad_mlp_1_input_comm_size'] = math.ceil(l_mlp_1.m / self.dp_degree) * l_mlp_1.seq_length * l_mlp_1.n * input_data_type_size
        mlp_comm_size['inp_grad_mlp_1_psum_comm_size'] = math.ceil(l_mlp_1.m / self.dp_degree) * l_mlp_1.seq_length * l_mlp_1.k * psum_data_type_size
        mlp_comm_size['inp_grad_mlp_2_input_comm_size'] = math.ceil(l_mlp_2.m / self.dp_degree) * l_mlp_2.seq_length * l_mlp_2.n * input_data_type_size
        mlp_comm_size['inp_grad_mlp_2_psum_comm_size'] = math.ceil(l_mlp_2.m / self.dp_degree) * l_mlp_2.seq_length * l_mlp_2.k * psum_data_type_size  

        mlp_comm_size['wgt_grad_mlp_1_input_comm_size'] = math.ceil(l_mlp_1.m / self.dp_degree) * l_mlp_1.seq_length * l_mlp_1.k * input_data_type_size
        mlp_comm_size['wgt_grad_mlp_1_loss_comm_size'] = math.ceil(l_mlp_1.m / self.dp_degree) * l_mlp_1.seq_length * l_mlp_1.n * input_data_type_size
        mlp_comm_size['wgt_grad_mlp_2_input_comm_size'] = math.ceil(l_mlp_2.m / self.dp_degree) * l_mlp_2.seq_length * l_mlp_2.k * input_data_type_size
        mlp_comm_size['wgt_grad_mlp_2_loss_comm_size'] = math.ceil(l_mlp_2.m / self.dp_degree) * l_mlp_2.seq_length * l_mlp_2.n * input_data_type_size

        return mlp_comm_size
    
    def get_attn_comm_size(self, layers):
        """
        This fucntion returns the ATNN comm size for fwd/inp_grad/wgt_grad pass.
        This communication also includes the scatter ops required for attention operations
        """
        # have the layers individually
        l_q_gen = layers[0]
        l_k_gen = layers[1]
        l_v_gen = layers[2]
        l_qk = layers[3]
        l_qkv = layers[4]
        l_linear = layers[5]

        attn_comm_size = {}
        attn_comm_size['fwd_l_q_gen_input_comm_size'] = math.ceil(l_q_gen.m / self.dp_degree) * l_q_gen.seq_length * l_q_gen.k * input_data_type_size
        attn_comm_size['fwd_l_q_gen_psum_comm_size'] = math.ceil(l_q_gen.m / self.dp_degree) * l_q_gen.seq_length * l_q_gen.n * l_q_gen.num_heads * psum_data_type_size
        attn_comm_size['fwd_l_k_gen_input_comm_size'] = math.ceil(l_k_gen.m / self.dp_degree) * l_k_gen.seq_length * l_k_gen.k * input_data_type_size
        attn_comm_size['fwd_l_k_gen_psum_comm_size'] = math.ceil(l_k_gen.m / self.dp_degree) * l_k_gen.seq_length * l_k_gen.n * l_k_gen.num_heads * psum_data_type_size     
        attn_comm_size['fwd_l_v_gen_input_comm_size'] = math.ceil(l_v_gen.m / self.dp_degree) * l_v_gen.seq_length * l_v_gen.k * input_data_type_size
        attn_comm_size['fwd_l_v_gen_psum_comm_size'] = math.ceil(l_v_gen.m / self.dp_degree) * l_v_gen.seq_length * l_v_gen.n * l_v_gen.num_heads * psum_data_type_size 
        # scatter K 
        attn_comm_size['fwd_l_qk_input_scatter_comm_size'] = math.ceil(l_qk.m / self.dp_degree) * l_qk.k * l_qk.n * l_qk.num_heads * input_data_type_size
        attn_comm_size['fwd_l_qk_input_comm_size'] = math.ceil(l_qk.m / self.dp_degree) * l_qk.seq_length * l_qk.k * l_qk.num_heads * input_data_type_size
        attn_comm_size['fwd_l_qk_psum_comm_size'] = 0
        # scatter V
        attn_comm_size['fwd_l_qkv_input_scatter_comm_size'] = math.ceil(l_qkv.m / self.dp_degree) * l_qkv.k * l_qkv.n * l_qkv.num_heads * input_data_type_size
        attn_comm_size['fwd_l_qkv_input_comm_size'] = 0
        attn_comm_size['fwd_l_qkv_psum_comm_size'] = math.ceil(l_qkv.m / self.dp_degree) * l_qkv.seq_length * l_qkv.n * l_qkv.num_heads * psum_data_type_size 
        attn_comm_size['fwd_l_linear_input_comm_size'] = math.ceil(l_linear.m / self.dp_degree) * l_linear.seq_length * l_linear.k * input_data_type_size
        attn_comm_size['fwd_l_linear_psum_comm_size'] = math.ceil(l_linear.m / self.dp_degree) * l_linear.seq_length * l_linear.n * psum_data_type_size
        
        attn_comm_size['inp_grad_l_q_gen_input_comm_size'] = math.ceil(l_q_gen.m / self.dp_degree) * l_q_gen.seq_length * l_q_gen.n * l_q_gen.num_heads * input_data_type_size
        attn_comm_size['inp_grad_l_q_gen_psum_comm_size'] = math.ceil(l_q_gen.m / self.dp_degree) * l_q_gen.seq_length * l_q_gen.k * psum_data_type_size
        attn_comm_size['inp_grad_l_k_gen_input_comm_size'] = math.ceil(l_k_gen.m / self.dp_degree) * l_k_gen.seq_length * l_k_gen.n * l_k_gen.num_heads * input_data_type_size
        attn_comm_size['inp_grad_l_k_gen_psum_comm_size'] =  math.ceil(l_k_gen.m / self.dp_degree) * l_k_gen.seq_length * l_k_gen.k * psum_data_type_size      
        attn_comm_size['inp_grad_l_v_gen_input_comm_size'] = math.ceil(l_v_gen.m / self.dp_degree) * l_v_gen.seq_length * l_v_gen.n * l_v_gen.num_heads * input_data_type_size
        attn_comm_size['inp_grad_l_v_gen_psum_comm_size'] = math.ceil(l_v_gen.m / self.dp_degree) * l_v_gen.seq_length * l_v_gen.k * psum_data_type_size 
        # scatter Q  for K_grad computations
        attn_comm_size['inp_grad_l_qk_k_grad_input_scatter_comm_size'] = math.ceil(l_qk.m / self.dp_degree) * l_qk.k * l_qk.n * l_qk.num_heads * input_data_type_size 
        attn_comm_size['inp_grad_l_qk_q_grad_input_comm_size'] = 0
        attn_comm_size['inp_grad_l_qk_q_grad_psum_comm_size'] = math.ceil(l_qk.m / self.dp_degree) * l_qk.k * l_qk.n * l_qk.num_heads * psum_data_type_size 
        attn_comm_size['inp_grad_l_qk_k_grad_input_comm_size'] = 0
        attn_comm_size['inp_grad_l_qk_k_grad_psum_comm_size'] = math.ceil(l_qk.m / self.dp_degree) * l_qk.seq_length * l_qk.k * l_qk.num_heads * psum_data_type_size
        attn_comm_size['inp_grad_l_qkv_v_grad_input_comm_size'] = math.ceil(l_qkv.m / self.dp_degree) * l_qkv.seq_length * l_qkv.n * l_qkv.num_heads * input_data_type_size
        attn_comm_size['inp_grad_l_qkv_v_grad_psum_comm_size'] = 0
        attn_comm_size['inp_grad_l_qkv_input_comm_size'] = math.ceil(l_qkv.m / self.dp_degree) * l_qkv.seq_length * l_qkv.n * l_qkv.num_heads * input_data_type_size
        attn_comm_size['inp_grad_l_qkv_psum_comm_size'] = 0
        attn_comm_size['inp_grad_l_qkv_v_gather_comm_size'] = math.ceil(l_qkv.m / self.dp_degree) * l_qkv.seq_length * l_qkv.n * l_qkv.num_heads * input_data_type_size
        attn_comm_size['inp_grad_l_linear_input_comm_size'] = math.ceil(l_linear.m / self.dp_degree) * l_linear.seq_length * l_linear.n * input_data_type_size
        attn_comm_size['inp_grad_l_linear_psum_comm_size'] = math.ceil(l_linear.m / self.dp_degree) * l_linear.seq_length * l_linear.k * psum_data_type_size

        attn_comm_size['wgt_grad_l_q_gen_input_comm_size'] = math.ceil(l_q_gen.m / self.dp_degree) * l_q_gen.seq_length * l_q_gen.k * input_data_type_size
        attn_comm_size['wgt_grad_l_q_gen_loss_comm_size'] = math.ceil(l_q_gen.m / self.dp_degree) * l_q_gen.seq_length * l_q_gen.n * l_q_gen.num_heads * input_data_type_size
        attn_comm_size['wgt_grad_l_k_gen_input_comm_size'] = math.ceil(l_k_gen.m / self.dp_degree) * l_k_gen.seq_length * l_k_gen.k * input_data_type_size
        attn_comm_size['wgt_grad_l_k_gen_loss_comm_size'] = math.ceil(l_k_gen.m / self.dp_degree) * l_k_gen.seq_length * l_k_gen.n * l_k_gen.num_heads * input_data_type_size         
        attn_comm_size['wgt_grad_l_v_gen_input_comm_size'] = math.ceil(l_v_gen.m / self.dp_degree) * l_v_gen.seq_length * l_v_gen.k * input_data_type_size
        attn_comm_size['wgt_grad_l_v_gen_loss_comm_size'] = math.ceil(l_v_gen.m / self.dp_degree) * l_v_gen.seq_length * l_v_gen.n * l_v_gen.num_heads * input_data_type_size 
        attn_comm_size['wgt_grad_l_linear_input_comm_size'] = math.ceil(l_linear.m / self.dp_degree) * l_linear.seq_length * l_linear.k * input_data_type_size
        attn_comm_size['wgt_grad_l_linear_loss_comm_size'] = math.ceil(l_linear.m / self.dp_degree) * l_linear.seq_length * l_linear.n * input_data_type_size

        return attn_comm_size

    def get_mlp_optimal_fusion_dim(self, fusion_configs, mlp_comm_size):
        """
        This function finds the optimal fusion configuration for MLP layers, while its set globally across both layers
        the logic is to find the fusion configuration that minimizes the maximum comm size of the MLP for fwd/inp grad.
        The communication for wgt grad just follows the optimal fusion config that minimzes the fwd/inp grad
        """
        comm_dict = {}
        max_comm_dict = {}
        for f_c in fusion_configs:
            mlp_comm_size_w_fusion = []
            if f_c[0] == self.num_fused_chips:
                # fwd
                mlp_comm_size_w_fusion.append(0)
                mlp_comm_size_w_fusion.append(mlp_comm_size['fwd_mlp_1_psum_comm_size'] / f_c[1])
                mlp_comm_size_w_fusion.append(mlp_comm_size['fwd_mlp_2_input_comm_size'] / f_c[1])
                mlp_comm_size_w_fusion.append(0)
                # inp_grad
                mlp_comm_size_w_fusion.append(mlp_comm_size['inp_grad_mlp_1_input_comm_size'] / f_c[1])
                mlp_comm_size_w_fusion.append(0)
                mlp_comm_size_w_fusion.append(0)
                mlp_comm_size_w_fusion.append(mlp_comm_size['inp_grad_mlp_2_psum_comm_size'] / f_c[1])
            elif f_c[1] == self.num_fused_chips:
                # fwd
                mlp_comm_size_w_fusion.append(mlp_comm_size['fwd_mlp_1_input_comm_size'] / f_c[0])
                mlp_comm_size_w_fusion.append(0)
                mlp_comm_size_w_fusion.append(0)
                mlp_comm_size_w_fusion.append(mlp_comm_size['fwd_mlp_2_psum_comm_size'] / f_c[0])
                # inp_grad
                mlp_comm_size_w_fusion.append(0)
                mlp_comm_size_w_fusion.append(mlp_comm_size['inp_grad_mlp_1_psum_comm_size'] / f_c[0])
                mlp_comm_size_w_fusion.append(mlp_comm_size['inp_grad_mlp_2_input_comm_size'] / f_c[0])
                mlp_comm_size_w_fusion.append(0)
            else:
                # fwd
                mlp_comm_size_w_fusion.append(mlp_comm_size['fwd_mlp_1_input_comm_size'] / f_c[0])
                mlp_comm_size_w_fusion.append(mlp_comm_size['fwd_mlp_1_psum_comm_size'] / f_c[1])
                mlp_comm_size_w_fusion.append(mlp_comm_size['fwd_mlp_2_input_comm_size'] / f_c[1])
                mlp_comm_size_w_fusion.append(mlp_comm_size['fwd_mlp_2_psum_comm_size'] / f_c[0])
                # inp_grad
                mlp_comm_size_w_fusion.append(mlp_comm_size['inp_grad_mlp_1_input_comm_size'] / f_c[1])
                mlp_comm_size_w_fusion.append(mlp_comm_size['inp_grad_mlp_1_psum_comm_size'] / f_c[0])
                mlp_comm_size_w_fusion.append(mlp_comm_size['inp_grad_mlp_2_input_comm_size'] / f_c[0])
                mlp_comm_size_w_fusion.append(mlp_comm_size['inp_grad_mlp_2_psum_comm_size'] / f_c[1])
            
            comm_dict[f_c] = mlp_comm_size_w_fusion
            max_comm_dict[f_c] = max(mlp_comm_size_w_fusion)
        optimal_fusion_config = min(max_comm_dict, key=max_comm_dict.get)
        # optimal_fusion_config = (16, 32)
        _comm_size_for_optimal_fusion_config = comm_dict[optimal_fusion_config]

        # calculate the comm size for wgt grad based on the optimal fusion config
        _comm_size_for_optimal_fusion_config_wgt_grad = []
        if optimal_fusion_config[0] == self.num_fused_chips:
            _comm_size_for_optimal_fusion_config_wgt_grad.append(0)
            _comm_size_for_optimal_fusion_config_wgt_grad.append(mlp_comm_size['wgt_grad_mlp_1_loss_comm_size']/ optimal_fusion_config[1])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(mlp_comm_size['wgt_grad_mlp_2_input_comm_size'] / optimal_fusion_config[1])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(0)
        elif optimal_fusion_config[1] == self.num_fused_chips:
            _comm_size_for_optimal_fusion_config_wgt_grad.append(mlp_comm_size['wgt_grad_mlp_1_input_comm_size'] / optimal_fusion_config[0])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(0)
            _comm_size_for_optimal_fusion_config_wgt_grad.append(0)
            _comm_size_for_optimal_fusion_config_wgt_grad.append(mlp_comm_size['wgt_grad_mlp_2_loss_comm_size'] / optimal_fusion_config[0])
        else:
            _comm_size_for_optimal_fusion_config_wgt_grad.append(mlp_comm_size['wgt_grad_mlp_1_input_comm_size'] / optimal_fusion_config[0])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(mlp_comm_size['wgt_grad_mlp_1_loss_comm_size']/ optimal_fusion_config[1])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(mlp_comm_size['wgt_grad_mlp_2_input_comm_size'] / optimal_fusion_config[1])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(mlp_comm_size['wgt_grad_mlp_2_loss_comm_size'] / optimal_fusion_config[0])            

        ### prepare the final format of comm size for fwd/inp grad
        comm_size_for_optimal_fusion_config = []
        # mlp1
        comm_size_for_optimal_fusion_config_mlp1 = {}
        comm_size_for_optimal_fusion_config_mlp1['fwd_input'] = _comm_size_for_optimal_fusion_config[0]
        comm_size_for_optimal_fusion_config_mlp1['fwd_psum'] = _comm_size_for_optimal_fusion_config[1]
        comm_size_for_optimal_fusion_config_mlp1['inp_grad_input'] = _comm_size_for_optimal_fusion_config[4]
        comm_size_for_optimal_fusion_config_mlp1['inp_grad_psum'] = _comm_size_for_optimal_fusion_config[5]
        comm_size_for_optimal_fusion_config.append(comm_size_for_optimal_fusion_config_mlp1)
        # mlp2
        comm_size_for_optimal_fusion_config_mlp2 = {}
        comm_size_for_optimal_fusion_config_mlp2['fwd_input'] = _comm_size_for_optimal_fusion_config[2]
        comm_size_for_optimal_fusion_config_mlp2['fwd_psum'] = _comm_size_for_optimal_fusion_config[3]
        comm_size_for_optimal_fusion_config_mlp2['inp_grad_input'] = _comm_size_for_optimal_fusion_config[6]
        comm_size_for_optimal_fusion_config_mlp2['inp_grad_psum'] = _comm_size_for_optimal_fusion_config[7]
        comm_size_for_optimal_fusion_config.append(comm_size_for_optimal_fusion_config_mlp2)

        ### prepare the final format of comm size for wgt grad
        comm_size_for_optimal_fusion_config_wgt_grad = []
        # mlp1
        comm_size_for_optimal_fusion_config_mlp1_wgt_grad = {}
        comm_size_for_optimal_fusion_config_mlp1_wgt_grad['wgt_grad_input'] = _comm_size_for_optimal_fusion_config_wgt_grad[0]
        comm_size_for_optimal_fusion_config_mlp1_wgt_grad['wgt_grad_loss'] = _comm_size_for_optimal_fusion_config_wgt_grad[1]
        comm_size_for_optimal_fusion_config_wgt_grad.append(comm_size_for_optimal_fusion_config_mlp1_wgt_grad)
        # mlp2
        comm_size_for_optimal_fusion_config_mlp2_wgt_grad = {}
        comm_size_for_optimal_fusion_config_mlp2_wgt_grad['wgt_grad_input'] = _comm_size_for_optimal_fusion_config_wgt_grad[2]
        comm_size_for_optimal_fusion_config_mlp2_wgt_grad['wgt_grad_loss'] = _comm_size_for_optimal_fusion_config_wgt_grad[3]
        comm_size_for_optimal_fusion_config_wgt_grad.append(comm_size_for_optimal_fusion_config_mlp2_wgt_grad)

        return optimal_fusion_config, comm_size_for_optimal_fusion_config, comm_size_for_optimal_fusion_config_wgt_grad
    
    def get_attn_optimal_fusion_dim(self, fusion_configs, attn_comm_size):
        """
        This function finds the optimal fusion configuration for Attention layers, while its set globally across the whole layers in attention.
        the logic is to find the fusion configuration that minimizes the maximum comm size of the Attention for fwd/inp grad.
        The communication for wgt grad just follows the optimal fusion config that minimzes the fwd/inp grad
        """
        comm_dict = {}
        max_comm_dict = {}
        for f_c in fusion_configs:
            attn_comm_size_w_fusion = []
            if f_c[0] == self.num_fused_chips:
                ### fwd
                # GEN-Q/K/V
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_q_gen_psum_comm_size'] / f_c[1])
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_k_gen_psum_comm_size'] / f_c[1])
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_v_gen_psum_comm_size'] / f_c[1])
                # QK
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_qk_input_scatter_comm_size'] / f_c[1])
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_qk_input_comm_size'] / f_c[1])
                attn_comm_size_w_fusion.append(0)
                # QKV
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_qkv_input_scatter_comm_size'] / f_c[1])
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_qkv_psum_comm_size'] / f_c[1])
                # Linear
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_linear_input_comm_size'] / f_c[1])
                attn_comm_size_w_fusion.append(0) 
                ### inp grad
                # GEN-Q/K/V
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_q_gen_input_comm_size'] / f_c[1])
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_k_gen_input_comm_size'] / f_c[1])
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_v_gen_input_comm_size'] / f_c[1])
                attn_comm_size_w_fusion.append(0)
                # QK
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qk_k_grad_input_scatter_comm_size'] / f_c[1])
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qk_q_grad_psum_comm_size'] / f_c[1])
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qk_k_grad_psum_comm_size'] / f_c[1])
                # QKV
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qkv_v_grad_input_comm_size'] / f_c[1])
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qkv_input_comm_size'] / f_c[1])
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qkv_v_gather_comm_size'] / f_c[1])
                # Linear
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_linear_psum_comm_size'] / f_c[1])                                             
            elif f_c[1] == self.num_fused_chips:
                ### fwd
                # GEN-Q/K/V
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_q_gen_input_comm_size'] / f_c[0])
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_k_gen_input_comm_size'] / f_c[0])
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_v_gen_input_comm_size'] / f_c[0])
                attn_comm_size_w_fusion.append(0)
                # QK
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_qk_psum_comm_size'] / f_c[0])
                # QKV
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_qkv_input_comm_size'] / f_c[0])
                attn_comm_size_w_fusion.append(0)
                # Linear
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_linear_psum_comm_size'] / f_c[0]) 
                ### inp grad
                # GEN-Q/K/V
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_q_gen_psum_comm_size'] / f_c[0])
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_k_gen_psum_comm_size'] / f_c[0])
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_v_gen_psum_comm_size'] / f_c[0])
                # QK
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qk_q_grad_input_comm_size'] / f_c[0])
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qk_k_grad_input_comm_size'] / f_c[0])
                attn_comm_size_w_fusion.append(0)
                # QKV
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qkv_v_grad_psum_comm_size'] / f_c[0])
                attn_comm_size_w_fusion.append(0)
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qkv_psum_comm_size'] / f_c[0])
                attn_comm_size_w_fusion.append(0) 
                # Linear
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_linear_input_comm_size'] / f_c[0])
                attn_comm_size_w_fusion.append(0)    
            else:
                ### fwd
                # GEN-Q/K/V
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_q_gen_input_comm_size'] / f_c[0]) # 0
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_q_gen_psum_comm_size'] / f_c[1]) # 1
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_k_gen_input_comm_size'] / f_c[0]) # 2
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_k_gen_psum_comm_size'] / f_c[1]) # 3
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_v_gen_input_comm_size'] / f_c[0]) # 4
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_v_gen_psum_comm_size'] / f_c[1]) # 5
                # QK
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_qk_input_scatter_comm_size'] / f_c[1]) # 6
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_qk_input_comm_size'] / f_c[1]) # 7
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_qk_psum_comm_size'] / f_c[0]) # 8
                # QKV
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_qkv_input_scatter_comm_size'] / f_c[1]) # 9
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_qkv_input_comm_size'] / f_c[0]) # 10
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_qkv_psum_comm_size'] / f_c[1]) # 11
                # Linear
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_linear_input_comm_size'] / f_c[1]) # 12
                attn_comm_size_w_fusion.append(attn_comm_size['fwd_l_linear_psum_comm_size'] / f_c[0]) # 13
                ### inp grad
                # GEN-Q/K/V
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_q_gen_input_comm_size'] / f_c[1]) # 14
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_q_gen_psum_comm_size'] / f_c[0]) # 15
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_k_gen_input_comm_size'] / f_c[1]) # 16
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_k_gen_psum_comm_size'] / f_c[0]) # 17
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_v_gen_input_comm_size'] / f_c[1]) # 18
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_v_gen_psum_comm_size'] / f_c[0]) # 19
                # QK
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qk_k_grad_input_scatter_comm_size'] / f_c[1]) # 20
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qk_q_grad_input_comm_size'] / f_c[0]) # 21
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qk_q_grad_psum_comm_size'] / f_c[1]) # 22
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qk_k_grad_input_comm_size'] / f_c[0]) # 23
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qk_k_grad_psum_comm_size'] / f_c[1]) # 24
                # QKV
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qkv_v_grad_input_comm_size'] / f_c[1]) # 25
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qkv_v_grad_psum_comm_size'] / f_c[0]) # 26
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qkv_input_comm_size'] / f_c[1]) # 27
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qkv_psum_comm_size'] / f_c[0]) # 28
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_qkv_v_gather_comm_size'] / f_c[1]) # 29
                # Linear
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_linear_input_comm_size'] / f_c[0]) # 30
                attn_comm_size_w_fusion.append(attn_comm_size['inp_grad_l_linear_psum_comm_size'] / f_c[1])  # 31                                           

            comm_dict[f_c] = attn_comm_size_w_fusion
            max_comm_dict[f_c] = max(attn_comm_size_w_fusion)
        optimal_fusion_config = min(max_comm_dict, key=max_comm_dict.get)
        # optimal_fusion_config = (16, 32)
        _comm_size_for_optimal_fusion_config = comm_dict[optimal_fusion_config]

        # calculate the comm size for wgt grad based on the optimal fusion config
        _comm_size_for_optimal_fusion_config_wgt_grad = []
        if optimal_fusion_config[0] == self.num_fused_chips:
            # GEN-Q/K/V
            _comm_size_for_optimal_fusion_config_wgt_grad.append(0)
            _comm_size_for_optimal_fusion_config_wgt_grad.append(attn_comm_size['wgt_grad_l_q_gen_loss_comm_size'] / optimal_fusion_config[1])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(0)
            _comm_size_for_optimal_fusion_config_wgt_grad.append(attn_comm_size['wgt_grad_l_k_gen_loss_comm_size'] / optimal_fusion_config[1])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(0)
            _comm_size_for_optimal_fusion_config_wgt_grad.append(attn_comm_size['wgt_grad_l_v_gen_loss_comm_size'] / optimal_fusion_config[1])
            # Linear
            _comm_size_for_optimal_fusion_config_wgt_grad.append(attn_comm_size['wgt_grad_l_linear_input_comm_size'] / optimal_fusion_config[1])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(0)           

        elif optimal_fusion_config[1] == self.num_fused_chips:
            # GEN-Q/K/V
            _comm_size_for_optimal_fusion_config_wgt_grad.append(attn_comm_size['wgt_grad_l_q_gen_input_comm_size'] / optimal_fusion_config[0])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(0)
            _comm_size_for_optimal_fusion_config_wgt_grad.append(attn_comm_size['wgt_grad_l_k_gen_input_comm_size'] / optimal_fusion_config[0])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(0)
            _comm_size_for_optimal_fusion_config_wgt_grad.append(attn_comm_size['wgt_grad_l_v_gen_input_comm_size'] / optimal_fusion_config[0])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(0)
            # Linear
            _comm_size_for_optimal_fusion_config_wgt_grad.append(0)
            _comm_size_for_optimal_fusion_config_wgt_grad.append(attn_comm_size['wgt_grad_l_linear_loss_comm_size'] / optimal_fusion_config[0])   
        else:
            # GEN-Q/K/V
            _comm_size_for_optimal_fusion_config_wgt_grad.append(attn_comm_size['wgt_grad_l_q_gen_input_comm_size'] / optimal_fusion_config[0])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(attn_comm_size['wgt_grad_l_q_gen_loss_comm_size'] / optimal_fusion_config[1])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(attn_comm_size['wgt_grad_l_k_gen_input_comm_size'] / optimal_fusion_config[0])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(attn_comm_size['wgt_grad_l_k_gen_loss_comm_size'] / optimal_fusion_config[1])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(attn_comm_size['wgt_grad_l_v_gen_input_comm_size'] / optimal_fusion_config[0])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(attn_comm_size['wgt_grad_l_v_gen_loss_comm_size'] / optimal_fusion_config[1])
            # Linear
            _comm_size_for_optimal_fusion_config_wgt_grad.append(attn_comm_size['wgt_grad_l_linear_input_comm_size'] / optimal_fusion_config[1])
            _comm_size_for_optimal_fusion_config_wgt_grad.append(attn_comm_size['wgt_grad_l_linear_loss_comm_size'] / optimal_fusion_config[0])            

        ### prepare the final format of comm size for fwd/inp grad
        comm_size_for_optimal_fusion_config = []
        # GEN-Q
        comm_size_for_optimal_fusion_config_q_gen = {}
        comm_size_for_optimal_fusion_config_q_gen['fwd_input'] = _comm_size_for_optimal_fusion_config[0]
        comm_size_for_optimal_fusion_config_q_gen['fwd_psum'] = _comm_size_for_optimal_fusion_config[1]
        comm_size_for_optimal_fusion_config_q_gen['inp_grad_input'] = _comm_size_for_optimal_fusion_config[14]
        comm_size_for_optimal_fusion_config_q_gen['inp_grad_psum'] = _comm_size_for_optimal_fusion_config[15]
        comm_size_for_optimal_fusion_config.append(comm_size_for_optimal_fusion_config_q_gen)
        # GEN-K
        comm_size_for_optimal_fusion_config_k_gen = {}
        comm_size_for_optimal_fusion_config_k_gen['fwd_input'] = _comm_size_for_optimal_fusion_config[2]
        comm_size_for_optimal_fusion_config_k_gen['fwd_psum'] = _comm_size_for_optimal_fusion_config[3]
        comm_size_for_optimal_fusion_config_k_gen['inp_grad_input'] = _comm_size_for_optimal_fusion_config[16]
        comm_size_for_optimal_fusion_config_k_gen['inp_grad_psum'] = _comm_size_for_optimal_fusion_config[17]
        comm_size_for_optimal_fusion_config.append(comm_size_for_optimal_fusion_config_k_gen)
        # GEN-V
        comm_size_for_optimal_fusion_config_v_gen = {}
        comm_size_for_optimal_fusion_config_v_gen['fwd_input'] = _comm_size_for_optimal_fusion_config[4]
        comm_size_for_optimal_fusion_config_v_gen['fwd_psum'] = _comm_size_for_optimal_fusion_config[5]
        comm_size_for_optimal_fusion_config_v_gen['inp_grad_input'] = _comm_size_for_optimal_fusion_config[18]
        comm_size_for_optimal_fusion_config_v_gen['inp_grad_psum'] = _comm_size_for_optimal_fusion_config[19]
        comm_size_for_optimal_fusion_config.append(comm_size_for_optimal_fusion_config_v_gen)
        # QK
        comm_size_for_optimal_fusion_config_qk = {}
        comm_size_for_optimal_fusion_config_qk['fwd_scatter'] = _comm_size_for_optimal_fusion_config[6]
        comm_size_for_optimal_fusion_config_qk['fwd_input'] = _comm_size_for_optimal_fusion_config[7]
        comm_size_for_optimal_fusion_config_qk['fwd_psum'] = _comm_size_for_optimal_fusion_config[8]
        comm_size_for_optimal_fusion_config_qk['inp_grad_scatter'] = _comm_size_for_optimal_fusion_config[20]
        comm_size_for_optimal_fusion_config_qk['inp_grad_input_1'] = _comm_size_for_optimal_fusion_config[21]
        comm_size_for_optimal_fusion_config_qk['inp_grad_psum_1'] = _comm_size_for_optimal_fusion_config[22]
        comm_size_for_optimal_fusion_config_qk['inp_grad_input_2'] = _comm_size_for_optimal_fusion_config[23]
        comm_size_for_optimal_fusion_config_qk['inp_grad_psum_2'] = _comm_size_for_optimal_fusion_config[24]
        comm_size_for_optimal_fusion_config.append(comm_size_for_optimal_fusion_config_qk)
        # QKV
        comm_size_for_optimal_fusion_config_qkv = {}
        comm_size_for_optimal_fusion_config_qkv['fwd_scatter'] = _comm_size_for_optimal_fusion_config[9]
        comm_size_for_optimal_fusion_config_qkv['fwd_input'] = _comm_size_for_optimal_fusion_config[10]
        comm_size_for_optimal_fusion_config_qkv['fwd_psum'] = _comm_size_for_optimal_fusion_config[11]
        comm_size_for_optimal_fusion_config_qkv['inp_grad_input_1'] = _comm_size_for_optimal_fusion_config[25]
        comm_size_for_optimal_fusion_config_qkv['inp_grad_psum_1'] = _comm_size_for_optimal_fusion_config[26]
        comm_size_for_optimal_fusion_config_qkv['inp_grad_input_2'] = _comm_size_for_optimal_fusion_config[27]
        comm_size_for_optimal_fusion_config_qkv['inp_grad_psum_2'] = _comm_size_for_optimal_fusion_config[28]
        comm_size_for_optimal_fusion_config_qkv['inp_grad_gather'] = _comm_size_for_optimal_fusion_config[29]
        comm_size_for_optimal_fusion_config.append(comm_size_for_optimal_fusion_config_qkv)
        # Linear
        comm_size_for_optimal_fusion_config_linear = {}
        comm_size_for_optimal_fusion_config_linear['fwd_input'] = _comm_size_for_optimal_fusion_config[12]
        comm_size_for_optimal_fusion_config_linear['fwd_psum'] = _comm_size_for_optimal_fusion_config[13]
        comm_size_for_optimal_fusion_config_linear['inp_grad_input'] = _comm_size_for_optimal_fusion_config[30]
        comm_size_for_optimal_fusion_config_linear['inp_grad_psum'] = _comm_size_for_optimal_fusion_config[31]
        comm_size_for_optimal_fusion_config.append(comm_size_for_optimal_fusion_config_linear)

        ### prepare the final format of comm size for wgt grad
        comm_size_for_optimal_fusion_config_wgt_grad = {}
        # GEN-Q
        comm_size_for_optimal_fusion_config_q_gen_wgt_grad = {}
        comm_size_for_optimal_fusion_config_q_gen_wgt_grad['wgt_grad_input'] = _comm_size_for_optimal_fusion_config_wgt_grad[0]
        comm_size_for_optimal_fusion_config_q_gen_wgt_grad['wgt_grad_loss'] = _comm_size_for_optimal_fusion_config_wgt_grad[1]
        comm_size_for_optimal_fusion_config_wgt_grad['GEMM_Q_GEN'] = comm_size_for_optimal_fusion_config_q_gen_wgt_grad
        # GEN-K
        comm_size_for_optimal_fusion_config_k_gen_wgt_grad = {}
        comm_size_for_optimal_fusion_config_k_gen_wgt_grad['wgt_grad_input'] = _comm_size_for_optimal_fusion_config_wgt_grad[2]
        comm_size_for_optimal_fusion_config_k_gen_wgt_grad['wgt_grad_loss'] = _comm_size_for_optimal_fusion_config_wgt_grad[3]
        comm_size_for_optimal_fusion_config_wgt_grad['GEMM_K_GEN'] = comm_size_for_optimal_fusion_config_k_gen_wgt_grad
        # GEN-V
        comm_size_for_optimal_fusion_config_v_gen_wgt_grad = {}
        comm_size_for_optimal_fusion_config_v_gen_wgt_grad['wgt_grad_input'] = _comm_size_for_optimal_fusion_config_wgt_grad[4]
        comm_size_for_optimal_fusion_config_v_gen_wgt_grad['wgt_grad_loss'] = _comm_size_for_optimal_fusion_config_wgt_grad[5]
        comm_size_for_optimal_fusion_config_wgt_grad['GEMM_V_GEN'] = comm_size_for_optimal_fusion_config_v_gen_wgt_grad
        # Linear
        comm_size_for_optimal_fusion_config_linear_wgt_grad = {}
        comm_size_for_optimal_fusion_config_linear_wgt_grad['wgt_grad_input'] = _comm_size_for_optimal_fusion_config_wgt_grad[6]
        comm_size_for_optimal_fusion_config_linear_wgt_grad['wgt_grad_loss'] = _comm_size_for_optimal_fusion_config_wgt_grad[7]
        comm_size_for_optimal_fusion_config_wgt_grad['GEMM_Linear'] = comm_size_for_optimal_fusion_config_linear_wgt_grad

        return optimal_fusion_config, comm_size_for_optimal_fusion_config, comm_size_for_optimal_fusion_config_wgt_grad
    
    def get_mlp_fusion_configs(self, optimal_fusion_config, layers):
        mlp_fusion_config_dict = {}
        mlp_fusion_config_dict[layers[0].name] = optimal_fusion_config
        mlp_fusion_config_dict[layers[1].name] = (optimal_fusion_config[1], optimal_fusion_config[0])

        return mlp_fusion_config_dict 

    def get_attn_fusion_configs(self, optimal_fusion_config, layers):
        attn_fusion_config_dict = {}
        # GEN-Q/K/V
        attn_fusion_config_dict[layers[0].name] = optimal_fusion_config
        attn_fusion_config_dict[layers[1].name] = optimal_fusion_config
        attn_fusion_config_dict[layers[2].name] = optimal_fusion_config
        # QK
        attn_fusion_config_dict[layers[3].name] = (1, optimal_fusion_config[0])
        # QKV
        attn_fusion_config_dict[layers[4].name] = (optimal_fusion_config[0], 1)     
        # Linear        
        attn_fusion_config_dict[layers[5].name] = (optimal_fusion_config[1], optimal_fusion_config[0])

        return attn_fusion_config_dict 

    def get_layer_wise_num_microbatch_for_fusion(self, layer, fusion_dim):
        """
        This function finds the number of compute tiles for microbatching that satisfies the fusion pipeline overhead
        This is used for fwd pass and inp grad pass
        """

        # *2 is due to the pipeline overhead of 1 mb comp + comm
        fusion_pipeline_delay = ((fusion_dim[0] - 1) + (fusion_dim[1] - 1)) * 2
        candidate_num_microbatch = math.ceil(fusion_pipeline_delay / self.fusion_pipeline_overhead_coeff)
        m_dim_divisors = list(divisorGenerator(layer.m * layer.seq_length))
        num_microbatch = get_the_minimum_greater_than_element_of_list(m_dim_divisors, candidate_num_microbatch)
        
        return num_microbatch

    def get_layer_wise_num_microbatch_for_fusion_wgt_grad(self, layer, fusion_dim):
        """
        This function finds the number of compute tiles for microbatching that satisfies the fusion pipeline overhead
        This is used for wgt grad grad pass
        """

        # *2 is not used here, since the comm are not based on psum of computations. 
        fusion_pipeline_delay = ((fusion_dim[0] - 1) + (fusion_dim[1] - 1)) * 1
        candidate_num_microbatch = math.ceil(fusion_pipeline_delay / self.fusion_pipeline_overhead_coeff)
        # we need to use a shared dimension for micorbatching
        # this dimension creates partial sums
        m_dim_divisors = list(divisorGenerator(layer.m * layer.seq_length))
        num_microbatch = get_the_minimum_greater_than_element_of_list(m_dim_divisors, candidate_num_microbatch)
        
        return num_microbatch

    def calculate_array_utulization_with_fusion_mlp(self, layer, fusion_dim, num_microbatch, wgt_grad_num_microbatch):
        """
        This function calcualtes the systolic array utilization based on the fusion configuration to see if this layer benefits from fission or not.
        This function is specific to MLP layers
        """
        fwd_pass_dim, inp_grad_dim, wgt_grad_dim = self.get_mlp_layer_dim_per_core_per_fused_chip(fusion_dim, num_microbatch, wgt_grad_num_microbatch, layer)

        fwd_array_utilization = (fwd_pass_dim['k'] / (math.ceil(fwd_pass_dim['k']/self.sys_array_rows) * self.sys_array_rows)) * \
                                (fwd_pass_dim['n'] / (math.ceil(fwd_pass_dim['n']/self.sys_array_cols) * self.sys_array_cols))
        inp_grad_array_utilization = (inp_grad_dim['k'] / (math.ceil(inp_grad_dim['k']/self.sys_array_rows) * self.sys_array_rows)) * \
                                (inp_grad_dim['n'] / (math.ceil(inp_grad_dim['n']/self.sys_array_cols) * self.sys_array_cols))
        wgt_grad_array_utilization = (wgt_grad_dim['k'] / (math.ceil(wgt_grad_dim['k']/self.sys_array_rows) * self.sys_array_rows)) * \
                                (wgt_grad_dim['n'] / (math.ceil(wgt_grad_dim['n']/self.sys_array_cols) * self.sys_array_cols))
        
        return fwd_array_utilization, inp_grad_array_utilization, wgt_grad_array_utilization

    def calculate_array_utulization_with_fusion_attn(self, layer, fusion_dim, num_microbatch, wgt_grad_num_microbatch):
        """
        This function calcualtes the systolic array utilization based on the fusion configuration to see if this layer benefits from fission or not.
        This function is specific to ATTN layers
        """
        fwd_pass_dim, inp_grad_dim_1, inp_grad_dim_2, wgt_grad_dim = self.get_attn_layer_dim_per_core_per_fused_chip(fusion_dim, num_microbatch, wgt_grad_num_microbatch, layer)

        fwd_array_utilization = (fwd_pass_dim['k'] / (math.ceil(fwd_pass_dim['k']/self.sys_array_rows) * self.sys_array_rows)) * \
                                (fwd_pass_dim['n'] / (math.ceil(fwd_pass_dim['n']/self.sys_array_cols) * self.sys_array_cols))
        inp_grad_1_array_utilization = (inp_grad_dim_1['k'] / (math.ceil(inp_grad_dim_1['k']/self.sys_array_rows) * self.sys_array_rows)) * \
                                (inp_grad_dim_1['n'] / (math.ceil(inp_grad_dim_1['n']/self.sys_array_cols) * self.sys_array_cols))
        if inp_grad_dim_2 == None:
            inp_grad_2_array_utilization = None
        else:
            inp_grad_2_array_utilization = (inp_grad_dim_2['k'] / (math.ceil(inp_grad_dim_2['k']/self.sys_array_rows) * self.sys_array_rows)) * \
                                    (inp_grad_dim_2['n'] / (math.ceil(inp_grad_dim_2['n']/self.sys_array_cols) * self.sys_array_cols))
        if wgt_grad_dim == None:
            wgt_grad_array_utilization = None
        else:
            wgt_grad_array_utilization = (wgt_grad_dim['k'] / (math.ceil(wgt_grad_dim['k']/self.sys_array_rows) * self.sys_array_rows)) * \
                                    (wgt_grad_dim['n'] / (math.ceil(wgt_grad_dim['n']/self.sys_array_cols) * self.sys_array_cols))
        
        return fwd_array_utilization, inp_grad_1_array_utilization, inp_grad_2_array_utilization, wgt_grad_array_utilization


    def get_mlp_layer_dim_per_core_per_fused_chip(self, fused_dim, num_microbatch, wgt_grad_num_microbatch, layer_obj):
        """
        This function calculates the dimensions of the MLP layers based on the DP_degree and fusion dimensions for Fwd/Inp_grad/Wgt_grad on each NPU systolic array core
        """
        fwd_pass_dim = {}
        inp_grad_dim = {}
        wgt_grad_dim = {}

        fwd_pass_dim['m'] = math.ceil(layer_obj.m * layer_obj.seq_length / self.dp_degree / self.num_cores_per_chip / self.num_mxu_cores_per_core / num_microbatch)
        fwd_pass_dim['k'] = math.ceil(layer_obj.k / fused_dim[0])
        fwd_pass_dim['n'] = math.ceil(layer_obj.n / fused_dim[1])

        inp_grad_dim['m'] = math.ceil(layer_obj.m * layer_obj.seq_length / self.dp_degree / self.num_cores_per_chip / self.num_mxu_cores_per_core / num_microbatch)
        inp_grad_dim['k'] = math.ceil(layer_obj.n / fused_dim[1])
        inp_grad_dim['n'] = math.ceil(layer_obj.k / fused_dim[0])

        wgt_grad_dim['m'] = math.ceil(layer_obj.k / fused_dim[0] / self.num_cores_per_chip / self.num_mxu_cores_per_core)
        wgt_grad_dim['k'] = math.ceil(layer_obj.m * layer_obj.seq_length / self.dp_degree / wgt_grad_num_microbatch)
        wgt_grad_dim['n'] = math.ceil(layer_obj.n / fused_dim[1])

        return fwd_pass_dim, inp_grad_dim, wgt_grad_dim

    def get_attn_layer_dim_per_core_per_fused_chip(self, fused_dim, num_microbatch, wgt_grad_num_microbatch, layer_obj):
        """
        This function calculates the dimensions of the ATTN layers based on the DP_degree and fusion dimensions for Fwd/Inp_grad/Wgt_grad on each NPU systolic array core
        """
        fwd_pass_dim = {}
        # QK, QKV Attn layers require two inp_grad calculations
        inp_grad_dim_1 = {}
        inp_grad_dim_2 = {}
        wgt_grad_dim = {}

        if layer_obj.name == "GEMM_QK":
            fwd_pass_dim['m'] = math.ceil(layer_obj.m * layer_obj.seq_length / self.dp_degree / self.num_cores_per_chip / self.num_mxu_cores_per_core / num_microbatch)
            fwd_pass_dim['k'] = math.ceil(layer_obj.k / 1)
            fwd_pass_dim['n'] = math.ceil(layer_obj.n / fused_dim[1])

            inp_grad_dim_1['m'] = math.ceil(layer_obj.m * layer_obj.seq_length / self.dp_degree / self.num_cores_per_chip / self.num_mxu_cores_per_core / num_microbatch)
            inp_grad_dim_1['k'] = math.ceil(layer_obj.n / fused_dim[1])
            inp_grad_dim_1['n'] = math.ceil(layer_obj.k / 1)

            inp_grad_dim_2['m'] = math.ceil(layer_obj.m * layer_obj.seq_length / self.dp_degree / self.num_cores_per_chip / self.num_mxu_cores_per_core / num_microbatch)
            inp_grad_dim_2['k'] = math.ceil(layer_obj.n / fused_dim[1])
            inp_grad_dim_2['n'] = math.ceil(layer_obj.k / 1)

            wgt_grad_dim = None      

        elif layer_obj.name == "GEMM_QKV":
            fwd_pass_dim['m'] = math.ceil(layer_obj.m * layer_obj.seq_length / self.dp_degree / self.num_cores_per_chip / self.num_mxu_cores_per_core / num_microbatch)
            fwd_pass_dim['k'] = math.ceil(layer_obj.k / fused_dim[0])
            fwd_pass_dim['n'] = math.ceil(layer_obj.n / 1)

            inp_grad_dim_1['m'] = math.ceil(layer_obj.n / self.num_cores_per_chip / self.num_mxu_cores_per_core)
            inp_grad_dim_1['k'] = math.ceil(math.ceil(layer_obj.m * layer_obj.seq_length / self.dp_degree / num_microbatch))
            inp_grad_dim_1['n'] = math.ceil(layer_obj.k / fused_dim[0])

            inp_grad_dim_2['m'] = math.ceil(layer_obj.m * layer_obj.seq_length / self.dp_degree / self.num_cores_per_chip / self.num_mxu_cores_per_core / num_microbatch)
            inp_grad_dim_2['k'] = math.ceil(layer_obj.n / 1)
            inp_grad_dim_2['n'] = math.ceil(layer_obj.k / fused_dim[0])

            wgt_grad_dim = None 

        elif layer_obj.name == "GEMM_Linear":
            fwd_pass_dim['m'] = math.ceil(layer_obj.m * layer_obj.seq_length / self.dp_degree / self.num_cores_per_chip / self.num_mxu_cores_per_core / num_microbatch)
            fwd_pass_dim['k'] = math.ceil(layer_obj.k / fused_dim[0])
            fwd_pass_dim['n'] = math.ceil(layer_obj.n / fused_dim[1])

            inp_grad_dim_1['m'] = math.ceil(layer_obj.m * layer_obj.seq_length / self.dp_degree / self.num_cores_per_chip / self.num_mxu_cores_per_core / num_microbatch)
            inp_grad_dim_1['k'] = math.ceil(layer_obj.n / fused_dim[1])
            inp_grad_dim_1['n'] = math.ceil(layer_obj.k / fused_dim[0])

            inp_grad_dim_2 = None

            wgt_grad_dim['m'] = math.ceil(layer_obj.k / fused_dim[0] / self.num_cores_per_chip / self.num_mxu_cores_per_core)
            wgt_grad_dim['k'] = math.ceil(layer_obj.m * layer_obj.seq_length / self.dp_degree / wgt_grad_num_microbatch)
            wgt_grad_dim['n'] = math.ceil(layer_obj.n / fused_dim[1])

        else:
            # GEN-Q/K/V
            fwd_pass_dim['m'] = math.ceil(layer_obj.m * layer_obj.seq_length / self.dp_degree / self.num_cores_per_chip / self.num_mxu_cores_per_core / num_microbatch)
            fwd_pass_dim['k'] = math.ceil(layer_obj.k / fused_dim[0])
            fwd_pass_dim['n'] = math.ceil(layer_obj.n / 1)

            inp_grad_dim_1['m'] = math.ceil(layer_obj.m * layer_obj.seq_length / self.dp_degree / self.num_cores_per_chip / self.num_mxu_cores_per_core / num_microbatch)
            inp_grad_dim_1['k'] = math.ceil(layer_obj.n / 1)
            inp_grad_dim_1['n'] = math.ceil(layer_obj.k / fused_dim[0])

            inp_grad_dim_2 = None

            wgt_grad_dim['m'] = math.ceil(layer_obj.k / fused_dim[0] / self.num_cores_per_chip / self.num_mxu_cores_per_core)
            wgt_grad_dim['k'] = math.ceil(layer_obj.m * layer_obj.seq_length / self.dp_degree / wgt_grad_num_microbatch)
            wgt_grad_dim['n'] = math.ceil(layer_obj.n / 1)            

        return fwd_pass_dim, inp_grad_dim_1, inp_grad_dim_2, wgt_grad_dim

    def get_layer_topology_for_chip_fusion_approach_mlp(self, fwd_pass_dim, inp_grad_dim, wgt_grad_dim, layer_obj):
        """
        This function produces the layer topology item for scalesim call for MLP layers
        """             
        fwd_pass_item = TopologyItem(layer_obj.name, fwd_pass_dim['m'], fwd_pass_dim['k'], 1, fwd_pass_dim['k'], 1, fwd_pass_dim['n'], 1)       
        inp_grad_item = TopologyItem(layer_obj.name, inp_grad_dim['m'], inp_grad_dim['k'], 1, inp_grad_dim['k'], 1, inp_grad_dim['n'], 1) 
        weight_grad_item = TopologyItem(layer_obj.name, wgt_grad_dim['m'], wgt_grad_dim['k'], 1, wgt_grad_dim['k'], 1, wgt_grad_dim['n'], 1) 

        return fwd_pass_item, inp_grad_item, weight_grad_item

    def get_layer_topology_for_chip_fusion_approach_attn(self, fwd_pass_dim, inp_grad_dim_1, inp_grad_dim_2, wgt_grad_dim, layer_obj):
        """
        This function produces the layer topology item for scalesim call for ATTN layers
        """                     
        fwd_pass_item = TopologyItem(layer_obj.name, fwd_pass_dim['m'], fwd_pass_dim['k'], 1, fwd_pass_dim['k'], 1, fwd_pass_dim['n'], 1)       
        inp_grad_item_1 = TopologyItem(layer_obj.name, inp_grad_dim_1['m'], inp_grad_dim_1['k'], 1, inp_grad_dim_1['k'], 1, inp_grad_dim_1['n'], 1) 
        if inp_grad_dim_2 == None:
            inp_grad_item_2 = None
        else:
            inp_grad_item_2 = TopologyItem(layer_obj.name + "_2", inp_grad_dim_2['m'], inp_grad_dim_2['k'], 1, inp_grad_dim_2['k'], 1, inp_grad_dim_2['n'], 1)
        if wgt_grad_dim == None:
            weight_grad_item = None
        else:
            weight_grad_item = TopologyItem(layer_obj.name, wgt_grad_dim['m'], wgt_grad_dim['k'], 1, wgt_grad_dim['k'], 1, wgt_grad_dim['n'], 1) 

        return fwd_pass_item, inp_grad_item_1, inp_grad_item_2, weight_grad_item

    def get_communication_size_for_chip_fusion_per_microbatch(self, comm_size_for_optimal_fusion_config, num_microbatch):
        """
        This function scales the comm size calculated for optimal fusion configuration based on microbatching
        """
        for k,v in comm_size_for_optimal_fusion_config.items():
            if "scatter" in k or "gather" in k:
                # We do not apply microbatching to attention layers that require scatter/gather
                comm_size_for_optimal_fusion_config[k] = v
            else:
                comm_size_for_optimal_fusion_config[k] = math.ceil((v / num_microbatch) / 4) * 4
        return comm_size_for_optimal_fusion_config

    def get_total_transformer_layer_toplogies_fusion_requirements(self, attn_layers, mlp_layers):
        """
        Based on the layers dimensions and fusion dimensions, this function produces the layer dimensions for scalesim call
        and the fusion confuigrations and requirements (compute tiles, communication size, etc.)
        """
        fwd_pass_layers = []
        inp_grad_layers = []
        weight_grad_layers = []   

        fusion_config_dict = {} 

        layers_num_microbatch_dict = {}
        wgt_grad_layers_num_microbatch_dict = {}

        fwd_layers_array_util_dict = {}
        inp_grad_layers_array_util_dict = {}
        wgt_grad_layers_array_util_dict = {}

        attn_layers_communication_size_per_microbatch_dict_fwd_inp_grad = {}
        attn_layers_communication_size_per_microbatch_dict_wgt_grad = {}

        mlp_layers_communication_size_per_microbatch_dict_fwd_inp_grad = {}
        mlp_layers_communication_size_per_microbatch_dict_wgt_grad = {}

        fusion_configs = self.get_possible_fusion_configs()  

        # ATTN Layers
        attn_comm_size = self.get_attn_comm_size(attn_layers)
        attn_optimal_fusion_dim, attn_comm_size_for_optimal_fusion_config, attn_comm_size_for_optimal_fusion_config_wgt_grad = self.get_attn_optimal_fusion_dim(fusion_configs, attn_comm_size)    
        attn_optimal_fusion_configs = self.get_attn_fusion_configs(attn_optimal_fusion_dim, attn_layers)

        for idx, layer in enumerate(attn_layers):
            attn_layer_fusion_config = attn_optimal_fusion_configs[layer.name]
            # num microbatch for fwd/inp grad
            attn_layer_num_microbatch = self.get_layer_wise_num_microbatch_for_fusion(layer, attn_layer_fusion_config)
            # num microbatch for wgt grad
            attn_layer_wgt_grad_num_microbatch = self.get_layer_wise_num_microbatch_for_fusion_wgt_grad(layer, attn_layer_fusion_config)
            attn_layer_fwd_pass_dim, attn_layer_inp_grad_dim_1, attn_layer_inp_grad_dim_2, attn_layer_wgt_grad_dim = self.get_attn_layer_dim_per_core_per_fused_chip(attn_layer_fusion_config,
                                                                                            attn_layer_num_microbatch, attn_layer_wgt_grad_num_microbatch, layer)
            attn_layer_fwd_array_utilization, attn_layer_inp_grad_1_array_utilization, attn_layer_inp_grad_2_array_utilization, attn_layer_wgt_grad_array_utilization = \
                                                        self.calculate_array_utulization_with_fusion_attn(layer, attn_layer_fusion_config, attn_layer_num_microbatch, 
                                                                                                            attn_layer_wgt_grad_num_microbatch)

            attn_layer_fwd_pass_item, attn_layer_inp_grad_1_item, attn_layer_inp_grad_2_item, attn_layer_weight_grad_item = self.get_layer_topology_for_chip_fusion_approach_attn(attn_layer_fwd_pass_dim,
                                                        attn_layer_inp_grad_dim_1, attn_layer_inp_grad_dim_2, attn_layer_wgt_grad_dim, layer)

            fwd_pass_layers.append(attn_layer_fwd_pass_item)
            inp_grad_layers.append(attn_layer_inp_grad_1_item)
            if attn_layer_inp_grad_2_item is None:
                pass
            else:
                inp_grad_layers.append(attn_layer_inp_grad_2_item)
            if attn_layer_weight_grad_item is None:
                pass
            else:
                weight_grad_layers.append(attn_layer_weight_grad_item)

            fusion_config_dict[layer.name] = attn_layer_fusion_config

            layers_num_microbatch_dict[layer.name] = attn_layer_num_microbatch
            wgt_grad_layers_num_microbatch_dict[layer.name] = attn_layer_wgt_grad_num_microbatch

            fwd_layers_array_util_dict[layer.name] = attn_layer_fwd_array_utilization
            inp_grad_layers_array_util_dict[layer.name] = attn_layer_inp_grad_1_array_utilization
            if attn_layer_inp_grad_2_array_utilization is None:
                pass
            else:
                inp_grad_layers_array_util_dict[layer.name] = attn_layer_inp_grad_2_array_utilization            

            wgt_grad_layers_array_util_dict[layer.name] = attn_layer_wgt_grad_array_utilization
            

            # getting the comm size per microbatch for attn layers, fwd/inp grad
            attn_layer_comm_size_per_microbatch = self.get_communication_size_for_chip_fusion_per_microbatch(attn_comm_size_for_optimal_fusion_config[idx], attn_layer_num_microbatch)
            attn_layers_communication_size_per_microbatch_dict_fwd_inp_grad[layer.name] = attn_layer_comm_size_per_microbatch

            if layer.name in attn_comm_size_for_optimal_fusion_config_wgt_grad.keys():
                # getting the comm size per microbatch for attn layers, wgt grad
                attn_layer_comm_size_per_microbatch_wgt_grad = self.get_communication_size_for_chip_fusion_per_microbatch(attn_comm_size_for_optimal_fusion_config_wgt_grad[layer.name], 
                                                                                                                attn_layer_wgt_grad_num_microbatch)
                attn_layers_communication_size_per_microbatch_dict_wgt_grad[layer.name] = attn_layer_comm_size_per_microbatch_wgt_grad
            else:
                pass
        
        # MLP Layers
        mlp_comm_size = self.get_mlp_comm_size(mlp_layers)
        mlp_optimal_fusion_dim, mlp_comm_size_for_optimal_fusion_config, mlp_comm_size_for_optimal_fusion_config_wgt_grad = self.get_mlp_optimal_fusion_dim(fusion_configs, mlp_comm_size)
        mlp_optimal_fusion_configs = self.get_mlp_fusion_configs(mlp_optimal_fusion_dim, mlp_layers)

        for idx, layer in enumerate(mlp_layers):
            mlp_layer_fusion_config = mlp_optimal_fusion_configs[layer.name]
            # num microbatch for fwd/inp grad
            mlp_layer_num_microbatch = self.get_layer_wise_num_microbatch_for_fusion(layer, mlp_layer_fusion_config)
            # num microbatch for wgt grad
            mlp_layer_wgt_grad_num_microbatch = self.get_layer_wise_num_microbatch_for_fusion_wgt_grad(layer, mlp_layer_fusion_config)

            mlp_layer_fwd_pass_dim, mlp_layer_inp_grad_dim, mlp_layer_wgt_grad_dim = self.get_mlp_layer_dim_per_core_per_fused_chip(mlp_layer_fusion_config,
                                                                                            mlp_layer_num_microbatch, mlp_layer_wgt_grad_num_microbatch, layer)
            mlp_layer_fwd_array_utilization, mlp_layer_inp_grad_array_utilization, mlp_layer_wgt_grad_array_utilization = self.calculate_array_utulization_with_fusion_mlp(
                                                                                                                        layer, mlp_layer_fusion_config, mlp_layer_num_microbatch, 
                                                                                                                        mlp_layer_wgt_grad_num_microbatch)
            mlp_layer_fwd_pass_item, mlp_layer_inp_grad_item, mlp_layer_weight_grad_item = self.get_layer_topology_for_chip_fusion_approach_mlp(mlp_layer_fwd_pass_dim,
                                                                                                mlp_layer_inp_grad_dim, mlp_layer_wgt_grad_dim, layer)
            
            fwd_pass_layers.append(mlp_layer_fwd_pass_item)
            inp_grad_layers.append(mlp_layer_inp_grad_item)
            weight_grad_layers.append(mlp_layer_weight_grad_item)

            fusion_config_dict[layer.name] = mlp_layer_fusion_config

            layers_num_microbatch_dict[layer.name] = mlp_layer_num_microbatch
            wgt_grad_layers_num_microbatch_dict[layer.name] = mlp_layer_wgt_grad_num_microbatch

            fwd_layers_array_util_dict[layer.name] = mlp_layer_fwd_array_utilization
            inp_grad_layers_array_util_dict[layer.name] = mlp_layer_inp_grad_array_utilization
            wgt_grad_layers_array_util_dict[layer.name] = mlp_layer_wgt_grad_array_utilization

            # getting the comm size per microbatch for mlp layers, assumption: the number of mcirobatches is the same for all mlp layers due to a global fusion config
            # this communication is for fwd/inp grad
            mlp_layer_comm_size_per_microbatch = self.get_communication_size_for_chip_fusion_per_microbatch(mlp_comm_size_for_optimal_fusion_config[idx], mlp_layer_num_microbatch)

            # getting the comm size per microbatch for mlp layers, assumption: the number of mcirobatches is the same for all mlp layers due to a global fusion config
            # this communication is for wgt grad
            mlp_layer_comm_size_per_microbatch_wgt_grad = self.get_communication_size_for_chip_fusion_per_microbatch(mlp_comm_size_for_optimal_fusion_config_wgt_grad[idx], 
                                                                                                            mlp_layer_wgt_grad_num_microbatch)

            mlp_layers_communication_size_per_microbatch_dict_fwd_inp_grad[layer.name] = mlp_layer_comm_size_per_microbatch
            mlp_layers_communication_size_per_microbatch_dict_wgt_grad[layer.name] = mlp_layer_comm_size_per_microbatch_wgt_grad


        
        final_out = {}
        final_out['fwd_pass_layers'] = fwd_pass_layers
        final_out['inp_grad_layers'] = inp_grad_layers
        final_out['weight_grad_layers'] = weight_grad_layers
        final_out['fusion_config_dict'] = fusion_config_dict
        final_out['layers_num_microbatch_dict'] = layers_num_microbatch_dict
        final_out['layers_num_microbatch_wgt_grad_dict'] = wgt_grad_layers_num_microbatch_dict
        final_out['fwd_layers_array_util_dict'] = fwd_layers_array_util_dict
        final_out['inp_grad_layers_array_util_dict'] = inp_grad_layers_array_util_dict
        final_out['wgt_grad_layers_array_util_dict'] = wgt_grad_layers_array_util_dict
        final_out['attn_layers_communication_size_per_microbatch_dict_fwd_inp_grad'] = attn_layers_communication_size_per_microbatch_dict_fwd_inp_grad
        final_out['attn_layers_communication_size_per_microbatch_dict_wgt_grad'] = attn_layers_communication_size_per_microbatch_dict_wgt_grad
        final_out['mlp_layers_communication_size_per_microbatch_dict_fwd_inp_grad'] = mlp_layers_communication_size_per_microbatch_dict_fwd_inp_grad
        final_out['mlp_layers_communication_size_per_microbatch_dict_wgt_grad'] = mlp_layers_communication_size_per_microbatch_dict_wgt_grad

        return final_out

    def get_fusion_comp_overlap_and_final_layer_time(self, layers, fwd_cycles_w_fusion, inp_grad_cycles_w_fusion, wgt_grad_cycles_w_fusion,
                                    fwd_layers_communication_time_dict, inp_grad_layers_communication_time_dict,
                                    wgt_grad_layers_communication_time_dict, layers_num_microbatch_dict, 
                                    layers_num_micobatch_wgt_grad_dict, fusion_config_dict):
        """
        This function calculates the overlap opportunity of computation of a microbatch and its fusion communication.
        Note: QK and QKV attention layers include scatter/gather communications that are blocking and cant be overlapped with computation
        """
        fwd_fusion_comp_overlap = {}
        inp_grad_fusion_comp_overlap = {}
        wgt_grad_fusion_comp_overlap = {}

        fwd_scatter_gather_time_overhead = {}
        inp_grad_scatter_gather_time_overhead = {}

        fwd_final_layer_time = {}
        inp_grad_final_layer_time = {}
        wgt_grad_final_layer_time = {}

        # FWD
        for i, layer in enumerate(layers):
            if layer.name == "GEMM_QK" or layer.name == "GEMM_QKV":
                # these layers require scatter operations
                layer_fusion_scatter_time = fwd_layers_communication_time_dict[layer.name]['fwd_scatter']
                layer_fusion_comm_list = list(fwd_layers_communication_time_dict[layer.name].values())
                # remove scatter time
                layer_fusion_comm_list.remove(layer_fusion_scatter_time)
                fwd_fusion_comm_time = max(layer_fusion_comm_list)
                fwd_cycles_w_fusion_layer = fwd_cycles_w_fusion[layer.name]

                _fwd_fusion_comp_overlap = fwd_fusion_comm_time / (fwd_cycles_w_fusion_layer / layers_num_microbatch_dict[layer.name])
                fwd_fusion_comp_overlap[layer.name] = _fwd_fusion_comp_overlap
                fwd_unit_time = max(fwd_fusion_comm_time, (fwd_cycles_w_fusion_layer / layers_num_microbatch_dict[layer.name]))
                fwd_fusion_pipeline_overhead = (sum(fusion_config_dict[layer.name]) - 2) * 2 * fwd_unit_time
                # scatter time is blocking
                fwd_final_time = (fwd_unit_time * layers_num_microbatch_dict[layer.name]) + fwd_fusion_pipeline_overhead + layer_fusion_scatter_time
                fwd_scatter_gather_time_overhead[layer.name] = layer_fusion_scatter_time
                fwd_final_layer_time[layer.name] = fwd_final_time
            else:
                fwd_fusion_comm_time = max(list(fwd_layers_communication_time_dict[layer.name].values()))
                fwd_cycles_w_fusion_layer = fwd_cycles_w_fusion[layer.name]
                _fwd_fusion_comp_overlap = fwd_fusion_comm_time / (fwd_cycles_w_fusion_layer / layers_num_microbatch_dict[layer.name])
                fwd_fusion_comp_overlap[layer.name] = _fwd_fusion_comp_overlap
                fwd_unit_time = max(fwd_fusion_comm_time, (fwd_cycles_w_fusion_layer / layers_num_microbatch_dict[layer.name]))
                fwd_fusion_pipeline_overhead = (sum(fusion_config_dict[layer.name]) - 2) * 2 * fwd_unit_time
                fwd_final_time = (fwd_unit_time * layers_num_microbatch_dict[layer.name]) + fwd_fusion_pipeline_overhead
                fwd_scatter_gather_time_overhead[layer.name] = 0
                fwd_final_layer_time[layer.name] = fwd_final_time
        # Inp Grad
        for i, layer in enumerate(layers):
            if layer.name == "GEMM_QK" or layer.name == "GEMM_QKV":
                # this layer requires scatter operation and 2 inp grad computations
                if layer.name == "GEMM_QK":
                    layer_fusion_scatter_gather_time = inp_grad_layers_communication_time_dict[layer.name]['inp_grad_scatter']
                else:
                    layer_fusion_scatter_gather_time = inp_grad_layers_communication_time_dict[layer.name]['inp_grad_gather']
                                    
                layer_fusion_comm_list = list(inp_grad_layers_communication_time_dict[layer.name].values())
                # remove scatter/gather time
                layer_fusion_comm_list.remove(layer_fusion_scatter_gather_time)

                inp_grad_1_fusion_comm_time = max(inp_grad_layers_communication_time_dict[layer.name]['inp_grad_input_1'], inp_grad_layers_communication_time_dict[layer.name]['inp_grad_psum_1'])
                inp_grad_1_cycles_w_fusion_layer = inp_grad_cycles_w_fusion[layer.name]
                inp_grad_2_fusion_comm_time = max(inp_grad_layers_communication_time_dict[layer.name]['inp_grad_input_2'], inp_grad_layers_communication_time_dict[layer.name]['inp_grad_psum_2'])
                inp_grad_2_cycles_w_fusion_layer = inp_grad_cycles_w_fusion[layer.name + "_2"]


                _inp_grad_1_fusion_comp_overlap = inp_grad_1_fusion_comm_time / (inp_grad_1_cycles_w_fusion_layer / layers_num_microbatch_dict[layer.name])
                _inp_grad_2_fusion_comp_overlap = inp_grad_2_fusion_comm_time / (inp_grad_2_cycles_w_fusion_layer / layers_num_microbatch_dict[layer.name])
                inp_grad_fusion_comp_overlap[layer.name] = max(_inp_grad_1_fusion_comp_overlap, _inp_grad_2_fusion_comp_overlap)

                inp_grad_1_unit_time = max(inp_grad_1_fusion_comm_time, (inp_grad_1_cycles_w_fusion_layer / layers_num_microbatch_dict[layer.name]))
                inp_grad_1_fusion_pipeline_overhead = (sum(fusion_config_dict[layer.name]) - 2) * 2 * inp_grad_1_unit_time
                inp_grad_2_unit_time = max(inp_grad_2_fusion_comm_time, (inp_grad_2_cycles_w_fusion_layer / layers_num_microbatch_dict[layer.name]))
                inp_grad_2_fusion_pipeline_overhead = (sum(fusion_config_dict[layer.name]) - 2) * 2 * inp_grad_2_unit_time
                # scatter/gather time is blocking
                inp_grad_1_final_time = (inp_grad_1_unit_time * layers_num_microbatch_dict[layer.name]) + inp_grad_1_fusion_pipeline_overhead 
                inp_grad_2_final_time = (inp_grad_2_unit_time * layers_num_microbatch_dict[layer.name]) + inp_grad_2_fusion_pipeline_overhead 
                inp_grad_final_time = inp_grad_1_final_time + inp_grad_2_final_time + layer_fusion_scatter_gather_time
                inp_grad_scatter_gather_time_overhead[layer.name] = layer_fusion_scatter_gather_time
                inp_grad_final_layer_time[layer.name] = inp_grad_final_time
            else:
                # this layer has only one inp_grad wo scatter/gather op
                inp_grad_fusion_comm_time = max(list(inp_grad_layers_communication_time_dict[layer.name].values()))
                inp_grad_cycles_w_fusion_layer = inp_grad_cycles_w_fusion[layer.name]
                _inp_grad_fusion_comp_overlap = inp_grad_fusion_comm_time / (inp_grad_cycles_w_fusion_layer / layers_num_microbatch_dict[layer.name])
                inp_grad_fusion_comp_overlap[layer.name] = _inp_grad_fusion_comp_overlap
                inp_grad_unit_time = max(inp_grad_fusion_comm_time, (inp_grad_cycles_w_fusion_layer / layers_num_microbatch_dict[layer.name]))
                inp_grad_fusion_pipeline_overhead = (sum(fusion_config_dict[layer.name]) - 2) * 2 * inp_grad_unit_time
                inp_grad_final_time = (inp_grad_unit_time * layers_num_microbatch_dict[layer.name]) + inp_grad_fusion_pipeline_overhead
                inp_grad_scatter_gather_time_overhead[layer.name] = 0
                inp_grad_final_layer_time[layer.name] = inp_grad_final_time
        # Wgt Grad
        for i, layer in enumerate(layers):
            # see if the layer has wgt grad
            if layer.name in wgt_grad_layers_communication_time_dict.keys():
                wgt_grad_fusion_comm_time = max(list(wgt_grad_layers_communication_time_dict[layer.name].values()))
                wgt_grad_cycles_w_fusion_layer = wgt_grad_cycles_w_fusion[layer.name]
                _wgt_grad_fusion_comp_overlap = wgt_grad_fusion_comm_time / (wgt_grad_cycles_w_fusion_layer / layers_num_micobatch_wgt_grad_dict[layer.name])
                wgt_grad_fusion_comp_overlap[layer.name] = _wgt_grad_fusion_comp_overlap
                wgt_grad_unit_time = max(wgt_grad_fusion_comm_time, (wgt_grad_cycles_w_fusion_layer / layers_num_micobatch_wgt_grad_dict[layer.name]))
                wgt_grad_fusion_pipeline_overhead = (sum(fusion_config_dict[layer.name]) - 2) * 1 * wgt_grad_unit_time
                wgt_grad_final_time = (wgt_grad_unit_time * layers_num_micobatch_wgt_grad_dict[layer.name]) + wgt_grad_fusion_pipeline_overhead
                wgt_grad_final_layer_time[layer.name] = wgt_grad_final_time                
            else:
                wgt_grad_fusion_comp_overlap[layer.name] = None
                wgt_grad_final_layer_time[layer.name] = 0

        return fwd_fusion_comp_overlap, inp_grad_fusion_comp_overlap, wgt_grad_fusion_comp_overlap, fwd_scatter_gather_time_overhead, inp_grad_scatter_gather_time_overhead, \
                                         fwd_final_layer_time, inp_grad_final_layer_time, wgt_grad_final_layer_time
    

class FusionCommMeasurement(object):
    def __init__(self, run_name, attn_layers, mlp_layers, attn_layers_communication_size_per_microbatch_dict_fwd_inp_grad, attn_layers_communication_size_per_microbatch_dict_wgt_grad,
                                        mlp_layers_communication_size_per_microbatch_dict_fwd_inp_grad, mlp_layers_communication_size_per_microbatch_dict_wgt_grad,
                                        nw_bw, nw_latency, num_nw_links, path_to_3d_torus_npu_system_config, fusion_config_dict):
        self.run_name = run_name 
        self.attn_layers = attn_layers
        self.mlp_layers = mlp_layers
        self.attn_layers_communication_size_per_microbatch_dict_fwd_inp_grad = attn_layers_communication_size_per_microbatch_dict_fwd_inp_grad
        self.attn_layers_communication_size_per_microbatch_dict_wgt_grad = attn_layers_communication_size_per_microbatch_dict_wgt_grad
        self.mlp_layers_communication_size_per_microbatch_dict_fwd_inp_grad = mlp_layers_communication_size_per_microbatch_dict_fwd_inp_grad
        self.mlp_layers_communication_size_per_microbatch_dict_wgt_grad = mlp_layers_communication_size_per_microbatch_dict_wgt_grad
        self.nw_bw = nw_bw
        self.nw_latency = nw_latency
        self.num_nw_links = num_nw_links
        self.path_to_3d_torus_npu_system_config = path_to_3d_torus_npu_system_config
        self.num_scatter_gather_chips = fusion_config_dict['GEMM_QK'][0] * fusion_config_dict['GEMM_QK'][1]
    
    def generate_workload_for_fusion_comm_measurement(self):
        """
        This function generates a dummy workload that models the chip-to-chip fusion communication (also scatter/gather) as a single allgather operation
        and generates the input workload for ASTRA-SIM
        """
        # create a directory for fusion_comm micro workloads
        micro_wl_dir = "../../inputs/workload/" + self.run_name + "_fusion_comm"
        if os.path.exists(micro_wl_dir):
            shutil.rmtree(micro_wl_dir, ignore_errors=True)
            os.makedirs(micro_wl_dir)
        else:
            os.makedirs(micro_wl_dir)

        for i, layer in enumerate(self.attn_layers):
            # fwd/ inp grad comm
            for k,v in self.attn_layers_communication_size_per_microbatch_dict_fwd_inp_grad[layer.name].items():
                wl_line = [layer.name + k, "-1", 5, "NONE", 0, 5, "NONE", 0, 5, "ALLGATHER", int(v), 5]
                wl_line_file_name = layer.name + k + ".txt"
                path_to_wl_line_file = micro_wl_dir + "/" + wl_line_file_name
                self.generate_micro_workload(path_to_wl_line_file, wl_line)
            # wgt grad comm
            if layer.name in self.attn_layers_communication_size_per_microbatch_dict_wgt_grad.keys():
                for k,v in self.attn_layers_communication_size_per_microbatch_dict_wgt_grad[layer.name].items():
                    wl_line = [layer.name + k, "-1", 5, "NONE", 0, 5, "NONE", 0, 5, "ALLGATHER", int(v), 5]
                    wl_line_file_name = layer.name + k + ".txt"
                    path_to_wl_line_file = micro_wl_dir + "/" + wl_line_file_name
                    self.generate_micro_workload(path_to_wl_line_file, wl_line)
            else:
                pass

        for i, layer in enumerate(self.mlp_layers):
            # fwd/ inp grad comm
            for k,v in self.mlp_layers_communication_size_per_microbatch_dict_fwd_inp_grad[layer.name].items():
                wl_line = [layer.name + k, "-1", 5, "NONE", 0, 5, "NONE", 0, 5, "ALLGATHER", int(v), 5]
                wl_line_file_name = layer.name + k + ".txt"
                path_to_wl_line_file = micro_wl_dir + "/" + wl_line_file_name
                self.generate_micro_workload(path_to_wl_line_file, wl_line)
            # wgt grad comm
            for k,v in self.mlp_layers_communication_size_per_microbatch_dict_wgt_grad[layer.name].items():
                wl_line = [layer.name + k, "-1", 5, "NONE", 0, 5, "NONE", 0, 5, "ALLGATHER", int(v), 5]
                wl_line_file_name = layer.name + k + ".txt"
                path_to_wl_line_file = micro_wl_dir + "/" + wl_line_file_name
                self.generate_micro_workload(path_to_wl_line_file, wl_line)


    def set_npu_system_params(self, fusion_flag):
        """
        This function sets the NPU system parameters such as BW, latency, Number of Links, and dimensions of the NPU sustem for the input network file for ASTRA-SIM
        """
        npu_system_jsonFile = open(self.path_to_3d_torus_npu_system_config, "r") # Open the JSON file for reading
        data = json.load(npu_system_jsonFile) # Read the JSON into the buffer
        npu_system_jsonFile.close() # Close the JSON file

        ## Working with buffered content
        data["links-count"] = [self.num_nw_links, self.num_nw_links, self.num_nw_links]
        data["link-latency"] = [self.nw_latency, self.nw_latency, self.nw_latency]
        data["link-bandwidth"] = [self.nw_bw, self.nw_bw, self.nw_bw]

        # set the dimensions of NPUs
        if fusion_flag:
            # chip fusion comm
            data["units-count"] = [1, 1, 2]
        else:
            # scatter/gather comm
            data["units-count"] = [1, 1, self.num_scatter_gather_chips]
        # HBM params for now are set to a number to be ignored. 

        ## Save our changes to JSON file
        npu_system_jsonFile_w = open(self.path_to_3d_torus_npu_system_config, "w+")
        npu_system_jsonFile_w.write(json.dumps(data,  indent = 6))
        npu_system_jsonFile_w.close()

    def generate_micro_workload(self, path_to_output_file, line_list):
        output = []
        line_list = map(lambda x: str(x), line_list)
        line_string = "\t".join(line_list)
        print(line_string)
        output.append(line_string)

        file_handle = open(path_to_output_file, "w")
        file_handle.write("MICRO" + "\n")
        file_handle.write(str(len(output)))
        for line in output:
            file_handle.write("\n")
            file_handle.write(line)
        file_handle.close()

    def run_astrasim_fusion_measurement(self):
        path_to_run_astrasim_fusion_comm = "../../examples/run_fusion_comm.sh"
        micro_wl_dir = "../../inputs/workload/" + self.run_name + "_fusion_comm"
        micro_wl_list = os.listdir(micro_wl_dir)

        for wl in micro_wl_list:
            ### modify shell file based on the workload
            # get the line location of modifables
            workload_line_loc = get_line_number("WORKLOAD=", path_to_run_astrasim_fusion_comm)
            stats_line_loc = get_line_number("STATS=", path_to_run_astrasim_fusion_comm)
            # update the lines
            with open(path_to_run_astrasim_fusion_comm, 'r') as file:
                data = file.readlines()

            workload_dir_name = self.run_name + '_fusion_comm/'
            data[workload_line_loc-1] = 'WORKLOAD="${SCRIPT_DIR:?}"/../inputs/workload/' + workload_dir_name + wl + '\n'
            data[stats_line_loc-1] = 'STATS="${SCRIPT_DIR:?}"/results/' + self.run_name + '_fusion_comm/' + wl.replace(".txt", "") + '\n'
            with open(path_to_run_astrasim_fusion_comm, 'w') as file:
                file.writelines(data)
            
            # create a directory for stats
            if not os.path.exists("../../examples/results/" + self.run_name + '_fusion_comm'):
                os.makedirs("../../examples/results/" + self.run_name + '_fusion_comm')
            
            ### run astrasim
            # run astrasim for scatter/gather communications
            if "scatter" in wl or "gather" in wl:
                # set npu system based on the number of fused chips
                self.set_npu_system_params(fusion_flag=False)
                command = "{} -n analytical".format(os.path.join(path_to_run_astrasim_fusion_comm))
                process = subprocess.Popen(command, shell=True)
                process.wait()

            else:
                # run astrasim for fusion communications
                self.set_npu_system_params(fusion_flag=True)
                command = "{} -n analytical".format(os.path.join(path_to_run_astrasim_fusion_comm))
                process = subprocess.Popen(command, shell=True)
                process.wait()
            print("*"*50)
            print(wl + " :Communication Measurement DONE!")
            print("*"*50)

    def get_fusion_comm_from_astrasim(self):
        """
        This function extracts the communication time for fusion from the ASTRA-SIM reports. 
        """
        self.generate_workload_for_fusion_comm_measurement()
        self.run_astrasim_fusion_measurement()

        path_to_reports_dir = "../../examples/results/" + self.run_name + "_fusion_comm/"

        fwd_layers_communication_time_dict = {}
        inp_grad_layers_communication_time_dict = {}
        wgt_grad_layers_communication_time_dict = {}


        for i, layer in enumerate(self.attn_layers):
            # fwd/ inp grad comm
            fwd_comm_time_dict = {}
            inp_grad_comm_time_dict = {}
            wgt_grad_comm_time_dict = {}
            for k,v in self.attn_layers_communication_size_per_microbatch_dict_fwd_inp_grad[layer.name].items():
                if "fwd" in k:
                    path_to_fwd_csv = path_to_reports_dir + layer.name + k + "/EndToEnd.csv"
                    fwd_comm_time_col = pd.read_csv(path_to_fwd_csv)['total exposed comm'].tolist()
                    fwd_comm_time_in_cylces = float(fwd_comm_time_col[0]) * 1000
                    fwd_comm_time_dict[k] = fwd_comm_time_in_cylces                    
                else:
                    # inp_grad
                    path_to_inp_grad_csv = path_to_reports_dir + layer.name + k + "/EndToEnd.csv"
                    inp_grad_comm_time_col = pd.read_csv(path_to_inp_grad_csv)['total exposed comm'].tolist()
                    inp_grad_comm_time_in_cylces = float(inp_grad_comm_time_col[0]) * 1000
                    inp_grad_comm_time_dict[k] = inp_grad_comm_time_in_cylces      
            # wgt grad comm
            if layer.name in self.attn_layers_communication_size_per_microbatch_dict_wgt_grad.keys():
                for k,v in self.attn_layers_communication_size_per_microbatch_dict_wgt_grad[layer.name].items():
                    path_to_wgt_grad_csv = path_to_reports_dir + layer.name + k + "/EndToEnd.csv"
                    wgt_grad_comm_time_col = pd.read_csv(path_to_wgt_grad_csv)['total exposed comm'].tolist()
                    wgt_grad_comm_time_in_cylces = float(wgt_grad_comm_time_col[0]) * 1000
                    wgt_grad_comm_time_dict[k] = wgt_grad_comm_time_in_cylces  
                wgt_grad_layers_communication_time_dict[layer.name] =  wgt_grad_comm_time_dict      
            else:
                pass

            fwd_layers_communication_time_dict[layer.name] = fwd_comm_time_dict       
            inp_grad_layers_communication_time_dict[layer.name] = inp_grad_comm_time_dict   


        for i, layer in enumerate(self.mlp_layers):
            # fwd/ inp grad comm
            fwd_comm_time_dict = {}
            inp_grad_comm_time_dict = {}
            wgt_grad_comm_time_dict = {}
            for k,v in self.mlp_layers_communication_size_per_microbatch_dict_fwd_inp_grad[layer.name].items():
                if "fwd" in k:
                    path_to_fwd_csv = path_to_reports_dir + layer.name + k + "/EndToEnd.csv"
                    fwd_comm_time_col = pd.read_csv(path_to_fwd_csv)['total exposed comm'].tolist()
                    fwd_comm_time_in_cylces = float(fwd_comm_time_col[0]) * 1000
                    fwd_comm_time_dict[k] = fwd_comm_time_in_cylces                    
                else:
                    # inp_grad
                    path_to_inp_grad_csv = path_to_reports_dir + layer.name + k + "/EndToEnd.csv"
                    inp_grad_comm_time_col = pd.read_csv(path_to_inp_grad_csv)['total exposed comm'].tolist()
                    inp_grad_comm_time_in_cylces = float(inp_grad_comm_time_col[0]) * 1000
                    inp_grad_comm_time_dict[k] = inp_grad_comm_time_in_cylces      
            # wgt grad comm
            for k,v in self.mlp_layers_communication_size_per_microbatch_dict_wgt_grad[layer.name].items():
                path_to_wgt_grad_csv = path_to_reports_dir + layer.name + k + "/EndToEnd.csv"
                wgt_grad_comm_time_col = pd.read_csv(path_to_wgt_grad_csv)['total exposed comm'].tolist()
                wgt_grad_comm_time_in_cylces = float(wgt_grad_comm_time_col[0]) * 1000
                wgt_grad_comm_time_dict[k] = wgt_grad_comm_time_in_cylces   

            fwd_layers_communication_time_dict[layer.name] = fwd_comm_time_dict       
            inp_grad_layers_communication_time_dict[layer.name] = inp_grad_comm_time_dict   
            wgt_grad_layers_communication_time_dict[layer.name] =  wgt_grad_comm_time_dict     

        return fwd_layers_communication_time_dict, inp_grad_layers_communication_time_dict, wgt_grad_layers_communication_time_dict


def get_cycles_w_fusion(fwd_pass_layers, inp_grad_layers, weight_grad_layers, layers_num_compute_tiles_dict, 
                                                        layers_num_compute_tiles_wgt_grad_dict, simulation_mode,
                                                        path_to_npu_config_file, frequency, mem_bw, mem_latency, fusion_num_heads_per_npu):
    
    if simulation_mode == "scalesim":
        ### get the compute time from scalesim with fusion
        output_w_fusion = get_scalesim_output_internal(fwd_pass_layers, inp_grad_layers, weight_grad_layers, 
                                                                "Fusion", dnn_name, run_name)
    else:
        ### get the compute time from analytical model with fusion
        output_w_fusion = get_analytical_output_internal(fwd_pass_layers, inp_grad_layers, weight_grad_layers, 
                                                                "Fusion", dnn_name, run_name, path_to_npu_config_file,
                                                                frequency, mem_bw, mem_latency, "dict")     
                                                                   
    # update the cycles with compute tiles and number of attention heads
    fwd_cycles_w_fusion = {}
    inp_grad_cycles_w_fusion = {}
    wgt_grad_cycles_w_fusion = {}

    fwd_cycles = output_w_fusion["fwd_pass_cycles"]
    inp_grad_cycles = output_w_fusion["inp_grad_cycles"]
    wgt_grad_cycles = output_w_fusion["weight_grad_cycles"]
    layer_names =  list(fwd_cycles.keys())

    for k,v in fwd_cycles.items():
        if "MLP" in k or k == "GEMM_Linear":
            # no head parallelism
            fwd_cycles_w_fusion[k] = int(v) * layers_num_compute_tiles_dict[k]
        else:
            # head parallelism 
            fwd_cycles_w_fusion[k] = int(v) * layers_num_compute_tiles_dict[k] * fusion_num_heads_per_npu[k]
    for k, v in inp_grad_cycles.items():
        if "MLP" in k or k == "GEMM_Linear":
            # no head parallelism
            inp_grad_cycles_w_fusion[k] = int(v) * layers_num_compute_tiles_dict[k]
        else:
            # head parallelism
            if k in layer_names:
                inp_grad_cycles_w_fusion[k] = int(v) * layers_num_compute_tiles_dict[k] * fusion_num_heads_per_npu[k]
            else:
                # for second inp_grad for QK and QKV
                _layer_name = k.replace("_2", "")
                inp_grad_cycles_w_fusion[k] = int(v) * layers_num_compute_tiles_dict[_layer_name] * fusion_num_heads_per_npu[_layer_name]
    for k, v in wgt_grad_cycles.items():
        if "MLP" in k or k == "GEMM_Linear":
            # no head parallelism
            wgt_grad_cycles_w_fusion[k] = int(v) * layers_num_compute_tiles_wgt_grad_dict[k]    
        else:
            # head parallelism in GEN-Q/K/V
            wgt_grad_cycles_w_fusion[k] = int(v) * layers_num_compute_tiles_wgt_grad_dict[k] * fusion_num_heads_per_npu[_layer_name]   

    return fwd_cycles_w_fusion, inp_grad_cycles_w_fusion, wgt_grad_cycles_w_fusion

def get_fusion_num_head_per_npu(layers, fusion_config_dict, mp_degree):
    fusion_num_heads = {}
    for l in layers:
        if "MLP" in l.name or l.name == "GEMM_Linear":
            fusion_num_heads[l.name] = 1
        elif l.name == "GEMM_QK" or l.name == "GEMM_QKV":
            fusion_num_heads[l.name] = l.num_heads / (mp_degree / (fusion_config_dict[l.name][0] * fusion_config_dict[l.name][1]))
        else:
            fusion_num_heads[l.name] = l.num_heads / fusion_config_dict[l.name][1]
    return fusion_num_heads

def get_baseline_cycles_wo_fusion(path_to_dnn_fwd_cycles_csv, path_to_dnn_inp_grad_cycles_csv, path_to_dnn_weight_grad_cycles_csv):

    baseline_csv_layer_name_col_name =  pd.read_csv(path_to_dnn_fwd_cycles_csv).columns[0]
    cycles_col_name = pd.read_csv(path_to_dnn_fwd_cycles_csv).columns[1]
    baseline_csv_layer_names = pd.read_csv(path_to_dnn_fwd_cycles_csv)[baseline_csv_layer_name_col_name].tolist()

    fwd_cycles_baseline = pd.read_csv(path_to_dnn_fwd_cycles_csv)[cycles_col_name].tolist()
    inp_grad_cycles_baseline = pd.read_csv(path_to_dnn_inp_grad_cycles_csv)[cycles_col_name].tolist()
    wgt_grad_cycles_baseline = pd.read_csv(path_to_dnn_weight_grad_cycles_csv)[cycles_col_name].tolist()

    return baseline_csv_layer_names, fwd_cycles_baseline, inp_grad_cycles_baseline, wgt_grad_cycles_baseline


def get_attn_mlp_layers(layers):
    attn_layers = []
    mlp_layers = []
    for layer in layers:
        if 'MLP' in layer.name:
            mlp_layers.append(layer)
        else:
            attn_layers.append(layer)
    return attn_layers, mlp_layers



def get_dnn_layers_from_mnk_csv(path_to_dnn_csv_file):
    """
    This function (gotten from gen_astrasim) reads the DNN CSV file and generates a list of DNN layer objects
    """
    file_handle = open(path_to_dnn_csv_file, "r")
    lines = file_handle.readlines()
    file_handle.close()
    first = True
    layers = []

    for line in lines:
        if first:
            first = False
            continue
        line = line.strip('\n').strip(' ')
        cols = line.split(",")
        if dnn_type == "Transformer":
            assert len(cols) == 10, "There should be 10 columns in the mnk file"
            layers.append(TransformerLayer(cols))
        else:
            # dnn type is CNN/GEMM
            assert len(cols) == 6, "There should be 6 columns in the mnk file"
            layers.append(Layer(cols))
        print(cols)
    
    return layers



if __name__ == "__main__":

	main()
