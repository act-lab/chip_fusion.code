import math
import configparser


def divisorGenerator(n):
    large_divisors = []
    for i in range(1, int(math.sqrt(n) + 1)):
        if n % i == 0:
            yield i
            if i*i != n:
                large_divisors.append(n / i)
    for divisor in reversed(large_divisors):
        yield divisor

def get_the_minimum_greater_than_element_of_list(elem_list, number):
    val = max(elem_list)
    for elem in elem_list:
        if elem >= number:
            val = elem
            break
        else:
            pass
    return val


def get_line_number(phrase, file_name):
    with open(file_name) as f:
        for i, line in enumerate(f, 1):
            if phrase in line:
                return i