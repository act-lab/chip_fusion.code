from absl import app
from absl import flags
from fusion_scalesim import get_analytical_output_internal
import os
import subprocess
import configparser as cp
import math


# Common variables for all layers
DatatypeSize            = 2
NumberOfNPUs            = 8 # This represents the total number of NPUs in the whole system
NumberOfNPUCores        = 2
NumberofMXUCoresPerNPUCore  = 2 
NumberOfPackages        = 8
NumberOfNPUsPerPackage  = 1
Strides                 = 1
FilterHeight            = 1
NumberOfChannels        = 1
# End common variables for all layers

# Region for constants
SCALESIM_VER = 1
SCALESIM_PATH = r"../../extern/compute/SCALE-Sim-V1/SCALE-Sim"
SCALESIM_OUTPUT_PATH = SCALESIM_PATH + '/outputs'
SCALESIM_CONFIG = r"../../extern/compute/SCALE-Sim-V1/SCALE-Sim/configs/google_smaller.cfg"
OUTPUT_FILE_NAME = r"workload.txt"
# SCALE Sim installed flag
scale_sim_installed_flag = False

Hybrid                    = "HYBRID"
ForwardPassCycles         = "fwd_pass_cycles"
InputGradientCycles       = "inp_grad_cycles"
WeightGradientCycles      = "weight_grad_cycles"
DataParallel              = "DATA"
TransformerDataParallel   = "TRANSFORMER_DATA"
TransformerHybridDataModelParallel   = "TRANSFORMER_HYBRID_DATA_MODEL"
ModelParallel             = "MODEL"
HybridDataModelParallel   = "HYBRID_DATA_MODEL"
HybridModelDataParallel   = "HYBRID_MODEL_DATA"
MegatronMLPModelParallel  = "MEGATRON_MLP_MODEL"
MegatronATTNModelParallel = "MEGATRON_ATTN_MODEL"
MegatronMLPHybridDataModelParallel  = "MEGATRON_MLP_HYBRID_DATA_MODEL"
MegatronATTNHybridDataModelParallel = "MEGATRON_ATTN_HYBRID_DATA_MODEL"
CustomizedParallel        = "HYBRID_CUSTOMIZED"
AllToAll                  = "ALLTOALL"
AllReduce                 = "ALLREDUCE"
AllGather                 = "ALLGATHER"
ReduceScatter             = "REDUCESCATTER"
NoCommunication           = "NONE"
Outputs                   = "outputs"
ParallelizationStrategy = ModelParallel
# End region for constants

# Region for command line arguments

FLAGS = flags.FLAGS
#name of flag | default | explanation
flags.DEFINE_string("mnk", "mnk_inputs/test.csv", "Path of the file that has the m,n,k values")
flags.DEFINE_string("run_name", "test", "Name of the folder that will have the generated output")
flags.DEFINE_string("output_file", OUTPUT_FILE_NAME, "Name of the generated ASTRA-Sim input file")
flags.DEFINE_string("scalesim_path", SCALESIM_PATH, "Path to SCALE-Sim folder")
flags.DEFINE_string("scalesim_config", SCALESIM_CONFIG, "Path to SCALE-Sim config file")
flags.DEFINE_string("parallel", DataParallel, "Parallelization strategy: MODEL, DATA, HYBRID_DATA_MODEL, HYBRID_MODEL_DATA, HYBRID_CUSTOMIZED, MEGATRON_MODEL, MEGATRON_HYBRID_DATA_MODEL")
flags.DEFINE_string("datatype_size", str(DatatypeSize), "Size of the data type")
flags.DEFINE_string("num_npus", str(NumberOfNPUs), "Total number of NPUs")
flags.DEFINE_string("num_packages", str(NumberOfPackages), "Number of packages")
flags.DEFINE_string("num_cores", str(NumberOfNPUCores), "Number of Cores within one NPU")
flags.DEFINE_string("num_mxu_cores", str(NumberofMXUCoresPerNPUCore), "Number of MXU Cores within one NPU Core")


def parseCommandLineArguments():
    global ParallelizationStrategy, DatatypeSize, NumberOfNPUs, NumberOfPackages, NumberOfNPUsPerPackage, NumberOfNPUCores, NumberofMXUCoresPerNPUCore
    global SCALESIM_PATH, SCALESIM_CONFIG, OUTPUT_FILE_NAME
    SCALESIM_PATH = os.path.abspath(FLAGS.scalesim_path)
    SCALESIM_CONFIG = os.path.abspath(FLAGS.scalesim_config)
    if not os.path.exists(SCALESIM_PATH):
        print('ERROR: SCALE-Sim not exists in {}'.format(SCALESIM_PATH))
        print('  run "git clone https://github.com/ARM-software/SCALE-Sim.git" to clone SCALE-Sim to the proper place')
        print('  or run build.sh in astra-sim root directory and find SCALE-Sim under astra-sim/compute/')
        exit()
    if not os.path.exists(SCALESIM_CONFIG):
        print('ERROR: SCALE-Sim config "{}" not found'.format(SCALESIM_CONFIG))
        exit()
    OUTPUT_FILE_NAME = os.path.abspath(FLAGS.output_file)
    ParallelizationStrategy = FLAGS.parallel
    DatatypeSize = int(FLAGS.datatype_size)
    NumberOfNPUs = int(FLAGS.num_npus)
    NumberOfPackages = int(FLAGS.num_packages)
    NumberOfNPUsPerPackage = int(NumberOfNPUs / NumberOfPackages)
    assert NumberOfNPUsPerPackage * NumberOfPackages == NumberOfNPUs
    NumberOfNPUCores = int(FLAGS.num_cores)
    NumberofMXUCoresPerNPUCore = int(FLAGS.num_mxu_cores)


DEBUG = 1

def DPRINT(message):
    if DEBUG == 1:
        print (message)

class ModelParallelStrategy:

    def __init__(self):
        pass

    def getCommunicationTypeForFwdPass(self, i, layer):
        if layer.parallelism_dim == "n":
            return AllGather
        elif layer.parallelism_dim == "k":
            return ReduceScatter
        else:
            pass

    def getCommunicationTypeForInpGrad(self, i, layer):
        if layer.parallelism_dim == "n":
            return ReduceScatter
        elif layer.parallelism_dim == "k":
            return AllGather
        else:
            pass

    def getCommunicationTypeForWeightGrad(self, i, layer):
        return NoCommunication


    def getCommunicationSizeForFwdPass(self, i, layer):
        if layer.parallelism_dim == "n":        
            return int(layer.m * layer.n * DatatypeSize / NumberOfNPUs)
        elif layer.parallelism_dim == "k":
            return int(layer.m * layer.n * DatatypeSize)

    def getCommunicationSizeForInpGrad(self, i, layer):
        if layer.parallelism_dim == "n": 
            return int(layer.m * layer.k * DatatypeSize)
        elif layer.parallelism_dim == "k":
            return int(layer.m * layer.k * DatatypeSize / NumberOfNPUs)        

    def getCommunicationSizeForWeightGrad(self, i, layer):
        return 0


class MegatronMLPModelParallelStrategy:
    """
    This class models the Megatron-LM model parallelization strategy for MLP layers of Transformers
    """
    # SG: Added the reduction time (add) estimation

    def __init__(self):
        pass

    def getCommunicationTypeForFwdPass(self, i, layer):
        if layer.parallelism_dim == "n":
            return NoCommunication
        elif layer.parallelism_dim == "k":
            return AllReduce
        else:
            pass

    def getCommunicationTypeForInpGrad(self, i, layer):
        if layer.parallelism_dim == "n":
            return AllReduce
        elif layer.parallelism_dim == "k":
            return NoCommunication
        else:
            pass

    def getCommunicationTypeForWeightGrad(self, i, layer):
        return NoCommunication


    def getCommunicationSizeForFwdPass(self, i, layer):
        if layer.parallelism_dim == "n":        
            return 0
        elif layer.parallelism_dim == "k":
            return int(layer.m * layer.seq_length * layer.n * DatatypeSize)

    def getCommunicationSizeForInpGrad(self, i, layer):
        if layer.parallelism_dim == "n": 
            return int(layer.m * layer.seq_length * layer.k * DatatypeSize)
        elif layer.parallelism_dim == "k":
            return 0      

    def getCommunicationSizeForWeightGrad(self, i, layer):
        return 0

    def getReductionDelayEstimation(self, i, layer):
        # delay in cycles
        if layer.parallelism_dim == "n":
            num_lanes, num_simd_units = getNumSIMDLanes(SCALESIM_CONFIG, NumberOfNPUCores)
            reduction_cycles = int(layer.m * layer.seq_length * layer.k / num_lanes / num_simd_units)
        elif layer.parallelism_dim == "k":
            num_lanes, num_simd_units = getNumSIMDLanes(SCALESIM_CONFIG, NumberOfNPUCores)
            reduction_cycles = int(layer.m * layer.seq_length * layer.n / num_lanes / num_simd_units)
        return reduction_cycles



class MegatronMLPHybridDataModelParallelStrategy:
    """
    This class models the Megatron-LM Hybrid data-model parallelization strategy for MLP layers of Transformers
    """
    # SG: Added the reduction time (add) estimation

    def __init__(self):
        pass

    def getCommunicationTypeForFwdPass(self, i, layer):
        if layer.parallelism_dim == "n":
            return NoCommunication
        elif layer.parallelism_dim == "k":
            return AllReduce
        else:
            pass

    def getCommunicationTypeForInpGrad(self, i, layer):
        if layer.parallelism_dim == "n":
            return AllReduce
        elif layer.parallelism_dim == "k":
            return NoCommunication
        else:
            pass

    def getCommunicationTypeForWeightGrad(self, i, layer):
        return AllReduce


    def getCommunicationSizeForFwdPass(self, i, layer):
        if layer.parallelism_dim == "n":        
            return 0
        elif layer.parallelism_dim == "k":
            return int(layer.m * layer.seq_length * layer.n * DatatypeSize / NumberOfPackages)

    def getCommunicationSizeForInpGrad(self, i, layer):
        if layer.parallelism_dim == "n": 
            return int(layer.m * layer.seq_length * layer.k * DatatypeSize / NumberOfPackages)
        elif layer.parallelism_dim == "k":
            return 0      

    def getCommunicationSizeForWeightGrad(self, i, layer):
        return int(layer.n * layer.k * DatatypeSize / NumberOfNPUsPerPackage)

    def getReductionDelayEstimation(self, i, layer):
        # delay in cycles 
        if layer.parallelism_dim == "n":
            num_lanes, num_simd_units = getNumSIMDLanes(SCALESIM_CONFIG, NumberOfNPUCores)
            reduction_cycles = int(layer.m * layer.seq_length * layer.k / num_lanes / num_simd_units / NumberOfPackages)
        elif layer.parallelism_dim == "k":
            num_lanes, num_simd_units = getNumSIMDLanes(SCALESIM_CONFIG, NumberOfNPUCores)
            reduction_cycles = int(layer.m * layer.seq_length * layer.n / num_lanes / num_simd_units / NumberOfPackages)
        return reduction_cycles



class MegatronATTNModelParallelStrategy:
    """
    This class models the Megatron-LM model parallelization strategy for Attention (ATTN) layers of Transformers
    """

    def __init__(self):
        pass

    def getCommunicationTypeForFwdPass(self, i, layer):
        if layer.parallelism_dim == "h":
            return NoCommunication
        elif layer.parallelism_dim == "k":
            return AllReduce
        else:
            pass

    def getCommunicationTypeForInpGrad(self, i, layer):
        if layer.parallelism_dim == "h" and layer.first:
            # we only apply the AllReduce once for Q,K,V, since the partial results for those are accumulated locally in each device.
            # As such,for simpliciy we only consider the first layer of the attention (Q_GEN)
            return AllReduce
        else:
            NoCommunication

    def getCommunicationTypeForWeightGrad(self, i, layer):
        return NoCommunication


    def getCommunicationSizeForFwdPass(self, i, layer):
        if layer.parallelism_dim == "h":        
            return 0
        elif layer.parallelism_dim == "k":
            return int(layer.m * layer.seq_length * layer.n * DatatypeSize)

    def getCommunicationSizeForInpGrad(self, i, layer):
        if layer.parallelism_dim == "h" and layer.first: 
            return int(layer.m * layer.seq_length * layer.k * DatatypeSize)
        else:
            return 0      

    def getCommunicationSizeForWeightGrad(self, i, layer):
        return 0

    def getReductionDelayEstimation(self, i, layer):
        # delay in cycles
        if layer.parallelism_dim == "h" and layer.first:
            num_lanes, num_simd_units = getNumSIMDLanes(SCALESIM_CONFIG, NumberOfNPUCores)
            reduction_cycles = int(layer.m * layer.seq_length * layer.k / num_lanes / num_simd_units)
        elif layer.parallelism_dim == "k":
            num_lanes, num_simd_units = getNumSIMDLanes(SCALESIM_CONFIG, NumberOfNPUCores)
            reduction_cycles = int(layer.m * layer.seq_length * layer.n / num_lanes / num_simd_units)
        else:
            reduction_cycles = 100
        return reduction_cycles


class MegatronATTNHybridDataModelParallelStrategy:
    """
    This class models the Megatron-LM hybrid data-model parallelization strategy for Attention (ATTN) layers of Transformers
    """

    def __init__(self):
        pass

    def getCommunicationTypeForFwdPass(self, i, layer):
        if layer.parallelism_dim == "h":
            return NoCommunication
        elif layer.parallelism_dim == "k":
            return AllReduce
        else:
            pass

    def getCommunicationTypeForInpGrad(self, i, layer):
        if layer.parallelism_dim == "h" and layer.first:
            # we only apply the AllReduce once for Q,K,V, since the partial results for those are accumulated locally in each device.
            # As such,for simpliciy we only consider the first layer of the attention (Q_GEN)
            return AllReduce
        else:
            NoCommunication

    def getCommunicationTypeForWeightGrad(self, i, layer):
        return AllReduce


    def getCommunicationSizeForFwdPass(self, i, layer):
        if layer.parallelism_dim == "h":        
            return 0
        elif layer.parallelism_dim == "k":
            return int(layer.m * layer.seq_length * layer.n * DatatypeSize / NumberOfPackages)

    def getCommunicationSizeForInpGrad(self, i, layer):
        if layer.parallelism_dim == "h" and layer.first: 
            return int(layer.m * layer.seq_length * layer.k * DatatypeSize / NumberOfPackages)
        else:
            return 0      

    def getCommunicationSizeForWeightGrad(self, i, layer):
        if layer.param == 1 and layer.num_heads > 0:
            return int(layer.n * layer.k * layer.num_heads * DatatypeSize / NumberOfNPUsPerPackage)
        elif layer.param == 1 and layer.num_heads == 0:
            return int(layer.n * layer.k * DatatypeSize / NumberOfNPUsPerPackage)
        else:
            return DatatypeSize

    def getReductionDelayEstimation(self, i, layer):
        # delay in cycles
        if layer.parallelism_dim == "h" and layer.first:
            num_lanes, num_simd_units = getNumSIMDLanes(SCALESIM_CONFIG, NumberOfNPUCores)
            reduction_cycles = int(layer.m * layer.seq_length * layer.k / num_lanes / num_simd_units / NumberOfPackages)
        elif layer.parallelism_dim == "k":
            num_lanes, num_simd_units = getNumSIMDLanes(SCALESIM_CONFIG, NumberOfNPUCores)
            reduction_cycles = int(layer.m * layer.seq_length * layer.n / num_lanes / num_simd_units / NumberOfPackages)
        else:
            reduction_cycles = 100
        return reduction_cycles

class DataParallelStrategy:

    def __init__(self):
        pass

    def getCommunicationTypeForFwdPass(self, i, layer):
        return NoCommunication

    def getCommunicationTypeForInpGrad(self, i, layer):
        return NoCommunication

    def getCommunicationTypeForWeightGrad(self, i, layer):
        return AllReduce

    def getCommunicationSizeForFwdPass(self, i, layer):
        return 0

    def getCommunicationSizeForInpGrad(self, i, layer):
        return 0

    def getCommunicationSizeForWeightGrad(self, i, layer):
        return int(layer.n * layer.k * DatatypeSize)
    def getReductionDelayEstimation(self, i, layer):
        # delay in cycles
        num_lanes, num_simd_units = getNumSIMDLanes(SCALESIM_CONFIG, NumberOfNPUCores)
        reduction_cycles = int(layer.n * layer.k  / num_lanes / num_simd_units)
        return reduction_cycles


class TransformerDataParallelStrategy:

    def __init__(self):
        pass

    def getCommunicationTypeForFwdPass(self, i, layer):
        return NoCommunication

    def getCommunicationTypeForInpGrad(self, i, layer):
        return NoCommunication

    def getCommunicationTypeForWeightGrad(self, i, layer):
        return AllReduce
     
    def getCommunicationSizeForFwdPass(self, i, layer):
        return 0

    def getCommunicationSizeForInpGrad(self, i, layer):
        return 0

    def getCommunicationSizeForWeightGrad(self, i, layer):
        if layer.param == 1 and layer.num_heads > 0:
            return int(layer.n * layer.k * layer.num_heads * DatatypeSize)
        elif layer.param == 1 and layer.num_heads == 0:
            return int(layer.n * layer.k * DatatypeSize)
        else:
            return DatatypeSize

    def getReductionDelayEstimation(self, i, layer):
        # delay in cycles
        if layer.param == 1 and layer.num_heads > 0:
            num_lanes, num_simd_units = getNumSIMDLanes(SCALESIM_CONFIG, NumberOfNPUCores)
            reduction_cycles = int(layer.n * layer.k * layer.num_heads / num_lanes / num_simd_units)
        elif layer.param == 1 and layer.num_heads == 0:
            num_lanes, num_simd_units = getNumSIMDLanes(SCALESIM_CONFIG, NumberOfNPUCores)
            reduction_cycles = int(layer.n * layer.k  / num_lanes / num_simd_units)
        else:
            reduction_cycles = 100
        return reduction_cycles


class TransformerHybridDataModelParallelStrategy:

    def __init__(self):
        pass

    def getCommunicationTypeForFwdPass(self, i, layer):
        return AllGather

    def getCommunicationTypeForInpGrad(self, i, layer):
        return ReduceScatter

    def getCommunicationTypeForWeightGrad(self, i, layer):
        return AllReduce
     
    def getCommunicationSizeForFwdPass(self, i, layer): # within package
        if layer.num_heads > 0 and 'V_GEN' not in layer.name:
            return int(layer.m * layer.seq_length * layer.n * layer.num_heads * DatatypeSize / NumberOfNPUs)
        elif layer.num_heads > 0 and 'V_GEN' in layer.name:
            return DatatypeSize
        else:
            return int(layer.m * layer.seq_length * layer.n * DatatypeSize / NumberOfNPUs)

    def getCommunicationSizeForInpGrad(self, i, layer): # within package
        if layer.num_heads > 0:
            return int(layer.m * layer.seq_length * layer.k * layer.num_heads * DatatypeSize / NumberOfPackages)
        else:
            return int(layer.m * layer.seq_length * layer.k * DatatypeSize / NumberOfPackages)

    def getCommunicationSizeForWeightGrad(self, i, layer): # between package
        if layer.param == 1 and layer.num_heads > 0:
            return int(layer.n * layer.k * layer.num_heads * DatatypeSize / NumberOfNPUsPerPackage)
        elif layer.param == 1 and layer.num_heads == 0:
            return int(layer.n * layer.k * DatatypeSize / NumberOfNPUsPerPackage)
        else:
            return DatatypeSize

    def getReductionDelayEstimation(self, i, layer):
        # delay in cycles
        # we assume the delay for the AllReduce Operation
        if layer.param == 1 and layer.num_heads > 0:
            num_lanes, num_simd_units = getNumSIMDLanes(SCALESIM_CONFIG, NumberOfNPUCores)
            reduction_cycles = int(layer.n * layer.k * layer.num_heads / num_lanes / num_simd_units / NumberOfNPUsPerPackage)
        elif layer.param == 1 and layer.num_heads == 0:
            num_lanes, num_simd_units = getNumSIMDLanes(SCALESIM_CONFIG, NumberOfNPUCores)
            reduction_cycles = int(layer.n * layer.k  / num_lanes / num_simd_units / NumberOfNPUsPerPackage)
        else:
            reduction_cycles = 100
        return reduction_cycles


# HybridDataModel: data-parallel between packages, model-parallel within package
class HybridDataModelParallelStrategy:

    def __init__(self):
        pass

    def getCommunicationTypeForFwdPass(self, i, layer):
        return AllGather

    def getCommunicationTypeForInpGrad(self, i, layer):
        return ReduceScatter

    def getCommunicationTypeForWeightGrad(self, i, layer):
        return AllReduce

    def getCommunicationSizeForFwdPass(self, i, layer): # within package
        return int(layer.m * layer.n * DatatypeSize / NumberOfNPUs)

    def getCommunicationSizeForInpGrad(self, i, layer): # within package
        return int(layer.m * layer.k * DatatypeSize / NumberOfPackages)

    def getCommunicationSizeForWeightGrad(self, i, layer): # between package
        return int(layer.k * layer.n * DatatypeSize / NumberOfNPUsPerPackage)
        
    def getReductionDelayEstimation(self, i, layer):
        # delay in cycles
        num_lanes, num_simd_units = getNumSIMDLanes(SCALESIM_CONFIG, NumberOfNPUCores)
        # we assume the delay for the AllReduce Operation
        reduction_cycles = int(layer.n * layer.k  / num_lanes / num_simd_units / NumberOfNPUsPerPackage)
        return reduction_cycles
    



# HybridModelData: model-parallel between pacakges, data-parallel within pacakge
class HybridModelDataParallelStrategy:

    def __init__(self):
        pass

    def getCommunicationTypeForFwdPass(self, i, layer):
        return AllGather

    def getCommunicationTypeForInpGrad(self, i, layer):
        return ReduceScatter

    def getCommunicationTypeForWeightGrad(self, i, layer):
        return AllReduce

    def getCommunicationSizeForFwdPass(self, i, layer): # between packages, all-gather
        return int(layer.m * layer.n * DatatypeSize / NumberOfNPUs)

    def getCommunicationSizeForInpGrad(self, i, layer): # between packages, all-reduce
        return int(layer.m * layer.k * DatatypeSize / NumberOfNPUsPerPackage)

    def getCommunicationSizeForWeightGrad(self, i, layer): # within packages
        return int(layer.n * layer.k * DatatypeSize / NumberOfPackages)

class AstraSimOutput:
    def __init__(self, layers, scaleSimOutput):
        self.layers = layers
        self.scaleSimOutput = scaleSimOutput
        self.strategy = {}
        self.strategy[ModelParallel] = ModelParallelStrategy()
        self.strategy[DataParallel] = DataParallelStrategy()
        self.strategy[HybridDataModelParallel] = HybridDataModelParallelStrategy()
        self.strategy[HybridModelDataParallel] = HybridModelDataParallelStrategy()
        self.strategy[MegatronMLPModelParallel] = MegatronMLPModelParallelStrategy()
        self.strategy[MegatronATTNModelParallel] = MegatronATTNModelParallelStrategy()
        self.strategy[MegatronMLPHybridDataModelParallel] = MegatronMLPHybridDataModelParallelStrategy()
        self.strategy[MegatronATTNHybridDataModelParallel] = MegatronATTNHybridDataModelParallelStrategy()
        self.strategy[TransformerDataParallel] = TransformerDataParallelStrategy()
        self.strategy[TransformerHybridDataModelParallel] = TransformerHybridDataModelParallelStrategy()

        self.output = []

    def generate(self):
        self.output = []
        length = len(self.layers)
        for i in range(0, length):
            line = []
            line.append(self.layers[i].name) # Layer name
            line.append("-1") # Reserved variable

            if (self.layers[i].parallelism_dim == "h" and self.layers[i].parallelism == MegatronATTNModelParallel):
                # this is a transformer multi-head attention layer, we need to scale the compute cycles based on parallelization of heads
                line.append(int(self.scaleSimOutput[ForwardPassCycles][i]) * math.ceil(self.layers[i].num_heads / NumberOfNPUs)) # Forward pass compute time scaled by the number of heads/NPU
            elif (self.layers[i].parallelism_dim == "h" and self.layers[i].parallelism == MegatronATTNHybridDataModelParallel):
                # this is a transformer multi-head attention layer, we need to scale the compute cycles based on parallelization of heads
                line.append(int(self.scaleSimOutput[ForwardPassCycles][i]) * math.ceil(self.layers[i].num_heads / NumberOfNPUsPerPackage)) # Forward pass compute time scaled by the number of heads/NPU/package             
            elif ((self.layers[i].parallelism == TransformerDataParallel or self.layers[i].parallelism == TransformerHybridDataModelParallel) and self.layers[i].num_heads > 0):
                line.append(int(self.scaleSimOutput[ForwardPassCycles][i]) * self.layers[i].num_heads) # Forward pass compute time scaled by the number of heads
            else:
                line.append(self.scaleSimOutput[ForwardPassCycles][i]) # Forward pass compute time

            line.append(self.strategy[self.layers[i].parallelism].getCommunicationTypeForFwdPass(i, self.layers[i])) # Forward pass communication type
            line.append(self.strategy[self.layers[i].parallelism].getCommunicationSizeForFwdPass(i, self.layers[i])) # Forward pass communication size

            if (self.layers[i].parallelism_dim == "h" and self.layers[i].parallelism == MegatronATTNModelParallel):
                if self.layers[i].param == 0:
                    # this is QK and QKV that requires two computations for input grad
                    line.append(int(self.scaleSimOutput[InputGradientCycles][i]) * 2 * math.ceil(self.layers[i].num_heads / NumberOfNPUs)) # Input gradient compute time scaled by the number of heads/NPU
                else:
                    line.append(int(self.scaleSimOutput[InputGradientCycles][i]) * math.ceil(self.layers[i].num_heads / NumberOfNPUs)) # Input gradient compute time scaled by the number of heads/NPU
            elif (self.layers[i].parallelism_dim == "h" and self.layers[i].parallelism == MegatronATTNHybridDataModelParallel):
                if self.layers[i].param == 0:
                    # this is QK and QKV that requires two computations for input grad
                    line.append(int(self.scaleSimOutput[InputGradientCycles][i]) * 2 * math.ceil(self.layers[i].num_heads / NumberOfNPUsPerPackage)) # Input gradient compute time scaled by the number of heads/NPU
                else:
                    line.append(int(self.scaleSimOutput[InputGradientCycles][i]) * math.ceil(self.layers[i].num_heads / NumberOfNPUsPerPackage)) # Input gradient compute time scaled by the number of heads/NPU
            elif ((self.layers[i].parallelism == TransformerDataParallel or self.layers[i].parallelism == TransformerHybridDataModelParallel) and self.layers[i].num_heads > 0):
                # TODO: Update this for QK/QKV if needed
                line.append(int(self.scaleSimOutput[InputGradientCycles][i]) * self.layers[i].num_heads) # Input gradient compute time scaled by the number of heads
            else:
                line.append(self.scaleSimOutput[InputGradientCycles][i]) # Input gradient compute time

            line.append(self.strategy[self.layers[i].parallelism].getCommunicationTypeForInpGrad(i, self.layers[i])) # Input gradient communication type
            line.append(self.strategy[self.layers[i].parallelism].getCommunicationSizeForInpGrad(i, self.layers[i])) # Input gradient communication size

            if (self.layers[i].parallelism_dim == "h" and self.layers[i].parallelism == MegatronATTNModelParallel and self.layers[i].param == 1):
                line.append(int(self.scaleSimOutput[WeightGradientCycles][i]) * math.ceil(self.layers[i].num_heads / NumberOfNPUs)) # Weight gradient compute time scaled by the number of heads/NPU
            elif (self.layers[i].parallelism_dim == "h" and self.layers[i].parallelism == MegatronATTNHybridDataModelParallel and self.layers[i].param == 1):
                line.append(int(self.scaleSimOutput[WeightGradientCycles][i]) * math.ceil(self.layers[i].num_heads / NumberOfNPUsPerPackage)) # Weight gradient compute time scaled by the number of heads/NPU/package
            elif (self.layers[i].parallelism_dim == "h" and (self.layers[i].parallelism == MegatronATTNModelParallel or self.layers[i].parallelism == MegatronATTNHybridDataModelParallel) and self.layers[i].param == 0):
                # these are the Q.K and V(Q.K) layers of the transformer layers that do not have params, we set the weight update cycles to zero for them
                line.append(10) # Weight gradient compute time set to zero (10) for transformer layers that do not have params

            elif ((self.layers[i].parallelism == TransformerDataParallel or self.layers[i].parallelism == TransformerHybridDataModelParallel) and self.layers[i].num_heads > 0 and self.layers[i].param == 1):
                line.append(int(self.scaleSimOutput[WeightGradientCycles][i]) * self.layers[i].num_heads) # Weight gradient compute time scaled by the number of heads
            elif ((self.layers[i].parallelism == TransformerDataParallel or self.layers[i].parallelism == TransformerHybridDataModelParallel) and self.layers[i].num_heads > 0 and self.layers[i].param == 0):
                line.append(0) # Weight gradient compute time set to zero for transformer layers that do not have params
            else:
                line.append(self.scaleSimOutput[WeightGradientCycles][i]) # Weight gradient compute time

            line.append(self.strategy[self.layers[i].parallelism].getCommunicationTypeForWeightGrad(i, self.layers[i])) # Weight gradient communication type
            line.append(self.strategy[self.layers[i].parallelism].getCommunicationSizeForWeightGrad(i, self.layers[i])) # Weight gradient communication size
            line.append(self.strategy[self.layers[i].parallelism].getReductionDelayEstimation(i, self.layers[i]))  # Delay for redcution
            # line.append(self.layers[i].parallelism)

            line = map(lambda x: str(x), line)
            line_string = "\t".join(line)

            DPRINT(line_string)
            self.output.append(line_string)

    def writeToFile(self):
        # TODO: Right now, the first few in the file are hardcoded. Instead, we must find a way to generate this
        file_handle = open(OUTPUT_FILE_NAME, "w")
        # if ParallelizationStrategy == "MEGATRON_MODEL" or ParallelizationStrategy == "MODEL":
        #     file_handle.write("MODEL" + "\n")
        # else:
        file_handle.write(ParallelizationStrategy + "\n")
        file_handle.write(str(len(self.output)))
        for line in self.output:
            file_handle.write("\n")
            file_handle.write(line)
        file_handle.close()


def getNumSIMDLanes(SCALESIM_CONFIG, NumberOfNPUCores):
    """
    This function sets the number of lanes for the SIMD Vector Unit based on the configuration of the systolic array to be used for reduction estimation cycles.
    """
    config = cp.ConfigParser()
    config.read(SCALESIM_CONFIG)
    num_lanes = int(config.get('architecture_presets', 'ArrayHeight'))
    num_simd_units = NumberOfNPUCores

    return num_lanes, num_simd_units




class TopologyItem:
    def __init__(self, name, ifmap_height, ifmap_width, filter_height, filter_width, channels, num_filters, strides):
        self.name = name
        self.ifmap_height = ifmap_height
        self.ifmap_width = ifmap_width
        self.filter_height = filter_height
        self.filter_width = filter_width
        self.channels = channels
        self.num_filters = num_filters
        self.strides = strides

    def print(self):
        line = [self.name, self.ifmap_height, self.ifmap_width, self.filter_height, self.filter_width, self.channels, self.num_filters, self.strides]
        line = list(map(lambda x: str(x), line))
        line = "\t".join(line)
        DPRINT(line)

    def write(self, file_handle):
        line = [self.name, self.ifmap_height, self.ifmap_width, self.filter_height, self.filter_width, self.channels, self.num_filters, self.strides, ""]
        line = list(map(lambda x: str(x), line))
        file_handle.write(",".join(line))
        file_handle.write("\n")

    @staticmethod
    def printHeader():
         DPRINT("Layer name\tifmap height\tifmap width\tfilter height\tfilter width\tchannels\tnum filter\tstrides")

    @staticmethod
    def writeHeaderToFile(file_handle):
        file_handle.write("Layer name,ifmap height,ifmap width,filter height,filter width,channels,num filter,strides,\n")

class Layer:
    # We use this class for regular GEMM layers and convolution layers
    def __init__(self, name, m, n, k, parallelism, parallelism_dim):
        self.name = name
        self.m = int(m) # batch size
        self.n = int(n) # output dimension
        self.k = int(k) # input dimension
        self.parallelism = parallelism
        self.parallelism_dim = parallelism_dim # the parallelism dim for specific strategy. for example in MP we can have both col and row slicing

    def __init__(self, row):
        self.name = row[0]
        self.m = int(row[1])
        self.n = int(row[2])
        self.k = int(row[3])
        self.parallelism = row[4]
        self.parallelism_dim = row[5]


    def print(self):
        # FIX ISSUE #41
        # print (self.name + ", " + self.m + ", " + self.n + ", " + self.k + ", " + self.parallelism)
        outlog = ", ".join([str(x) for x in [self.name, self.m, self.n, self.k, self.parallelism, self.parallelism_dim]])
        print(outlog)


class TransformerLayer:
    # We use this class for Transformer Layers
    def __init__(self, name, m, n, k, parallelism, parallelism_dim, seq_length, num_heads, first, param):
        self.name = name
        self.m = int(m) # batch size
        self.n = int(n) # output dimension
        self.k = int(k) # input dimension
        self.parallelism = parallelism
        self.parallelism_dim = parallelism_dim # the parallelism dim for specific strategy. for example in MP we can have both col and row slicing
        self.seq_length = int(seq_length)
        self.num_heads = int(num_heads)
        self.first = first
        self.param = int(param)

    def __init__(self, row):
        self.name = row[0]
        self.m = int(row[1])
        self.n = int(row[2])
        self.k = int(row[3])
        self.parallelism = row[4]
        self.parallelism_dim = row[5]
        self.seq_length = int(row[6])
        self.num_heads = int(row[7])
        self.first = int(row[9]) == 1
        self.param = int(row[8])

        

    def print(self):
        # FIX ISSUE #41
        # print (self.name + ", " + self.m + ", " + self.n + ", " + self.k + ", " + self.parallelism)
        outlog = ", ".join([str(x) for x in [self.name, self.m, self.n, self.k, self.parallelism, self.parallelism_dim]])
        print(outlog)



def writeGeneratedTopologyToFile(folder_name, file_name, items):
    current_path = os.getcwd()

    # Create the output directory if does not exist and change to output directory
    output_folder_path = os.path.join(current_path, Outputs)
    if not os.path.exists(output_folder_path):
        os.makedirs(Outputs)
    os.chdir(output_folder_path)

    output_folder_path = os.path.join(output_folder_path, folder_name)
    if not os.path.exists(output_folder_path):
        os.makedirs(folder_name)
    os.chdir(output_folder_path)

    # Create the scale sim output directory if does not exist
    #run_name_path = os.path.join(output_folder_path, "scaleSimOutput")
    #if not os.path.exists(run_name_path):
    #    os.makedirs("scaleSimOutput")

    # Create the run name directory if does not exist and change change to run name directory
    run_name_path = os.path.join(output_folder_path, FLAGS.run_name)
    if not os.path.exists(run_name_path):
        os.makedirs(FLAGS.run_name)
    os.chdir(run_name_path)

    # Write the generated topology to a file
    file_handle = open(file_name, "w")
    TopologyItem.writeHeaderToFile(file_handle)
    for row in items:
        row.write(file_handle)
    file_handle.close()

    # Go back to old directory
    os.chdir(current_path)

def runScaleSim(topology_file, folder_name):
    global SCALESIM_OUTPUT_PATH
    global scale_sim_installed_flag
    current_path = os.getcwd()
    full_path = os.path.join(os.getcwd(), Outputs, folder_name, FLAGS.run_name, topology_file)

    if SCALESIM_VER == 2:
        if not scale_sim_installed_flag:
            os.chdir(SCALESIM_PATH)
            process = subprocess.Popen(["python3", "setup.py", "install"])
            process.wait()
            os.chdir(current_path)
            scale_sim_installed_flag = True

        SCALESIM_RUN_PATH = SCALESIM_PATH + '/scalesim/'
    else:
        SCALESIM_RUN_PATH = SCALESIM_PATH

    os.chdir(SCALESIM_RUN_PATH)
    if SCALESIM_VER == 2:
        SCALESIM_OUTPUT_PATH = current_path + "/outputs/"
        process = subprocess.Popen(["python3", "scale.py",
                                    "-c", SCALESIM_CONFIG,
                                    "-t", full_path,
                                    "-p", SCALESIM_OUTPUT_PATH])
    else:
        process = subprocess.Popen(["python3", "scale.py", "-arch_config=" + SCALESIM_CONFIG, "-network="+full_path])
    process.wait()
    os.chdir(current_path)

def getCylesFromScaleSimOutput(folder_name, topology_file):
    config = cp.ConfigParser()
    config.read(SCALESIM_CONFIG)
    run_name = config.get('general', 'run_name').strip('"')

    if SCALESIM_VER == 2:
        cycles_filename = "COMPUTE_REPORT.csv"
    else:
        cycles_filename = topology_file.rstrip("csv").rstrip(".") + "_cycles.csv" # remove .csv frim topology file and makes it _cycles.csv

    current_path = os.getcwd()
    if SCALESIM_VER == 2:
        full_path = SCALESIM_OUTPUT_PATH + '/' + run_name
    else:
        full_path = os.path.join(SCALESIM_PATH, "outputs", run_name)
    os.chdir(full_path)
    cpy_path = os.path.join(current_path, Outputs, folder_name, FLAGS.run_name)
    process = subprocess.call("cp -rf " + cycles_filename + " " + cpy_path, shell=True)

    file_handle = open(cycles_filename, "r")
    lines = file_handle.readlines()
    lines = map(lambda x: x.strip("\n"), lines) # removes the newline characters from the end

    first = True
    cycles = []

    for line in lines:
        print("line", line)
        if first:
            first = False
            continue
        cycles.append(line.split(',')[1].strip('\t')) # '\t removes the last preceeding tab character preesnt in scalesim output

    os.chdir(current_path)
    return cycles

def getScaleSimOutputInternal(fwd_pass, inp_grad, weight_grad, folder_name):
    fwd_pass_filename = os.path.basename(FLAGS.mnk).rstrip(".csv") + "_fwd_pass.csv"
    inp_grad_filename = os.path.basename(FLAGS.mnk).rstrip(".csv") + "_inp_grad.csv"
    weight_grad_filename = os.path.basename(FLAGS.mnk).rstrip(".csv") + "_weight_grad.csv"
    writeGeneratedTopologyToFile(folder_name, fwd_pass_filename, fwd_pass)
    writeGeneratedTopologyToFile(folder_name, inp_grad_filename, inp_grad)
    writeGeneratedTopologyToFile(folder_name, weight_grad_filename, weight_grad)

    runScaleSim(fwd_pass_filename, folder_name)
    runScaleSim(inp_grad_filename, folder_name)
    runScaleSim(weight_grad_filename, folder_name)

    fwd_pass_cycles = getCylesFromScaleSimOutput(folder_name, fwd_pass_filename)
    inp_grad_cycles = getCylesFromScaleSimOutput(folder_name, inp_grad_filename)
    weight_grad_cycles = getCylesFromScaleSimOutput(folder_name, weight_grad_filename)

    return { ForwardPassCycles : fwd_pass_cycles, InputGradientCycles : inp_grad_cycles, WeightGradientCycles : weight_grad_cycles }

def getLayerTopologyForDataParallelApproach(layer):
    fwd_pass_item = TopologyItem(layer.name, math.ceil(layer.m / NumberOfNPUs / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), layer.k, FilterHeight, layer.k, NumberOfChannels, layer.n, Strides)

    inp_grad_item = TopologyItem(layer.name, math.ceil(layer.m / NumberOfNPUs / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), layer.n, FilterHeight, layer.n, NumberOfChannels, layer.k, Strides)

    weight_grad_item = TopologyItem(layer.name, math.ceil(layer.k / NumberOfNPUCores / NumberofMXUCoresPerNPUCore) , math.ceil(layer.m / NumberOfNPUs), FilterHeight, math.ceil(layer.m / NumberOfNPUs), NumberOfChannels, layer.n, Strides)

    return fwd_pass_item, inp_grad_item, weight_grad_item



def getLayerTopologyForTransformerDataParallelApproach(layer):
    fwd_pass_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUs / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), layer.k, FilterHeight, layer.k, NumberOfChannels, layer.n, Strides)

    inp_grad_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUs / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), layer.n, FilterHeight, layer.n, NumberOfChannels, layer.k, Strides)

    weight_grad_item = TopologyItem(layer.name, math.ceil(layer.k / NumberOfNPUCores / NumberofMXUCoresPerNPUCore) , math.ceil(layer.m * layer.seq_length / NumberOfNPUs), FilterHeight, math.ceil(layer.m * layer.seq_length / NumberOfNPUs), NumberOfChannels, layer.n, Strides)      
    return fwd_pass_item, inp_grad_item, weight_grad_item

def getLayerTopologyForTransformerHybridDataModelParallelApproach(layer):
    fwd_pass_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfPackages / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), layer.k, FilterHeight, layer.k, NumberOfChannels, math.ceil(layer.n / NumberOfNPUsPerPackage), Strides)

    inp_grad_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfPackages / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), math.ceil(layer.n / NumberOfNPUsPerPackage), FilterHeight, math.ceil(layer.n / NumberOfNPUsPerPackage), NumberOfChannels, layer.k, Strides)

    weight_grad_item = TopologyItem(layer.name, math.ceil(layer.k / NumberOfNPUCores / NumberofMXUCoresPerNPUCore) , math.ceil(layer.m * layer.seq_length / NumberOfPackages), FilterHeight, math.ceil(layer.m * layer.seq_length / NumberOfPackages), NumberOfChannels, math.ceil(layer.n / NumberOfNPUsPerPackage), Strides)      
    return fwd_pass_item, inp_grad_item, weight_grad_item


# HybridDataModel: data-parallel between packages, model-parallel within package
def getLayerTopologyForHybridDataModelParallelApproach(layer):
    fwd_pass_item = TopologyItem(layer.name, math.ceil(layer.m / NumberOfPackages / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), layer.k, FilterHeight, layer.k, NumberOfChannels, math.ceil(layer.n / NumberOfNPUsPerPackage), Strides)

    inp_grad_item = TopologyItem(layer.name, math.ceil(layer.m / NumberOfPackages / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), math.ceil(layer.n / (NumberOfNPUsPerPackage)), FilterHeight, math.ceil(layer.n / NumberOfNPUsPerPackage), NumberOfChannels, layer.k, Strides)

    weight_grad_item = TopologyItem(layer.name, math.ceil(layer.k / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), math.ceil(layer.m / NumberOfPackages), FilterHeight, math.ceil(layer.m / NumberOfPackages), NumberOfChannels, math.ceil(layer.n / NumberOfNPUsPerPackage), Strides)

    return fwd_pass_item, inp_grad_item, weight_grad_item

# HybridModelData: model-parallel between packages, data-parallel within package
def getLayerTopologyForHybridModelDataParallelApproach(layer):
    fwd_pass_item = TopologyItem(layer.name, math.ceil(layer.m / NumberOfNPUsPerPackage), layer.k, FilterHeight, layer.k, NumberOfChannels, math.ceil(layer.n / NumberOfPackages), Strides)

    inp_grad_item = TopologyItem(layer.name, math.ceil(layer.m / NumberOfNPUsPerPackage), math.ceil(layer.n / NumberOfPackages), FilterHeight, math.ceil(layer.n / NumberOfPackages), NumberOfChannels, layer.k, Strides)

    weight_grad_item = TopologyItem(layer.name, layer.k, math.ceil(layer.m / NumberOfNPUsPerPackage), FilterHeight, math.ceil(layer.m / NumberOfNPUsPerPackage), NumberOfChannels, math.ceil(layer.n / NumberOfPackages), Strides)

    return fwd_pass_item, inp_grad_item, weight_grad_item

def getLayerTopologyForMegatronMLPModelParallelApproach(layer):
    # SG: Adding the option to partition the layer params in either row/col dimensions
    # SG: We use this for Megatron-MLP-Model-Parallel 
    # SG: Added the Num_cores effect, parallelizing the "batch" dimension across the cores

    if layer.parallelism_dim == "n":
        fwd_pass_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), layer.k, FilterHeight, layer.k, NumberOfChannels, math.ceil(layer.n / NumberOfNPUs), Strides)

        inp_grad_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), math.ceil(layer.n / NumberOfNPUs), FilterHeight, math.ceil(layer.n / NumberOfNPUs), NumberOfChannels, layer.k, Strides)

        weight_grad_item = TopologyItem(layer.name, math.ceil(layer.k / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), layer.m * layer.seq_length, FilterHeight, layer.m * layer.seq_length, NumberOfChannels, math.ceil(layer.n / NumberOfNPUs), Strides)

    elif layer.parallelism_dim == "k":
        fwd_pass_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), math.ceil(layer.k / NumberOfNPUs), FilterHeight, math.ceil(layer.k / NumberOfNPUs), NumberOfChannels, layer.n, Strides)

        inp_grad_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), layer.n, FilterHeight, layer.n, NumberOfChannels, math.ceil(layer.k / NumberOfNPUs), Strides)

        weight_grad_item = TopologyItem(layer.name, math.ceil(layer.k / NumberOfNPUs / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), layer.m * layer.seq_length, FilterHeight, layer.m * layer.seq_length, NumberOfChannels, layer.n, Strides)
    
    else:
        pass

    return fwd_pass_item, inp_grad_item, weight_grad_item


def getLayerTopologyForMegatronMLPHybridDataModelParallelApproach(layer):
    # SG: Adding the option to partition the layer params in either row/col dimensions
    # SG: We use this for Megatron-MLP-Model-Parallel 
    # SG: Added the Num_cores effect, parallelizing the "batch" dimension across the cores

    if layer.parallelism_dim == "n":
        fwd_pass_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUCores / NumberofMXUCoresPerNPUCore / NumberOfPackages), layer.k, FilterHeight, layer.k, NumberOfChannels, math.ceil(layer.n / NumberOfNPUsPerPackage), Strides)

        inp_grad_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUCores / NumberofMXUCoresPerNPUCore / NumberOfPackages), math.ceil(layer.n / NumberOfNPUsPerPackage), FilterHeight, math.ceil(layer.n / NumberOfNPUsPerPackage), NumberOfChannels, layer.k, Strides)

        weight_grad_item = TopologyItem(layer.name, math.ceil(layer.k / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), math.ceil(layer.m * layer.seq_length / NumberOfPackages), FilterHeight, math.ceil(layer.m * layer.seq_length / NumberOfPackages), NumberOfChannels, math.ceil(layer.n / NumberOfNPUsPerPackage), Strides)

    elif layer.parallelism_dim == "k":
        fwd_pass_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUCores / NumberofMXUCoresPerNPUCore / NumberOfPackages), math.ceil(layer.k / NumberOfNPUsPerPackage), FilterHeight, math.ceil(layer.k / NumberOfNPUsPerPackage), NumberOfChannels, layer.n, Strides)

        inp_grad_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUCores / NumberofMXUCoresPerNPUCore / NumberOfPackages), layer.n, FilterHeight, layer.n, NumberOfChannels, math.ceil(layer.k / NumberOfNPUsPerPackage), Strides)

        weight_grad_item = TopologyItem(layer.name, math.ceil(layer.k / NumberOfNPUsPerPackage / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), math.ceil(layer.m * layer.seq_length / NumberOfPackages), FilterHeight, math.ceil(layer.m * layer.seq_length / NumberOfPackages), NumberOfChannels, layer.n, Strides)
    
    else:
        pass

    return fwd_pass_item, inp_grad_item, weight_grad_item

def getLayerTopologyForMegatronATTNModelParallelApproach(layer):
    # SG: We use this for Megatron-ATTN-Model-Parallel 
    # SG: Added the Num_cores effect, parallelizing the "batch" dimension across the cores

    if layer.parallelism_dim == "h":
        fwd_pass_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), layer.k, FilterHeight, layer.k, NumberOfChannels, layer.n, Strides)

        inp_grad_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), layer.n, FilterHeight, layer.n, NumberOfChannels, layer.k, Strides)

        weight_grad_item = TopologyItem(layer.name, math.ceil(layer.k / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), layer.m * layer.seq_length, FilterHeight, layer.m * layer.seq_length, NumberOfChannels, layer.n, Strides)

    elif layer.parallelism_dim == "k":
        fwd_pass_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), math.ceil(layer.k / NumberOfNPUs), FilterHeight, math.ceil(layer.k / NumberOfNPUs), NumberOfChannels, layer.n, Strides)

        inp_grad_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), layer.n, FilterHeight, layer.n, NumberOfChannels, math.ceil(layer.k / NumberOfNPUs), Strides)

        weight_grad_item = TopologyItem(layer.name, math.ceil(layer.k / NumberOfNPUs / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), layer.m * layer.seq_length, FilterHeight, layer.m * layer.seq_length, NumberOfChannels, layer.n, Strides)
    
    else:
        pass

    return fwd_pass_item, inp_grad_item, weight_grad_item

def getLayerTopologyForMegatronATTNHybridDataModelParallelApproach(layer):
    # SG: We use this for Megatron-ATTN-Model-Parallel 
    # SG: Added the Num_cores effect, parallelizing the "batch" dimension across the cores
    print("here", layer.name)

    if layer.parallelism_dim == "h":
        fwd_pass_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUCores / NumberofMXUCoresPerNPUCore / NumberOfPackages), layer.k, FilterHeight, layer.k, NumberOfChannels, layer.n, Strides)

        inp_grad_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUCores / NumberofMXUCoresPerNPUCore / NumberOfPackages), layer.n, FilterHeight, layer.n, NumberOfChannels, layer.k, Strides)

        weight_grad_item = TopologyItem(layer.name, math.ceil(layer.k / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), math.ceil(layer.m * layer.seq_length / NumberOfPackages), FilterHeight, math.ceil(layer.m * layer.seq_length / NumberOfPackages), NumberOfChannels, layer.n, Strides)

    elif layer.parallelism_dim == "k":
        fwd_pass_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUCores / NumberofMXUCoresPerNPUCore / NumberOfPackages), math.ceil(layer.k / NumberOfNPUsPerPackage), FilterHeight, math.ceil(layer.k / NumberOfNPUsPerPackage), NumberOfChannels, layer.n, Strides)

        inp_grad_item = TopologyItem(layer.name, math.ceil(layer.m * layer.seq_length / NumberOfNPUCores / NumberofMXUCoresPerNPUCore / NumberOfPackages), layer.n, FilterHeight, layer.n, NumberOfChannels, math.ceil(layer.k / NumberOfNPUsPerPackage), Strides)

        weight_grad_item = TopologyItem(layer.name, math.ceil(layer.k / NumberOfNPUsPerPackage / NumberOfNPUCores / NumberofMXUCoresPerNPUCore), math.ceil(layer.m * layer.seq_length / NumberOfPackages), FilterHeight, math.ceil(layer.m * layer.seq_length / NumberOfPackages), NumberOfChannels, layer.n, Strides)
    
    else:
        pass

    return fwd_pass_item, inp_grad_item, weight_grad_item

def getLayerTopologyForModelParallelApproach(layer):
    fwd_pass_item = TopologyItem(layer.name, layer.m, layer.k, FilterHeight, layer.k, NumberOfChannels, math.ceil(layer.n / NumberOfNPUs), Strides)

    inp_grad_item = TopologyItem(layer.name, layer.m, math.ceil(layer.n / NumberOfNPUs), FilterHeight, math.ceil(layer.n / NumberOfNPUs), NumberOfChannels, layer.k, Strides)

    weight_grad_item = TopologyItem(layer.name, layer.k, layer.m, FilterHeight, layer.m, NumberOfChannels, math.ceil(layer.n / NumberOfNPUs), Strides)

    return fwd_pass_item, inp_grad_item, weight_grad_item

def getTopology(layers):
    fwd_pass = []
    inp_grad = []
    weight_grad = []

    for layer in layers:
        if layer.parallelism == DataParallel:
            fwd_pass_item, inp_grad_item, weight_grad_item = getLayerTopologyForDataParallelApproach(layer)
        elif layer.parallelism == TransformerDataParallel:
            fwd_pass_item, inp_grad_item, weight_grad_item = getLayerTopologyForTransformerDataParallelApproach(layer)
        elif layer.parallelism == TransformerHybridDataModelParallel:
            fwd_pass_item, inp_grad_item, weight_grad_item = getLayerTopologyForTransformerHybridDataModelParallelApproach(layer)
        elif layer.parallelism == ModelParallel:
            fwd_pass_item, inp_grad_item, weight_grad_item = getLayerTopologyForModelParallelApproach(layer)
        elif layer.parallelism == HybridDataModelParallel:
            fwd_pass_item, inp_grad_item, weight_grad_item = getLayerTopologyForHybridDataModelParallelApproach(layer)
        elif layer.parallelism == HybridModelDataParallel:
            fwd_pass_item, inp_grad_item, weight_grad_item = getLayerTopologyForHybridModelDataParallelApproach(layer)
        elif layer.parallelism == MegatronMLPModelParallel:
            fwd_pass_item, inp_grad_item, weight_grad_item = getLayerTopologyForMegatronMLPModelParallelApproach(layer)
        elif layer.parallelism == MegatronATTNModelParallel:
            fwd_pass_item, inp_grad_item, weight_grad_item = getLayerTopologyForMegatronATTNModelParallelApproach(layer)
        elif layer.parallelism == MegatronMLPHybridDataModelParallel:
            fwd_pass_item, inp_grad_item, weight_grad_item = getLayerTopologyForMegatronMLPHybridDataModelParallelApproach(layer)
        elif layer.parallelism == MegatronATTNHybridDataModelParallel:
            fwd_pass_item, inp_grad_item, weight_grad_item = getLayerTopologyForMegatronATTNHybridDataModelParallelApproach(layer)
        else:
            raise RuntimeError("Invalid parallelization strategy {}".format(layer.parallelism))

        fwd_pass.append(fwd_pass_item)
        inp_grad.append(inp_grad_item)
        weight_grad.append(weight_grad_item)

    return fwd_pass, inp_grad, weight_grad


def main(argv):
    parseCommandLineArguments()
    mnk_file = FLAGS.mnk
    file_handle = open(mnk_file, "r")
    lines = file_handle.readlines()
    file_handle.close()
    first = True
    layers = []

    for line in lines:
        if first:
            first = False
            continue
        line = line.strip('\n').strip(' ')
        cols = line.split(",")
        if ParallelizationStrategy == "HYBRID_CUSTOMIZED":
            # SG: adding one more column to express the parallelization dim
            # assert len(cols) == 5, "There should be 5 columns in the mnk file"
            assert len(cols) == 6, "There should be 6 columns in the mnk file"
            layers.append(Layer(cols))
        elif ParallelizationStrategy == "MEGATRON_MODEL" or ParallelizationStrategy == "MEGATRON_HYBRID_DATA_MODEL" or \
                                        ParallelizationStrategy == "TRANSFORMER_DATA" or ParallelizationStrategy == "TRANSFORMER_HYBRID_DATA_MODEL":

            assert len(cols) == 10, "There should be 10 columns in the mnk file"
            layers.append(TransformerLayer(cols))
        else:
            # SG: adding one more column to express the parallelization dim
            # assert len(cols) == 4 or len(cols), "There should be 4 columns in the mnk file"
            assert len(cols) == 6, "There should be 5 columns in the mnk file"
            # if len(cols) == 5:
            #     cols.append(ParallelizationStrategy)
            # else:
            #     cols[-1] = ParallelizationStrategy
            layers.append(Layer(cols))
        print(cols)
            

    fwd_pass, inp_grad, weight_grad = getTopology(layers)

    # scaleSimOutput = getScaleSimOutputInternal(fwd_pass, inp_grad, weight_grad, ParallelizationStrategy)
    simulation_mode = "analytical"
    dnn_name = (FLAGS.mnk).rstrip(".csv")
    dnn_name = dnn_name.replace("mnk_inputs/", "")
    run_name = FLAGS.run_name + "_" + simulation_mode
    
    scaleSimOutput = get_analytical_output_internal(fwd_pass, inp_grad, weight_grad, ParallelizationStrategy, dnn_name, 
                                                                    run_name, SCALESIM_CONFIG, 0, 0, 0, "list")

    astraSimOutput = AstraSimOutput(layers, scaleSimOutput)
    astraSimOutput.generate()
    astraSimOutput.writeToFile()

if __name__ == '__main__':
    app.run(main)
