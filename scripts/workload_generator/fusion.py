from distutils.command.config import config
import math
import os
import pandas as pd
import configparser as cp

from gen_astrasim_workload_input import Layer, TransformerLayer, TopologyItem
from utils import divisorGenerator, get_the_minimum_greater_than_element_of_list
from fusion_scalesim import get_scalesim_output_internal

##############################
### Input parameters and files ###
# in bytes
data_type_size = 4
num_npus = 512
mp_degree = 8
dp_degree = 64
assert mp_degree * dp_degree == num_npus, 'There is mismatch between number of NPUs and DP/MP degree'
dnn_name = "resnet50"
dnn_type = "CNN"
# NPU
num_cores = 2
num_mxu_cores = 2
num_subarrays = 16
# Freq in Mhz
frequency = 1000
# Network
# BW in GB/sec
nw_bw = 40
# latency in cycles
nw_latency = 200
num_nw_links = 2
# Fusion pipeline delay overhead
fusion_pipeline_overhead_coeff = 0.2
###
###################################

run_name = dnn_name + "_" + "{}".format(dp_degree) + "DP_" + "{}".format(mp_degree) + "MP_tpuv3_" + "{}".format(num_npus) + "_npu_fusion"

path_to_dnn_csv_file = "mnk_inputs/" + dnn_name + ".csv"

path_to_dnn_fwd_cycles_csv = "outputs/HYBRID_DATA_MODEL/" + dnn_name + "_" + "{}".format(dp_degree) + "DP_" + "{}".format(mp_degree) + "MP_tpuv3_" + "{}".format(num_npus) + "_npu/" + \
                                                                            dnn_name + "_fwd_pass_cycles.csv"
path_to_dnn_inp_grad_cycles_csv = "outputs/HYBRID_DATA_MODEL/" + dnn_name + "_" + "{}".format(dp_degree) + "DP_" + "{}".format(mp_degree) + "MP_tpuv3_" + "{}".format(num_npus) + "_npu/" + \
                                                                            dnn_name + "_inp_grad_cycles.csv"
path_to_dnn_weight_grad_cycles_csv = "outputs/HYBRID_DATA_MODEL/" + dnn_name + "_" + "{}".format(dp_degree) + "DP_" + "{}".format(mp_degree) + "MP_tpuv3_" + "{}".format(num_npus) + "_npu/" + \
                                                                            dnn_name + "_weight_grad_cycles.csv"

path_to_npu_config_file = "../../extern/compute/SCALE-Sim-V1/SCALE-Sim/configs/google_smaller.cfg"
path_to_scalesim = "../../extern/compute/SCALE-Sim-V1/SCALE-Sim/"

path_to_output_csv = "fusion_outputs/" + dnn_name + "_" + "{}".format(dp_degree) + "DP_" + "{}".format(mp_degree) + "MP_tpuv3_" + "{}".format(num_npus) + "_npu.csv"

#####################################

def main():

    ### get list of layers from mnk file
    nn_layers = get_dnn_layers_from_mnk_csv(path_to_dnn_csv_file)

    ### create a ChipFusion object
    chip_fusion_obj = FusedChips(mp_degree, dp_degree, num_cores, num_mxu_cores, num_subarrays, path_to_npu_config_file, frequency, 
                                nw_bw, nw_latency, num_nw_links, fusion_pipeline_overhead_coeff)
    chip_fusion_obj.set_npu_dims_from_config_file()
    fission_configs = chip_fusion_obj.get_possible_fission_dims()

    ### get candidate fusion dims to minimize bandwidth pressure
    fusion_dims_list = chip_fusion_obj.get_layer_wise_candidate_fusion_dims(nn_layers)

    ### get layer topologies and fission/fusion requirements
    fusion_info = chip_fusion_obj.get_total_dnn_layer_toplogies_fission_fusion_requirements(nn_layers, fission_configs, fusion_dims_list)

    ###
    fwd_pass_layers_wo_fission = fusion_info['fwd_pass_layers_wo_fission']
    inp_grad_layers_wo_fission = fusion_info['inp_grad_layers_wo_fission']
    weight_grad_layers_wo_fission = fusion_info['weight_grad_layers_wo_fission']

    fwd_pass_layers_w_fission = fusion_info['fwd_pass_layers_w_fission']
    inp_grad_layers_w_fission = fusion_info['inp_grad_layers_w_fission']
    weight_grad_layers_w_fission = fusion_info['weight_grad_layers_w_fission']

    layers_num_compute_tiles_list = fusion_info['layers_num_compute_tiles']

    fwd_layers_array_util_list = fusion_info['fwd_layers_array_util']
    inp_grad_layers_array_util_list = fusion_info['inp_grad_layers_array_util']
    wgt_grad_layers_array_util_list = fusion_info['wgt_grad_layers_array_util']

    fwd_optimal_fission_dims_list = fusion_info['fwd_optimal_fission_dims']
    inp_grad_optimal_fission_dims_list = fusion_info['inp_grad_optimal_fission_dims']
    wgt_grad_optimal_fission_dims_list = fusion_info['wgt_grad_optimal_fission_dims']
    
    layers_communication_size_per_tile_list = fusion_info['layers_communication_size_per_tile']

    ### get scalesim cycles with fusion
    fwd_cycles_w_fusion, inp_grad_cycles_w_fusion, wgt_grad_cycles_w_fusion = get_scalesim_cycles_w_fusion(fwd_pass_layers_wo_fission, inp_grad_layers_wo_fission, 
                                                                    weight_grad_layers_wo_fission, layers_num_compute_tiles_list)
    
    ### get scalesim cycles wo fusion (original hyrid dp-mp)
    cycles_col_name = pd.read_csv(path_to_dnn_fwd_cycles_csv).columns[1]
    fwd_cycles_original = pd.read_csv(path_to_dnn_fwd_cycles_csv)[cycles_col_name].tolist()
    inp_grad_cycles_original = pd.read_csv(path_to_dnn_inp_grad_cycles_csv)[cycles_col_name].tolist()
    wgt_grad_cycles_original = pd.read_csv(path_to_dnn_weight_grad_cycles_csv)[cycles_col_name].tolist()

    ### get ideal fusion dims
    ideal_fusion_dim_dp_2 = chip_fusion_obj.get_layer_wise_fusion_dim_for_full_comp_comm_overlap(nn_layers, fwd_cycles_original, inp_grad_cycles_original, 2, fusion_dims_list)
    ideal_fusion_dim_dp_4 = chip_fusion_obj.get_layer_wise_fusion_dim_for_full_comp_comm_overlap(nn_layers, fwd_cycles_original, inp_grad_cycles_original, 4, fusion_dims_list)
    ideal_fusion_dim_dp_8 = chip_fusion_obj.get_layer_wise_fusion_dim_for_full_comp_comm_overlap(nn_layers, fwd_cycles_original, inp_grad_cycles_original, 8, fusion_dims_list)
    ideal_fusion_dim_dp_16 = chip_fusion_obj.get_layer_wise_fusion_dim_for_full_comp_comm_overlap(nn_layers, fwd_cycles_original, inp_grad_cycles_original, 16, fusion_dims_list)
    ideal_fusion_dim_dp_32 = chip_fusion_obj.get_layer_wise_fusion_dim_for_full_comp_comm_overlap(nn_layers, fwd_cycles_original, inp_grad_cycles_original, 32, fusion_dims_list) 
    ideal_fusion_dim_dp_64 = chip_fusion_obj.get_layer_wise_fusion_dim_for_full_comp_comm_overlap(nn_layers, fwd_cycles_original, inp_grad_cycles_original, 64, fusion_dims_list)    
    ### write to csv
    output_csv_col = ['Layer Name', 'm', 'k', 'n', 'DP Degree', 'MP Degree', 'Fusion Rows', 'Fusion Cols', 
                                                        'Fwd Array Util w Fusion', 'Inp Grad Array Util w Fusion', 'Wgt Grad Array Util w Fusion',
                                                         'Fwd Fission Config', 'Inp Grad Fission Config', 'Wgt Grad Fission Config', 'Num Compute Tiles', 
                                                         'Fwd cycles hybrid dp-mp', 'Fwd cycles w fusion', 'Inp Grad cycles hybrid dp-mp',
                                                          'Inp Grad cycles w fusion', 'Wgt Grad cycles hybrid dp-mp', 'Wgt Grad cycles w fusion', 'Communication Size per Tile',
                                                          'Ideal Fusion, DP=2', 'Ideal Fusion, DP=4', 'Ideal Fusion, DP=8', 'Ideal Fusion, DP=16',
                                                          'Ideal Fusion, DP=32', 'Ideal Fusion, DP=64']
    output_csv_df = pd.DataFrame(columns=output_csv_col)

    for idx, layer in enumerate(nn_layers):
        layer_info = [layer.name, layer.m, layer.k, layer.n, dp_degree, mp_degree, fusion_dims_list[idx][0], fusion_dims_list[idx][1],
                                                        fwd_layers_array_util_list[idx], inp_grad_layers_array_util_list[idx], wgt_grad_layers_array_util_list[idx],
                                                        fwd_optimal_fission_dims_list[idx], inp_grad_optimal_fission_dims_list[idx],
                                                        wgt_grad_optimal_fission_dims_list[idx], layers_num_compute_tiles_list[idx],
                                                        fwd_cycles_original[idx], fwd_cycles_w_fusion[idx], inp_grad_cycles_original[idx],
                                                        inp_grad_cycles_w_fusion[idx], wgt_grad_cycles_original[idx], wgt_grad_cycles_w_fusion[idx],
                                                        layers_communication_size_per_tile_list[idx], 
                                                        ideal_fusion_dim_dp_2[idx], ideal_fusion_dim_dp_4[idx], ideal_fusion_dim_dp_8[idx], ideal_fusion_dim_dp_16[idx],
                                                        ideal_fusion_dim_dp_32[idx], ideal_fusion_dim_dp_64[idx]]
        layer_info_series = pd.Series(layer_info, index=output_csv_col)
        output_csv_df = output_csv_df.append(layer_info_series, ignore_index=True)
    
    output_csv_df.to_csv(path_to_output_csv)

    return 


class FusedChips(object):

    """
    This class models a virtually fused groups of chips
    """

    def __init__(self, num_fused_chips, dp_degree, num_cores_per_chip, num_mxu_cores_per_core, num_subarrays, path_to_npu_config_file, frequency, 
                                        nw_bw, nw_latency, num_nw_links, fusion_pipeline_overhead_coeff):      
        self.num_fused_chips = num_fused_chips
        self.dp_degree = dp_degree
        self.num_cores_per_chip = num_cores_per_chip
        self.num_mxu_cores_per_core = num_mxu_cores_per_core
        self.num_subarrays = num_subarrays
        self.path_to_npu_config_file = path_to_npu_config_file
        self.frequency = frequency
        self.nw_bw = nw_bw
        self.nw_latency = nw_latency
        self.num_nw_links = num_nw_links
        self.fusion_pipeline_overhead_coeff = fusion_pipeline_overhead_coeff

    def set_npu_dims_from_config_file(self):
        cfg_parser = cp.ConfigParser()
        cfg_parser.read(self.path_to_npu_config_file)

        self.sys_array_rows = int(cfg_parser.get("architecture_presets", "ArrayHeight"))
        self.sys_array_cols = int(cfg_parser.get("architecture_presets", "ArrayWidth"))

    def get_layer_wise_candidate_fusion_dims(self, layers):
        """
        This funstion finds the candiate fusion dimensions for each layer. The main objective is to ease pressure on the 
        network bandwidth. The second objective is to maximize compute utlization. The later can be resolved potentially with intra-chip Fission
        """
        candidate_fusion_dims = []

        for l in layers:
            max_nw_bw_stressful_dim = max(l.k, l.n)
            nw_bw_dim_ratio = max(l.k, l.n) / min(l.k, l.n)

            if max_nw_bw_stressful_dim == l.n:
                nw_bw_stressful_dim = "n"
                fused_col = mp_degree
                fused_row = 1
                while fused_col > 1:
                    if nw_bw_dim_ratio == fused_col / fused_row:
                        break
                    elif nw_bw_dim_ratio > fused_col / fused_row:
                        fused_col = min(fused_col * 2, mp_degree)
                        fused_row = mp_degree / fused_col
                        break
                    else:
                        fused_col = fused_col / 2
                        fused_row = fused_row * 2
            else:
                nw_bw_stressful_dim = "k"
                fused_row = mp_degree
                fused_col = 1            
                while fused_row > 1:
                    if nw_bw_dim_ratio == fused_row / fused_col:
                        break
                    elif nw_bw_dim_ratio > fused_row / fused_col:
                        fused_row = min(fused_row * 2, mp_degree)
                        fused_col = mp_degree / fused_row
                        
                        break
                    else:
                        fused_col = fused_col * 2
                        fused_row = fused_row / 2
            candidate_fusion_dims.append((fused_row, fused_col)) 
        return candidate_fusion_dims
    
    def get_layer_wise_fusion_dim_for_full_comp_comm_overlap(self, layers, fwd_cycles, inp_grad_cycles, dp_degree, fusion_dims):
        """
        This function finds the required fusion dim for each layer, to fully overlap the compute and communication in chip fusion
        """
        ideal_fusion_dim = []
        for idx, layer in enumerate(layers):
            _fwd_cycles = fwd_cycles[idx]
            _inp_grad_cycles = inp_grad_cycles[idx]
            compute_time_to_overlap = min(_fwd_cycles, _inp_grad_cycles)
            det_comm_size = max((layer.m / dp_degree) * (layer.k / fusion_dims[idx][0]), (layer.m / dp_degree) * (layer.n / fusion_dims[idx][1]))
            # TODO: make this with freq and bw
            req_num_chip_factor = math.ceil(det_comm_size * data_type_size / (compute_time_to_overlap * self.nw_bw * self.num_nw_links))
            _ideal_fusion_dim = (req_num_chip_factor*fusion_dims[idx][0], req_num_chip_factor*fusion_dims[idx][1])
            ideal_fusion_dim.append(_ideal_fusion_dim)
        
        return ideal_fusion_dim

    def get_layer_wise_num_compute_tiles_for_fusion(self, layer, fusion_dim):
        """
        This function finds the number of compute tiles for microbatching that satisfies the fusion pipeline overhead
        This is used for fwd pass and inp grad pass
        """

        fusion_pipeline_delay = (fusion_dim[0] - 1) + (fusion_dim[1] - 1)
        candidate_num_compute_tiles = math.ceil(fusion_pipeline_delay / self.fusion_pipeline_overhead_coeff)
        m_dim_divisors = list(divisorGenerator(layer.m))
        num_compute_tiles = get_the_minimum_greater_than_element_of_list(m_dim_divisors, candidate_num_compute_tiles)
        
        return num_compute_tiles


    def get_layer_wise_fission_requirements(self, layer, fusion_dim, num_compute_tiles, fission_configs):
        """
        This function measures the NPU systolic array utilization and recommends if intra-chip fission is required (and its degree)
        It deos so based on the candidate fusion dim and for fwd/inp_grad/wgt_grad
        """

        fwd_pass_dim, inp_grad_dim, wgt_grad_dim = self.get_layer_dim_per_core_per_fused_chip(fusion_dim, num_compute_tiles, layer)

        fwd_fission_configs_scores = {}
        inp_grad_fission_configs_scores = {}
        wgt_grad_fission_configs_scores = {}
        for f_c in fission_configs:
            num_fission_sys_array = f_c[0]
            fission_sys_array_row = (self.sys_array_rows / math.sqrt(self.num_subarrays)) * f_c[1]
            fission_sys_array_col = (self.sys_array_cols / math.sqrt(self.num_subarrays)) * f_c[2]
            # fwd pass sys array utilization
            fwd_fission_configs_scores[f_c] = math.ceil(fwd_pass_dim['m'] / num_fission_sys_array)  * math.ceil(fwd_pass_dim['k'] / fission_sys_array_row) * \
                                                                                            math.ceil(fwd_pass_dim['n'] / fission_sys_array_col)
            # inp grad sys array utilization
            inp_grad_fission_configs_scores[f_c] = math.ceil(inp_grad_dim['m'] / num_fission_sys_array)  * math.ceil(inp_grad_dim['k'] / fission_sys_array_row) * \
                                                                                            math.ceil(inp_grad_dim['n'] / fission_sys_array_col)                 
            # wgt grad sys array utilization
            wgt_grad_fission_configs_scores[f_c] = math.ceil(wgt_grad_dim['m'] / num_fission_sys_array)  * math.ceil(wgt_grad_dim['k'] / fission_sys_array_row) * \
                                                                                            math.ceil(wgt_grad_dim['n'] / fission_sys_array_col)
        # we get the fission config with minimum estimated cycles (maximum sys array utilization)
        fwd_optimal_fission_dim = min(fwd_fission_configs_scores, key=fwd_fission_configs_scores.get)
        inp_grad_optimal_fission_dim = min(inp_grad_fission_configs_scores, key=inp_grad_fission_configs_scores.get)
        wgt_grad_optimal_fission_dim = min(wgt_grad_fission_configs_scores, key=wgt_grad_fission_configs_scores.get)


        return fwd_optimal_fission_dim, inp_grad_optimal_fission_dim, wgt_grad_optimal_fission_dim
    
    def calculate_array_utulization_with_fusion(self, layer, fusion_dim, num_compute_tiles):
        """
        This function calcualtes the systolic array utilization based on the fusion configuration to see if this layer benefits from fission or not
        """
        fwd_pass_dim, inp_grad_dim, wgt_grad_dim = self.get_layer_dim_per_core_per_fused_chip(fusion_dim, num_compute_tiles, layer)

        fwd_array_utilization = (fwd_pass_dim['k'] / (math.ceil(fwd_pass_dim['k']/self.sys_array_rows) * self.sys_array_rows)) * \
                                (fwd_pass_dim['n'] / (math.ceil(fwd_pass_dim['n']/self.sys_array_cols) * self.sys_array_cols))
        inp_grad_array_utilization = (inp_grad_dim['k'] / (math.ceil(inp_grad_dim['k']/self.sys_array_rows) * self.sys_array_rows)) * \
                                (inp_grad_dim['n'] / (math.ceil(inp_grad_dim['n']/self.sys_array_cols) * self.sys_array_cols))
        wgt_grad_array_utilization = (wgt_grad_dim['k'] / (math.ceil(wgt_grad_dim['k']/self.sys_array_rows) * self.sys_array_rows)) * \
                                (wgt_grad_dim['n'] / (math.ceil(wgt_grad_dim['n']/self.sys_array_cols) * self.sys_array_cols))
        
        return fwd_array_utilization, inp_grad_array_utilization, wgt_grad_array_utilization


    def get_layer_dim_per_core_per_fused_chip(self, fused_dim, num_compute_tiles, layer_obj):
        """
        This function calculates the dimensions of the layers based on the DP_degree and fusion dimensions for Fwd/Inp_grad/Wgt_grad on each NPU systolic array core
        """
        fwd_pass_dim = {}
        inp_grad_dim = {}
        wgt_grad_dim = {}

        fwd_pass_dim['m'] = math.ceil(layer_obj.m / self.dp_degree / self.num_cores_per_chip / self.num_mxu_cores_per_core / num_compute_tiles)
        fwd_pass_dim['k'] = math.ceil(layer_obj.k / fused_dim[0])
        fwd_pass_dim['n'] = math.ceil(layer_obj.n / fused_dim[1])

        inp_grad_dim['m'] = math.ceil(layer_obj.m / self.dp_degree / self.num_cores_per_chip / self.num_mxu_cores_per_core / num_compute_tiles)
        inp_grad_dim['k'] = math.ceil(layer_obj.n / fused_dim[1])
        inp_grad_dim['n'] = math.ceil(layer_obj.k / fused_dim[0])

        wgt_grad_dim['m'] = math.ceil(layer_obj.k / fused_dim[0] / self.num_cores_per_chip / self.num_mxu_cores_per_core)
        wgt_grad_dim['k'] = math.ceil(layer_obj.m / self.dp_degree)
        wgt_grad_dim['n'] = math.ceil(layer_obj.n / fused_dim[1])

        return fwd_pass_dim, inp_grad_dim, wgt_grad_dim

    def get_layer_topology_for_chip_fusion_approach(self, fused_dim, num_compute_tiles, fwd_fission_dims, inp_grad_fission_dims, 
                                                    wgt_grad_fission_dims, layer_obj, fission_enable):
        """
        This function produces the layer topology item for scalesim call. 
        it both considers with and without fission capability
        """
        fwd_pass_dim, inp_grad_dim, wgt_grad_dim = self.get_layer_dim_per_core_per_fused_chip(fused_dim, num_compute_tiles, layer_obj)
        if fission_enable:            
            fwd_pass_item = TopologyItem(layer_obj.name, math.ceil(fwd_pass_dim['m'] / fwd_fission_dims[0]), fwd_pass_dim['k'], 1, fwd_pass_dim['k'], 1, fwd_pass_dim['n'], 1)       
            inp_grad_item = TopologyItem(layer_obj.name, math.ceil(inp_grad_dim['m'] / inp_grad_fission_dims[0]), inp_grad_dim['k'], 1, inp_grad_dim['k'], 1, inp_grad_dim['n'], 1) 
            weight_grad_item = TopologyItem(layer_obj.name, math.ceil(wgt_grad_dim['m'] / wgt_grad_fission_dims[0]), wgt_grad_dim['k'], 1, wgt_grad_dim['k'], 1, wgt_grad_dim['n'], 1)             

        else:
            fwd_pass_item = TopologyItem(layer_obj.name, fwd_pass_dim['m'], fwd_pass_dim['k'], 1, fwd_pass_dim['k'], 1, fwd_pass_dim['n'], 1)       
            inp_grad_item = TopologyItem(layer_obj.name, inp_grad_dim['m'], inp_grad_dim['k'], 1, inp_grad_dim['k'], 1, inp_grad_dim['n'], 1) 
            weight_grad_item = TopologyItem(layer_obj.name, wgt_grad_dim['m'], wgt_grad_dim['k'], 1, wgt_grad_dim['k'], 1, wgt_grad_dim['n'], 1) 

        return fwd_pass_item, inp_grad_item, weight_grad_item

    def get_communication_size_for_chip_fusion_per_compute_tile(self, layer, fusion_dim, num_compute_tiles, data_type_size):
        """
        This function finds the maximum data that needs to be communicated over the network per each compute tile of microbatching during chip fusion
        Note that this value is the same for both fwd pass and inp_grad, as they are dual of each other in chip fusion
        """
        if fusion_dim[0] == self.num_fused_chips:
            inp_data_forwarding_size = 0
            partial_sum_forwarding_size = (layer.m / self.dp_degree / num_compute_tiles) * (layer.n / fusion_dim[1]) * data_type_size
        elif fusion_dim[1] == self.num_fused_chips:
            inp_data_forwarding_size = (layer.m / self.dp_degree / num_compute_tiles) * (layer.k / fusion_dim[0]) * data_type_size
            partial_sum_forwarding_size = 0
        else:
            inp_data_forwarding_size = (layer.m / self.dp_degree / num_compute_tiles) * (layer.k / fusion_dim[0]) * data_type_size
            partial_sum_forwarding_size = (layer.m / self.dp_degree / num_compute_tiles) * (layer.n / fusion_dim[1]) * data_type_size
        limiting_data_communication_size = max(inp_data_forwarding_size, partial_sum_forwarding_size)

        return  limiting_data_communication_size

    def get_possible_fission_dims(self):
        """
        This functions produces the possible configurations for fission given a number of subarrays
        """
        one_d_possibilities = []
        d = 0
        while 2**d <= self.num_subarrays:
            one_d_possibilities.append(2**d)
            d += 1

        configs = []
        for i in one_d_possibilities:
            for j in one_d_possibilities:
                if i * j <= self.num_subarrays:
                    configs.append((self.num_subarrays/(i*j),i,j))
        return configs
    
    def get_total_dnn_layer_toplogies_fission_fusion_requirements(self, nn_layers, fission_configs, fusion_dims_list):
        """
        Based on the layers dimensions and fusion dimensions, this function produces the layer dimensions for scalesim call
        and the fission/fusion confuigrations and requirements (compute tiles, communication size, etc.)
        """

        fwd_pass_layers_wo_fission = []
        inp_grad_layers_wo_fission = []
        weight_grad_layers_wo_fission = []
        fwd_pass_layers_w_fission = []
        inp_grad_layers_w_fission = []
        weight_grad_layers_w_fission = []

        layers_num_compute_tiles_list = []

        fwd_layers_array_util_list = []
        inp_grad_layers_array_util_list = []
        wgt_grad_layers_array_util_list = []

        fwd_optimal_fission_dims_list = []
        inp_grad_optimal_fission_dims_list = []
        wgt_grad_optimal_fission_dims_list = []

        layers_communication_size_per_tile_list = []
        ### looping over the DNN layers 
        for idx, layer in enumerate(nn_layers):
            # get the num compute tiles of each layer (for microbatching) based on fusion_dim
            layer_num_compute_tiles = self.get_layer_wise_num_compute_tiles_for_fusion(layer, fusion_dims_list[idx])
            layers_num_compute_tiles_list.append(layer_num_compute_tiles)
            # get array utilization after fusion and without fission
            fwd_layer_array_util, inp_grad_layer_array_util, wgt_grad_layer_array_util = self.calculate_array_utulization_with_fusion(layer, fusion_dims_list[idx], layer_num_compute_tiles)
            fwd_layers_array_util_list.append(fwd_layer_array_util)
            inp_grad_layers_array_util_list.append(inp_grad_layer_array_util)
            wgt_grad_layers_array_util_list.append(wgt_grad_layer_array_util)
            # get fission requiremnts 
            fwd_optimal_fission_dim, inp_grad_optimal_fission_dim, wgt_grad_optimal_fission_dim = self.get_layer_wise_fission_requirements(layer, fusion_dims_list[idx], 
                                                                                                    layer_num_compute_tiles, fission_configs)
            fwd_optimal_fission_dims_list.append(fwd_optimal_fission_dim)
            inp_grad_optimal_fission_dims_list.append(inp_grad_optimal_fission_dim)
            wgt_grad_optimal_fission_dims_list.append(wgt_grad_optimal_fission_dim)
            # get communication size requirements for chip fusion based on microbatching
            layer_communication_size_per_tile = self.get_communication_size_for_chip_fusion_per_compute_tile(layer, fusion_dims_list[idx],
                                                            layer_num_compute_tiles, data_type_size)
            layers_communication_size_per_tile_list.append(layer_communication_size_per_tile)
            # get layers topology for Scalesim without fission
            fwd_pass_item_wo_fission, inp_grad_item_wo_fission, weight_grad_item_wo_fission = self.get_layer_topology_for_chip_fusion_approach(fusion_dims_list[idx], 
                                                                                                    layer_num_compute_tiles, fwd_optimal_fission_dim,
                                                                                                    inp_grad_optimal_fission_dim, wgt_grad_optimal_fission_dim, layer, False)       
            # get layers topology for Scalesim with fission
            fwd_pass_item_w_fission, inp_grad_item_w_fission, weight_grad_item_w_fission = self.get_layer_topology_for_chip_fusion_approach(fusion_dims_list[idx], 
                                                                                                    layer_num_compute_tiles, fwd_optimal_fission_dim,
                                                                                                    inp_grad_optimal_fission_dim, wgt_grad_optimal_fission_dim, layer, True)

            fwd_pass_layers_wo_fission.append(fwd_pass_item_wo_fission)
            inp_grad_layers_wo_fission.append(inp_grad_item_wo_fission)
            weight_grad_layers_wo_fission.append(weight_grad_item_wo_fission)
            fwd_pass_layers_w_fission.append(fwd_pass_item_w_fission)
            inp_grad_layers_w_fission.append(inp_grad_item_w_fission)
            weight_grad_layers_w_fission.append(weight_grad_item_w_fission)
        
        final_out = {}
        final_out['fwd_pass_layers_wo_fission'] = fwd_pass_layers_wo_fission
        final_out['inp_grad_layers_wo_fission'] = inp_grad_layers_wo_fission
        final_out['weight_grad_layers_wo_fission'] = weight_grad_layers_wo_fission
        final_out['fwd_pass_layers_w_fission'] = fwd_pass_layers_w_fission
        final_out['inp_grad_layers_w_fission'] = inp_grad_layers_w_fission
        final_out['weight_grad_layers_w_fission'] = weight_grad_layers_w_fission
        final_out['layers_num_compute_tiles'] = layers_num_compute_tiles_list
        final_out['fwd_layers_array_util'] = fwd_layers_array_util_list
        final_out['inp_grad_layers_array_util'] = inp_grad_layers_array_util_list
        final_out['wgt_grad_layers_array_util'] = wgt_grad_layers_array_util_list
        final_out['fwd_optimal_fission_dims'] = fwd_optimal_fission_dims_list
        final_out['inp_grad_optimal_fission_dims'] = inp_grad_optimal_fission_dims_list
        final_out['wgt_grad_optimal_fission_dims'] = wgt_grad_optimal_fission_dims_list
        final_out['layers_communication_size_per_tile'] = layers_communication_size_per_tile_list

        return final_out





def get_scalesim_cycles_w_fusion(fwd_pass_layers_wo_fission, inp_grad_layers_wo_fission, weight_grad_layers_wo_fission, layers_num_compute_tiles_list):
    ### get the compute time from scalesim with fusion and without fission
    scalesim_output_w_fusion = get_scalesim_output_internal(fwd_pass_layers_wo_fission, inp_grad_layers_wo_fission, weight_grad_layers_wo_fission, 
                                                            "Fusion", dnn_name, run_name)
    # update the cycles with compute tiles
    fwd_cycles_w_fusion = []
    inp_grad_cycles_w_fusion = []
    wgt_grad_cycles_w_fusion = []

    
    for i in range(len(layers_num_compute_tiles_list)):
        fwd_cycles_w_fusion.append(int(scalesim_output_w_fusion["fwd_pass_cycles"][i]) * layers_num_compute_tiles_list[i])
        inp_grad_cycles_w_fusion.append(int(scalesim_output_w_fusion["inp_grad_cycles"][i]) * layers_num_compute_tiles_list[i])
        wgt_grad_cycles_w_fusion.append(int(scalesim_output_w_fusion["weight_grad_cycles"][i]))
    
    return fwd_cycles_w_fusion, inp_grad_cycles_w_fusion, wgt_grad_cycles_w_fusion


def update_scalesim_cfg_file_with_fission(path_to_npu_config_file, new_array_height, new_array_width):

    npu_config = cp.ConfigParser()
    npu_config.read(path_to_npu_config_file)

    npu_config.set("architecture_presets", "ArrayHeight", new_array_height)
    npu_config.set("architecture_presets", "ArrayWidth", new_array_width)



def get_dnn_layers_from_mnk_csv(path_to_dnn_csv_file):
    """
    This function (gotten from gen_astrasim) reads the DNN CSV file and generates a list of DNN layer objects
    """
    file_handle = open(path_to_dnn_csv_file, "r")
    lines = file_handle.readlines()
    file_handle.close()
    first = True
    layers = []

    for line in lines:
        if first:
            first = False
            continue
        line = line.strip('\n').strip(' ')
        cols = line.split(",")
        if dnn_type == "Transformer":
            assert len(cols) == 10, "There should be 10 columns in the mnk file"
            layers.append(TransformerLayer(cols))
        else:
            # dnn type is CNN/GEMM
            assert len(cols) == 6, "There should be 6 columns in the mnk file"
            layers.append(Layer(cols))
        print(cols)
    
    return layers



if __name__ == "__main__":

	main()
