import os
import subprocess
import configparser as cp
from systolic_npu_sim import NPUSim


path_to_npu_config_file = r"../../extern/compute/SCALE-Sim-V1/SCALE-Sim/configs/google_smaller.cfg"
path_to_scalesim = r"../../extern/compute/SCALE-Sim-V1/SCALE-Sim/"



def get_scalesim_output_internal(fwd_pass, inp_grad, weight_grad, folder_name, dnn_name, run_name):
    fwd_pass_filename = dnn_name + "_fwd_pass.csv"
    inp_grad_filename = dnn_name + "_inp_grad.csv"
    weight_grad_filename = dnn_name + "_weight_grad.csv"
    write_generated_topology_to_file(folder_name, fwd_pass_filename, fwd_pass, run_name)
    write_generated_topology_to_file(folder_name, inp_grad_filename, inp_grad, run_name)
    write_generated_topology_to_file(folder_name, weight_grad_filename, weight_grad, run_name)


    run_scalesim(fwd_pass_filename, folder_name, run_name)
    run_scalesim(inp_grad_filename, folder_name, run_name)
    run_scalesim(weight_grad_filename, folder_name, run_name)


    fwd_pass_cycles = get_cycles_from_scalesim_output(folder_name, fwd_pass_filename, run_name)
    inp_grad_cycles = get_cycles_from_scalesim_output(folder_name, inp_grad_filename, run_name)
    weight_grad_cycles = get_cycles_from_scalesim_output(folder_name, weight_grad_filename, run_name)


    return { "fwd_pass_cycles" : fwd_pass_cycles, "inp_grad_cycles" : inp_grad_cycles, "weight_grad_cycles" : weight_grad_cycles }


def get_analytical_output_internal(fwd_pass, inp_grad, weight_grad, folder_name, dnn_name, run_name, 
                                                    path_to_npu_config_file, frequency, mem_bw, mem_latency, report_mode):
    fwd_pass_filename = dnn_name + "_fwd_pass.csv"
    inp_grad_filename = dnn_name + "_inp_grad.csv"
    weight_grad_filename = dnn_name + "_weight_grad.csv"
    write_generated_topology_to_file(folder_name, fwd_pass_filename, fwd_pass, run_name)
    write_generated_topology_to_file(folder_name, inp_grad_filename, inp_grad, run_name)
    write_generated_topology_to_file(folder_name, weight_grad_filename, weight_grad, run_name)

    run_analytical_sim(fwd_pass, fwd_pass_filename, folder_name, run_name, path_to_npu_config_file, frequency, mem_bw, mem_latency)
    run_analytical_sim(inp_grad, inp_grad_filename, folder_name, run_name, path_to_npu_config_file, frequency, mem_bw, mem_latency)
    run_analytical_sim(weight_grad, weight_grad_filename, folder_name, run_name, path_to_npu_config_file, frequency, mem_bw, mem_latency)

    fwd_pass_cycles = get_cycles_from_analytical_sim_output(folder_name, fwd_pass_filename, run_name, report_mode)
    inp_grad_cycles = get_cycles_from_analytical_sim_output(folder_name, inp_grad_filename, run_name, report_mode)
    weight_grad_cycles = get_cycles_from_analytical_sim_output(folder_name, weight_grad_filename, run_name, report_mode)

    return { "fwd_pass_cycles" : fwd_pass_cycles, "inp_grad_cycles" : inp_grad_cycles, "weight_grad_cycles" : weight_grad_cycles }


def run_analytical_sim(layer_topologies, filename, folder_name, run_name, path_to_npu_config_file, frequency, mem_bw, mem_latency):
    """
    This fucntion creates a systolic NPU simulation object and calculates the cycle counts and write the report csv
    """
    cycles_filename = filename.rstrip("csv").rstrip(".") + "_cycles.csv" # remove .csv frim topology file and makes it _cycles.csv
    path_to_report_file = "outputs/" + folder_name + "/" + run_name + "/" + cycles_filename
    cycle_report = open(path_to_report_file, 'w')
    cycle_report.write("Layer,\tCycles,\t% Utilization,\n")

    # creating an NPU sim object
    npu_sim_obj = NPUSim(path_to_npu_config_file, frequency, mem_bw, mem_latency)

    for layer in layer_topologies:
        cycles, util = npu_sim_obj.get_compute_cycles_and_utilization(layer)
        line = layer.name + ",\t" + str(cycles) +",\t" + str(util) +",\n"
        cycle_report.write(line)
    


def get_cycles_from_analytical_sim_output(folder_name, topology_file, run_name, report_mode):

    cycles_filename = topology_file.rstrip("csv").rstrip(".") + "_cycles.csv" # remove .csv frim topology file and makes it _cycles.csv
    path_to_cycles_report = "outputs/" + folder_name + "/" + run_name + "/" + cycles_filename
    file_handle = open(path_to_cycles_report, "r")
    lines = file_handle.readlines()
    lines = map(lambda x: x.strip("\n"), lines) # removes the newline characters from the end

    first = True


    if report_mode == "dict":
        cycles = {}
        for line in lines:
            if first:
                first = False
                continue
            cycles['{}'.format(line.split(',')[0].strip('\t'))] = line.split(',')[1].strip('\t')  # '\t removes the last preceeding tab character preesnt in scalesim output
    else:
        cycles = []
        for line in lines:
            if first:
                first = False
                continue
            cycles.append(line.split(',')[1].strip('\t')) # '\t removes the last preceeding tab character preesnt in scalesim output        
    return cycles



def write_generated_topology_to_file(folder_name, file_name, items, run_name):
    current_path = os.getcwd()

    # Create the output directory if does not exist and change to output directory
    output_folder_path = os.path.join(current_path, "outputs")
    if not os.path.exists(output_folder_path):
        os.makedirs("outputs")
    os.chdir(output_folder_path)

    output_folder_path = os.path.join(output_folder_path, folder_name)
    if not os.path.exists(output_folder_path):
        os.makedirs(folder_name)
    os.chdir(output_folder_path)

    # Create the scale sim output directory if does not exist
    #run_name_path = os.path.join(output_folder_path, "scaleSimOutput")
    #if not os.path.exists(run_name_path):
    #    os.makedirs("scaleSimOutput")

    # Create the run name directory if does not exist and change change to run name directory
    run_name_path = os.path.join(output_folder_path, run_name)
    if not os.path.exists(run_name_path):
        os.makedirs(run_name)
    os.chdir(run_name_path)

    # Write the generated topology to a file
    file_handle = open(file_name, "w")
    TopologyItem.writeHeaderToFile(file_handle)
    for row in items:
        row.write(file_handle)
    file_handle.close()

    # Go back to old directory
    os.chdir(current_path)


def run_scalesim(topology_file, folder_name, run_name):
    current_path = os.getcwd()
    full_path = os.path.join(os.getcwd(), "outputs", folder_name, run_name, topology_file)

    # if SCALESIM_VER == 2:
    #     if not scale_sim_installed_flag:
    #         os.chdir(SCALESIM_PATH)
    #         process = subprocess.Popen(["python3", "setup.py", "install"])
    #         process.wait()
    #         os.chdir(current_path)
    #         scale_sim_installed_flag = True

    #     SCALESIM_RUN_PATH = SCALESIM_PATH + '/scalesim/'
    # else:
    SCALESIM_RUN_PATH = path_to_scalesim
    # SCALESIM_CONFIG = "./configs/google_smaller.cfg"
    # os.chdir(SCALESIM_RUN_PATH)
    # if SCALESIM_VER == 2:
    #     SCALESIM_OUTPUT_PATH = current_path + "/outputs/"
    #     process = subprocess.Popen(["python3", "scale.py",
    #                                 "-c", SCALESIM_CONFIG,
    #                                 "-t", full_path,
    #                                 "-p", SCALESIM_OUTPUT_PATH])
    # else:
    process = subprocess.Popen("pwd")
    command = "python3 {} -arch_config={} -network={}".format(os.path.join(SCALESIM_RUN_PATH, 'scale.py'), path_to_npu_config_file, full_path)
    process = subprocess.Popen(command, shell=True)
    process.wait()
    # os.chdir(current_path)





def get_cycles_from_scalesim_output(folder_name, topology_file, run_name):
    config = cp.ConfigParser()
    config.read(path_to_npu_config_file)
    _run_name = config.get('general', 'run_name').strip('"')

    # if SCALESIM_VER == 2:
    #     cycles_filename = "COMPUTE_REPORT.csv"
    # else:
    cycles_filename = topology_file.rstrip("csv").rstrip(".") + "_cycles.csv" # remove .csv frim topology file and makes it _cycles.csv

    current_path = os.getcwd()
    # if SCALESIM_VER == 2:
    #     full_path = SCALESIM_OUTPUT_PATH + '/' + run_name
    # else:
    full_path = os.path.join(current_path, "outputs", _run_name)
    os.chdir(full_path)
    cpy_path = os.path.join(current_path, "outputs", folder_name, run_name)
    process = subprocess.call("cp -rf " + cycles_filename + " " + cpy_path, shell=True)

    file_handle = open(cycles_filename, "r")
    lines = file_handle.readlines()
    lines = map(lambda x: x.strip("\n"), lines) # removes the newline characters from the end

    first = True
    cycles = []

    for line in lines:
        if first:
            first = False
            continue
        cycles.append(line.split(',')[1].strip('\t')) # '\t removes the last preceeding tab character preesnt in scalesim output

    os.chdir(current_path)
    return cycles




class TopologyItem:
    def __init__(self, name, ifmap_height, ifmap_width, filter_height, filter_width, channels, num_filters, strides):
        self.name = name
        self.ifmap_height = ifmap_height
        self.ifmap_width = ifmap_width
        self.filter_height = filter_height
        self.filter_width = filter_width
        self.channels = channels
        self.num_filters = num_filters
        self.strides = strides

    def print(self):
        line = [self.name, self.ifmap_height, self.ifmap_width, self.filter_height, self.filter_width, self.channels, self.num_filters, self.strides]
        line = list(map(lambda x: str(x), line))
        line = "\t".join(line)
        DPRINT(line)

    def write(self, file_handle):
        line = [self.name, self.ifmap_height, self.ifmap_width, self.filter_height, self.filter_width, self.channels, self.num_filters, self.strides, ""]
        line = list(map(lambda x: str(x), line))
        file_handle.write(",".join(line))
        file_handle.write("\n")

    @staticmethod
    def printHeader():
         DPRINT("Layer name\tifmap height\tifmap width\tfilter height\tfilter width\tchannels\tnum filter\tstrides")

    @staticmethod
    def writeHeaderToFile(file_handle):
        file_handle.write("Layer name,ifmap height,ifmap width,filter height,filter width,channels,num filter,strides,\n")