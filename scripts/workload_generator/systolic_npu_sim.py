import math
import configparser as cp

class NPUSim(object):
    """
    This class models a systolic array NPU
    """
    def __init__(self, config, frequency, mem_bw, mem_latency):
        self.config = config
        self.frequency = frequency
        self.mem_bw = mem_bw
        self.mem_latency = mem_latency
        # read the config file
        cfg_parser = cp.ConfigParser()
        cfg_parser.read(self.config)
        # set the systolic array params
        self.sys_array_rows = int(cfg_parser.get("architecture_presets", "ArrayHeight"))
        self.sys_array_cols = int(cfg_parser.get("architecture_presets", "ArrayWidth"))
        self.ibuf_sram_size = int(cfg_parser.get("architecture_presets", "IfmapSramSz"))
        self.wbuf_sram_size = int(cfg_parser.get("architecture_presets", "FilterSramSz"))
        self.obuf_sram_size = int(cfg_parser.get("architecture_presets", "OfmapSramSz"))
        self.dataflow = cfg_parser.get("architecture_presets", "Dataflow")

    def get_compute_cycles_and_utilization(self, layer_topo):
        """
        Calculating the Conv compute cycles on Systolic Array
        """
        ih = layer_topo.ifmap_height
        iw = layer_topo.ifmap_width
        kh = layer_topo.filter_height
        kw = layer_topo.filter_width
        ic = layer_topo.channels
        oc = layer_topo.num_filters
        strd = layer_topo.strides

        oh = math.floor((ih - kh + strd)/ strd)
        ow = math.floor((iw - kw + strd)/ strd)

        _compute_cycles = (oh * ow) * math.ceil(kh * kw * ic / self.sys_array_rows) * \
                                            math.ceil(oc / self.sys_array_cols)
        _pipeline_delay_cycles = self.sys_array_rows + self.sys_array_cols

        compute_cycles = _compute_cycles + _pipeline_delay_cycles

        # calculating array utilization
        array_util = ((kw * kh * ic) / (math.ceil(kw*kh*ic/self.sys_array_rows) * self.sys_array_rows)) * \
                                (oc / (math.ceil(oc/self.sys_array_cols) * self.sys_array_cols)) * 100

        return compute_cycles, array_util

 